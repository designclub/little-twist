<div id="writetousModal" class="modal modal-my modal-my-xs fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="modal-my-heading">
                    <strong class="color-green">Остались </strong><strong>вопросы?</strong>
                    Оставьте заявку и мы Вам перезвоним!
 
                </div>
            </div>
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                    'id'=>'writetous-form-modal',
                    'type' => 'vertical',
                    'htmlOptions' => ['class' => 'form-my', 'data-type' => 'ajax-form'],
                ]); ?>

                <?php if (Yii::app()->user->hasFlash('writetous-success')) : ?>
                    <script>
                        $('#writetousModal').modal('hide');
                        $('#messageModal').modal('show');
                        setTimeout(function(){
                            $('#messageModal').modal('hide');
                        }, 4000);
                    </script>
                <?php endif ?>

                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-row__left">
                            <?= $form->textFieldGroup($model, 'name', [
                                'widgetOptions'=>[
                                    'htmlOptions'=>[
                                        'class' => '',
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                            <div class="form-group">
                                <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                                <?php $this->widget('CMaskedTextFieldPhone', [
                                    'model' => $model,
                                    'attribute' => 'phone',
                                    'mask' => '+7(999)999-99-99',
                                    'htmlOptions'=>[
                                        'class' => 'data-mask form-control',
                                        'data-mask' => 'phone',
                                        'placeholder' => Yii::t('MailModule.mail', 'Телефон'),
                                        'autocomplete' => 'off'
                                    ]
                                ]) ?>
                                <?php echo $form->error($model, 'phone'); ?>
                            </div>
                            <?= $form->textFieldGroup($model, 'email', [
                                'widgetOptions'=>[
                                    'htmlOptions'=>[
                                        'class' => '',
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                        <div class="form-row__right">
                            <?= $form->textAreaGroup($model, 'body', [
                                'widgetOptions'=>[
                                    'htmlOptions'=>[
                                        'class' => '',
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <?= $form->hiddenField($model, 'verify'); ?>
                    <div class="form-bot">
                        <div class="form-captcha">
                            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
                            </div>
                            <?= $form->error($model, 'verifyCode');?>
                        </div>
                        <div class="form-button">
                            <button class="but but-green" id="writetous-modal-button" data-send="ajax">Отправить</button>
                        </div>
                    </div>
                    <div class="terms_of_use"> 
                        <?php if (Yii::app()->hasModule('contentblock')): ?>
                            <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'policy-form']); ?>
                        <?php endif; ?>
                     </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>