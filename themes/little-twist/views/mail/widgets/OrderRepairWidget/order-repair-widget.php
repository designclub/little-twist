<div class="form-question-box">
    <div class="container">
        <div class="top-block">
            <h2 class="header-text text-center">Остались вопросы</h2>
            <p class="text-center" style="position: relative;">Заполните форму и мы в кратчайшие сроки ответим на все ваши вопросы</p>
        </div>
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
            'id'=>'order-repair-form',
            'type' => 'inline',
            'htmlOptions' => ['class' => 'form form-my form-order-repair', 'data-type' => 'ajax-form'],
        ]); ?>
        <?php if (Yii::app()->user->hasFlash('order-repair-success')): ?>
        <script>
            $( '#callbackModal' ).modal( 'hide' );
            $( '#messageModal' ).modal( 'show' );
            setTimeout( function () {
                $( '#messageModal' ).modal( 'hide' );
            }, 4000 );
        </script>
        <?php endif ?>

        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div class="form-order-repair-row__item">
                    <div class="ico-inputs">
                        <div class="ico-input">
                            <span class="icon-user1"></span>
                        </div>
                        <?= $form->textFieldGroup($model, 'name', [
                                'widgetOptions'=>[
                                    'htmlOptions'=>[
                                        'class' => '',
                                        'autocomplete' => 'off'
                                    ]
                                ]
                            ]); ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-xs-12">
                <div class="ico-inputs">
                    <div class="ico-input">
                        <span class="icon-phone-footer"></span>
                    </div>
                    <div class="form-group">
                        <?php $this->widget('CMaskedTextFieldPhone', [
                                'model' => $model,
                                'attribute' => 'phone',
                                'mask' => '+7(999)999-99-99',
                                'htmlOptions'=>[
                                    'class' => 'data-mask form-control',
                                    'data-mask' => 'phone',
                                    'placeholder' => Yii::t('MailModule.mail', 'Ваш телефон'),
                                    'autocomplete' => 'off'
                                ]
                            ]) ?>
                        <?php echo $form->error($model, 'phone'); ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-xs-12">
                <div class="ico-inputs">
                    <div class="ico-input">
                        <span class="icon-close-envelope"></span>
                    </div>
                    <?= $form->textFieldGroup($model, 'email', [
                            'widgetOptions'=>[
                                'htmlOptions'=>[
                                    'class' => '',
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="ico-inputs">
                    <div class="ico-input">
                        <span class="icon-msg"></span>
                    </div>
                    <?= $form->textAreaGroup($model, 'text'); ?>
                </div>
            </div>
        </div>


        <?= $form->hiddenField($model, 'verify'); ?>

        <div class="form-order-repair-row form-order-repair__bottom">
            <div class="form-order-repair__captcha">
                <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
                </div>
                <?= $form->error($model, 'verifyCode');?>
            </div>
            <div class="form-order-repair__button">
                <?= CHtml::submitButton(Yii::t('MailModule.mail', 'Задать вопрос'), [
                    'id' => 'callback-button', 
                    'class' => 'btn btn-red but but-green', 
                    'data-send'=>'ajax'
                ]) ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
        <?php if (Yii::app()->hasModule('contentblock')): ?>
        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'policy-form']); ?>
        <?php endif; ?>
    </div>
</div>