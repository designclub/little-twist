<div class="category-item">
    <div class="inner-item">
        <div class="image-category">
            <?php if(empty($data->icon_category)): ?>
                <?= CHtml::link(CHtml::image($data->getImageUrl(), '', ['alt' => CHtml::encode($data->description), 'title' => CHtml::encode($data->title) ]), '/store/'.$data->slug); ?>
            <?php else: ?>
                <?= CHtml::link($data->icon_category, '/store/'.$data->slug); ?>
            <?php endif; ?>
        </div>
        <label><?= CHtml::link($data->name, '/store/'.$data->slug); ?></label>
        <div><?= CHtml::link(strip_tags($data->description), '/store/'.$data->slug); ?></div>
    </div>
</div>