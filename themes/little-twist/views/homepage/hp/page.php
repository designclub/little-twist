<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>

<div class="hp-container">
    <div class="catalog-menu hidden-xs">
        <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 3]); ?>
        <?php if (Yii::app()->hasModule('contentblock')): ?>
        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'main-social']); ?>
        <?php endif; ?>
    </div><!-- end catalog-menu -->
    <div class="slider">
        <?php $this->widget("application.modules.gallery.widgets.SlickWidget", ['galleryId' => 1, 
            'clietOptions' => [
                'prevArrow' => 'js:$(".prev-slider-arrow")',
                'nextArrow' => 'js:$(".next-slider-arrow")',
                'dotsClass' => 'container dots-slider',
                'dots' => true,
                'arrows' => false,
                'responsive' => [
                    [
                        'breakpoint' => 767,
                        'settings'=> [
                            'arrows' => false
                        ]
                    ],                
                ]
            ]
        ]); ?>
    </div><!-- end catalog-menu -->
</div><!-- end hp-container -->

<div class="banner banner-middle">
    <?php if (Yii::app()->hasModule('contentblock')): ?>
        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'banner-middle']); ?>
    <?php endif; ?>
</div>
<div class="banner banner-bottom">
    <?php if (Yii::app()->hasModule('contentblock')): ?>
        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'banner-bottom']); ?>
    <?php endif; ?>
</div>