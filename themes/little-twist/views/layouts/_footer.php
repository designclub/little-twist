<div class="panel-header footer">
    
    <div class="logo hidden-xs">
        <?php if (Yii::app()->hasModule('contentblock')): ?>
            <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
        <?php endif; ?>
        <div class="copyright">
            &copy; <?= date('Y'); ?>
            <?php if (Yii::app()->hasModule('contentblock')): ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'copyright']); ?>
            <?php endif; ?>
        </div>
    </div><!-- end logo -->
    
    <div class="content-site">
        <div class="header-menu hidden-xs">
            <?php if (Yii::app()->hasModule('menu')): ?>
                <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'layout' => 'menu']); ?>
            <?php endif; ?>
        </div><!-- end header-menu -->
        <div class="policy-block">
            <?php if (Yii::app()->hasModule('contentblock')): ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'policy']); ?>
            <?php endif; ?>
        </div>
    </div>
    
    <div class="header-contact">
        <div class="shopcart-block foot-phone">
            <a href="https://wa.me/79128404141" class="whatsapp"><i class="icon-whatsapp-logo"></i></a>
            <?php if (Yii::app()->hasModule('contentblock')): ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'phone']); ?>
            <?php endif; ?>
        </div>
        <div class="callback-block">
            <a href="mailto:little-twist@info.ru"><i class="icon--3"></i></a>
            <?php if (Yii::app()->hasModule('contentblock')): ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'email']); ?>
            <?php endif; ?>
        </div>
        
    </div><!-- end header-contact -->
</div><!-- end panel-header -->