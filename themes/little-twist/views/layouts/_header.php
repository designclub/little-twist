<div class="panel-header panel-header-top">
    <div class="mobile-container">
        <div class="mobile-panel">
            <div class="m-menu-button">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>
            <div class="mobile-menu close-panel">
                <div class="m-menu-buttons">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
                <div class="mobile-content">
                    <div class="catalog-menu-mobile visible-xs">
                        <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 2]); ?>
                    </div><!-- end catalog-menu -->
                    
                    <?php if (Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'layout' => 'menu']); ?>
                    <?php endif; ?>

                    <?php if (Yii::app()->hasModule('contentblock')): ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'main-social']); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="logo">
        <?php if (Yii::app()->hasModule('contentblock')): ?>
            <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
        <?php endif; ?>
    </div><!-- end logo -->
     
    <div class="content-site hidden-xs">
        <div class="header-menu">
            <?php if (Yii::app()->hasModule('menu')): ?>
                <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'layout' => 'menu']); ?>
            <?php endif; ?>
        </div><!-- end header-menu -->
        <div class="search-panel hidden">
            <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
        </div><!-- end search-panel -->
    </div>
    
    <div class="header-contact">
        <div class="search-block  hidden-xs">
            <a href="#" class="search-icon"><i class="icon-magnifying-glass"></i></a>
            <a href="#" class="close-icon hidden"><i class="icon-close" aria-hidden="true"></i></a>
        </div>
        <div class="shopcart-block" id="shopping-cart-widget">
            <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget'); ?>
        </div>
        <div class="callback-block  hidden-xs">
            <a href="https://wa.me/79128404141" class="whatsapp"><i class="icon-whatsapp-logo"></i></a>
        </div>
        <div class="phone-block  hidden-xs">
        <?php if (Yii::app()->hasModule('contentblock')): ?>
            <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'phone']); ?>
        <?php endif; ?>
        </div>
        <div class="shopcart-block top-phone visible-xs">
            <a href="https://wa.me/79128404141" class="whatsapp"><i class="icon-whatsapp-logo"></i></a>
        </div>
    </div><!-- end header-contact -->
</div><!-- end panel-header -->