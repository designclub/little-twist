<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <?php
        Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.css');

        Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/lightbox.min.css');
//Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/jquery.kladr.min.css');
    
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/blog.js');
//Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.kladr.min.js');
//Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/address.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.scrollbar.min.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/bootstrap-notify.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.li-translit.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/html5lightbox/html5lightbox.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.zoom.min.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/lightbox.min.js', CClientScript::POS_END);
    ?>
    
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>
    
<!-- container -->
<div class="container-site">
    
    <!-- header -->
    <?php $this->renderPartial('//layouts/_header'); ?>
    <!-- header end -->
    
    <!-- flashMessages -->
    <?php $this->widget('yupe\widgets\YFlashMessages'); ?>
    <!-- end flashMessages -->
    
    <!-- breadcrumbs -->
    <?php /*$this->widget(
        'bootstrap.widgets.TbBreadcrumbs',
        [
            'links' => $this->breadcrumbs,
        ]
    );*/ ?>
    <!-- end breadcrumbs -->
    
    <?= $content; ?>
    
    <!-- footer -->
    <?php $this->renderPartial('//layouts/_footer'); ?>
    <!-- footer end -->
    
</div>
    
<div class="lds-roller hidden"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    
<script type="text/javascript">
    $(function(){

        function visibleMobileMenu(){
            $('body').addClass('menu_m_visible');
            $('.mobile-panel').addClass('active-menu');  
        }
        
        function hiddenMobileMenu(){
            $('body').removeClass('menu_m_visible');
            $('.mobile-panel').removeClass('active-menu');  
        }

        $(document).on('click', function(e){

            var targetClass = e.target.className;
            var targetNodeClass = e.target.parentNode.className;

            if(targetClass == 'm-menu-button' || targetNodeClass == 'mobile-panel')
                visibleMobileMenu();
            
            if(targetClass == 'm-menu-buttons' || targetNodeClass == 'm-menu-buttons')
                hiddenMobileMenu();
            
            if(targetNodeClass != 'mobile-menu close-panel' && targetClass != 'm-menu-button' && targetNodeClass != 'catalog-menu-mobile' && targetNodeClass != 'mobile-panel active-menu' && targetNodeClass != 'mobile-content')
                hiddenMobileMenu();
        });
        
        function removeClasses() {
            $(".mobile-menu ul li").each(function() {
                var custom_class = $(this).find('a').data('class');
                $('body').removeClass(custom_class);
            });
        }

		$(".search-icon").on("click", function(clck){
            clck.preventDefault();
			$(".header-menu").hide();
			$(".search-panel").removeClass("hidden");
			$(".search-icon").addClass("hidden");
			$(".close-icon").removeClass("hidden");
		});
		
		$(".close-icon").on("click", function(clck){
            clck.preventDefault();
			$(".header-menu").show();
			$(".search-panel").addClass("hidden");
			$(".close-icon").addClass("hidden");
            $(".search-icon").removeClass("hidden");
		});
        
        $('#razmer-2 label').on("click", function(clck){
            clck.preventDefault();
            $(this).toggleClass('active');
        });

        $('.catalog-menu li').on("click", function(clck){
            var element = $(this);
            
            if (element.parents('li').length == 0){
                $(this).toggleClass('active');
                clck.preventDefault();
            }
            
            if (element.children('ul').length == 0){
                window.location.href = $(this).children('a').attr('href');
                return true;
            }
            
            clck.stopPropagation();
        });
        
        $('.catalog-menu-mobile li').on("click", function(clck){
            var element = $(this);
            if (element.parents('li').length == 0){
                $(this).toggleClass('active');
                clck.preventDefault();
            }
            clck.stopPropagation();
        });
        
    });    
</script>
    
<!--<div class='notifications top-right' id="notifications"></div>-->
<!-- container end -->

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js"></script>
    
</body>
</html>