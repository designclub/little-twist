<script type="text/javascript">
    $(document).ready(function () {
        $('.scrollbar-inner').scrollbar();
    });
</script>

<div class="scrollbar-inner list-pvz-sdek">
    <?php if(isset($pvz['pvz']) && count($pvz['pvz']) > 0): ?>
        <h3>Из какого пункта выдачи вы хотите забрать покупку?</h3>
        <fieldset class="row block-pvz">
            <?php foreach ($pvz['pvz'] as $key => $item): ?>
             <div class="item-pvz col-lg-6 col-md-4 col-sm-12 col-xs-12">
                    <input type="radio" name="Order[pvz_id]" id="pvz-<?= $item['code']; ?>" value="<?= $item['code']; ?>" data-address="<?= $item['address']; ?>">
                    <label class="radio" for="pvz-<?= $item['code']; ?>">
                        <div class="pvz-name"><?= $item['name']; ?></div>
                        <div class="col-fl col-item pvz-address">
                            <i class="fa fa-map-marker" aria-hidden="true"></i><p><?= $item['address']; ?></p>
                            <!--<div class="hover-info">
                                <p><?php //$item['fullAddress']; ?> <span>(<?php //$item['addressComment']; ?>)</span></p>
                                <p><i class="fa fa-envelope-o" aria-hidden="true"></i><?php // $item['email']; ?></p>
                            </div>-->
                        </div>
                        <div class="col-fl col-item pvz-phone">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <p><?= $item['phone']; ?></p>
                        </div>
                        <div class="col-fl col-item pvz-workTime">
                            <i class="fa fa-clock-o" aria-hidden="true"></i><p><?= $item['workTime']; ?></p>
                        </div>
                    </label>
                </div>
            <?php endforeach; ?>
        </fieldset>
    <?php endif; ?>
</div>