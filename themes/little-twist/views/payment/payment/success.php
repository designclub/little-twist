<?php
	$this->title = Yii::t( 'PaymentModule.payment', 'Успешное проведение оплаты');
?>

<div class="content-site">
    <div class="page-txt page-site">
        <h2><?= Yii::t( 'PaymentModule.payment', 'Оплата прошла успешно!'); ?></h2>
        <p>Спасибо за заказ! Ваша оплата проведена успешно!</p>
        <?php if(!empty($order->dispatch_number)): ?>
            <p>Данные по Вашему заказу  № <?= $order->id; ?> и трек номер № <?= $order->dispatch_number; ?> доставки отправлен на указанную Вами электронную почту.</p>
        <?php endif; ?>
    </div>
</div>