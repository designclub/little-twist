<?php
	$this->title = Yii::t( 'PaymentModule.payment', 'Ошибка платежа');
?>

<div class="content-site">
    <div class="page-txt page-site">
        <h2><?= Yii::t( 'PaymentModule.payment', 'Ошибка проведения платежа!'); ?></h2>
        <p>Возникла ошибка при проведении платежа. Необходимо попробовать позже!</p>
    </div>
</div>