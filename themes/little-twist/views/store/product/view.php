<?php

/* @var $product Product */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical();

$mainAssets = Yii::app()->getModule( 'store' )->getAssetsUrl();
Yii::app()->getClientScript()->registerScriptFile( $mainAssets . '/js/jquery.simpleGal.js' );

Yii::app()->getClientScript()->registerCssFile( Yii::app()->getTheme()->getAssetsUrl() . '/css/store-frontend.css' );
Yii::app()->getClientScript()->registerScriptFile( Yii::app()->getTheme()->getAssetsUrl() . '/js/store.js' );

$this->breadcrumbs = array_merge(
    [ Yii::t( "StoreModule.store", 'Catalog' ) => [ '/store/product/index' ] ],
    $product->category ? $product->category->getBreadcrumbs( true ) : [], [ CHtml::encode( $product->name ) ]
);

?>

<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="https://yastatic.net/share2/share.js"></script>

<div class="store-container store-container-category">
    <div class="left-panel-store">
        <div class="catalog-menu">
            <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 2]); ?>
            <?php if (Yii::app()->hasModule('contentblock')): ?>
            <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'main-social']); ?>
            <?php endif; ?>
        </div>
    </div>
            
<!--<div class="store-container product-container">-->
    <div class="right-panel-store">
        <div class="breadcrumbs hidden-xs">
            <div class="row">
                <div class="col-xs-12">
                    <?php $this->widget(
                        'bootstrap.widgets.TbBreadcrumbs',
                        [
                            'links' => $this->breadcrumbs,
                        ]
                    );?>
                </div>
            </div>
        </div>

        <div class="product-panel">
            <div class="row" xmlns="http://www.w3.org/1999/html" itemscope itemtype="http://schema.org/Product">
                <div class="col-sm-6 col-xs-12 product-images">
                    
                    <div class="left-thumb">
                        <div class="thumbnails">
                            <div class="image-thumbnails">
                                <?= CHtml::link(CHtml::image($product->getImageUrl(100, 100, false), '', 
                                     [
                                        'alt' => CHtml::encode($product->getImageAlt()), 
                                        'title' => CHtml::encode($product->getImageTitle()), 
                                    ]), $product->getImageUrl(), ['class' => 'image thumb thumbnail']); ?>
                                <?php foreach ($product->getImages() as $key => $image) : ?>
                                     <?= CHtml::link(CHtml::image($image->getImageUrl(100, 100, false), '', 
                                     [
                                        'alt' => CHtml::encode($image->alt), 
                                        'title' => CHtml::encode($image->title), 
                                    ]), $image->getImageUrl(), ['class' => 'image thumb thumbnail']); ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="image-thumbnails video-link">
                            <?php if(!empty($product->video)): ?>
                                <?= CHtml::link(CHtml::image($product->getImageUrl(100, 100, true), CHtml::encode($product->getImageTitle()), ['alt' => CHtml::encode($product->getImageAlt())]), "JavaScript:html5Lightbox.showLightbox(3, '" . $product->video . "', '');"); ?>
                            <?php endif; ?> 
                        </div>
                    </div>

                    <div class="image-main">
                        <?php if(!empty($product->video)): ?>
                            <div class="button-video">
                                <?= CHtml::link('<i class="icon-play-button"></i>Есть видеообзор', "JavaScript:html5Lightbox.showLightbox(3, '" . $product->video . "', '');"); ?>
                            </div>
                        <?php endif; ?>
                        <?php //CHtml::link(CHtml::image($product->getImageUrl(450, 600, true), CHtml::encode($product->getImageTitle()), ['id' => 'main-image', 'alt' => CHtml::encode($product->getImageAlt()), 'itemprop' => 'image']), $product->getImageUrl(), ['class' => 'image img-lightox', 'data-lightbox' => 'roadtrip']); ?>
                        <?php //CHtml::link(CHtml::image($product->getImageUrl(360, 480, false), CHtml::encode($product->getImageTitle()), ['id' => 'main-image', 'alt' => CHtml::encode($product->getImageAlt()), 'itemprop' => 'image']), $product->getImageUrl(), ['class' => 'image img-lightox', 'data-lightbox' => 'roadtrip']); ?>
                        <?= CHtml::link(CHtml::image($product->getImageUrl(), CHtml::encode($product->getImageTitle()), ['id' => 'main-image', 'alt' => CHtml::encode($product->getImageAlt()), 'itemprop' => 'image', /*'style' => 'max-height: 480px;'*/]), $product->getImageUrl(), ['class' => 'image img-lightbox zoom', 'data-lightbox' => 'roadtrip']); ?>
                    </div>
                </div><!-- end product-images -->

                <div class="col-sm-6 col-xs-12 product-data">
                    <h1 class="product-title" itemprop="name"><?= CHtml::encode($product->getTitle()); ?></h1>
                    <div class="product-sku">Артикул: <span><?= $product->sku; ?></span></div>

                     <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post">

                        <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                        <?= CHtml::hiddenField(Yii::app()->getRequest()->csrfTokenName, Yii::app()->getRequest()->csrfToken); ?>

                        <div class="product-block-prices-shopcart">
                            <div class="block-flex-auto block-prices">
                                <?php /*if (($product->discount_price > 0) && ($product->getBasePrice() > $product->getResultPrice())): ?>
                                <div class="price price-old">
                                    <span><?= $product->getBasePrice(); ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                </div>
                                <?php endif;*/ ?>
                                <div class="price <?= ($product->discount_price > 0) && ($product->getBasePrice() > $product->getResultPrice()) ? 'price-new' : ''; ?>">
                                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <input type="hidden" id="base-price" value="<?= round($product->getResultPrice(), 2); ?>"/>
                                        <div id="result-price" itemprop="price">
                                            <?= number_format($product->getResultPrice(), 0, '.', ' '); ?>
                                        </div>
                                        <i class="icon-ruble-currency-sign"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="block-flex-auto block-shopcart">
                                <button class="btn btn-shopcart" id="add-product-to-cart" data-loading-text="<?= Yii::t("StoreModule.store", "Добавление в корзину"); ?>">
                                    <?= Yii::t("StoreModule.store", "Add to cart"); ?>
                                </button>
                            </div>
                        </div><!-- end product-block-prices-shopcart -->

                        <div class="product-options">

                                <table class="table table-condensed hide">
                                    <?php foreach ($product->getVariantsGroup() as $title => $variantsGroup) : ?>
                                        <tr>
                                            <td style="padding: 0;">
                                                <?php echo CHtml::encode($title); ?>
                                            </td>
                                            <td>
                                                <?php echo
                                                CHtml::dropDownList(
                                                    'ProductVariant[]',
                                                    null,
                                                    CHtml::listData($variantsGroup, 'id', 'optionValue'),
                                                    ['empty' => '', 'class' => 'form-control', 'options' => $product->getVariantsOptions()]
                                                ); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            <?php $this->widget('application.modules.store.widgets.VariantTreeWidget', ['product' => $product]) ?>
                            <p class="hidden">Остаток: <span class="js-view-quantity">0</span> шт.</p>
                            <?= CHtml::hiddenField('maxCount', 0); ?>
                        </div>         

                        <div class="product-description line-bottom">
                            <label><?= Yii::t("StoreModule.store", "Description"); ?></label>
                            <p><?= $product->description; ?></p>
                         </div>

                        <div class="product-share">
                            <label>Поделиться:</label><div class="ya-share2" data-services="vkontakte,viber,whatsapp"></div>
                        </div>

                    </form>
                </div><!-- end product-data -->

            </div>
        </div><!-- end product-panel -->
        
        <div class="linked-products">
            <?php $this->widget('application.modules.store.widgets.LinkedProductsWidget', ['product' => $product, 'code' => null,]); ?>
        </div>
        
    </div><!-- end right-panel-store -->
</div><!-- end store-container -->



<?php Yii::app()->getClientScript()->registerScript(
    "product-images",
    <<<JS
        $(".thumbnails").simpleGal({mainImage: "#main-image"});
        
        function windowSize(){
            if ($(window).width() <= '768'){
                $('.zoom').trigger('zoom.destroy');
            }else{
                $('.zoom').zoom();
            }
        }
        
        $(window).on('load resize',windowSize);
        $(window).resize(windowSize);
JS
); ?>