<div class="col-blg-3 col-sm-4 product-item-block">
	<div class="product-item item-product">
        
        <div class="product-image">
            <?php if($data->video): ?>
                <?= CHtml::link('<i class="icon-youtube-pink"></i>', "JavaScript:html5Lightbox.showLightbox(3, '" .$data->video. "', '');"); ?>
            <?php endif; ?>
            <?php if (($data->discount_price > 0) && ($data->getBasePrice() > $data->getResultPrice())): ?>
                <div class="btn-offer">Акция на товар</div>
            <?php endif; ?>
            <a href="<?= ProductHelper::getUrl($data); ?>">
                <?= CHtml::image($data->getImageUrl(), CHtml::encode($data->getImageAlt()), ['class' => 'img-main-hover', 'style' => 'max-height: 350px', 'title' => CHtml::encode($data->getImageTitle())]); ?>

                <?php if(count($data->getImages()) > 0): ?>
                <div class="thubms-product-images-carousel">
                    <div class="thubms-product-images">
                            <div class="image-thumb active" data-src-images="<?= $data->getImageUrl(280, 350, true); ?>"></div>
                        <?php foreach($data->getImages() as $key => $image): ?>
                            <div class="image-thumb" data-src-images="<?= $image->getImageUrl(280, 350, true); ?>"></div>
                        <?php endforeach; ?> 
                    </div>
                </div>
                <?php endif; ?>
            </a>
        </div>
        
        <div class="product-info-block product-title">
            <?= CHtml::link(CHtml::encode($data->getName()), ProductHelper::getUrl($data)); ?>
        </div>
        
        <div class="product-info-block product-price-more">
            <div class="product-price">
                <?= number_format($data->getResultPrice(), 0, '.', ' '); ?><i class="icon-ruble-currency-sign"></i>
            </div> 
            <div class="product-link-more">
                <?= CHtml::link('Подробнее', ProductHelper::getUrl($data), ['class' => 'btn-pink']); ?>
            </div>
        </div>
        
    </div>
</div>