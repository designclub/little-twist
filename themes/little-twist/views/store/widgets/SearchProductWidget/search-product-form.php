<section class="catalog-filter">
    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'action' => ['/store/product/index'],
            'method' => 'GET',
        ]
    ) ?>
    <div class="search-group">
        <?= CHtml::textField(
            AttributeFilter::MAIN_SEARCH_QUERY_NAME,
            CHtml::encode(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)),
            ['class' => 'form-control']
        ); ?>
        <button type="submit" class="btn btn-search">
            <i class="glyphicon glyphicon-search"></i>
        </button>
    </div>
    <?php $this->endWidget(); ?>
</section>
