<?php
/* @var $attributes array */
?>
<div class="filter-block">
    <div class="filter-attributes">
        <?php $this->widget(
            'application.modules.store.widgets.filters.AttributesFilterWidget', [
                'attributes' => $attributes,
                'category' => $category,
            ]
        ) ?>
    </div>
</div>
