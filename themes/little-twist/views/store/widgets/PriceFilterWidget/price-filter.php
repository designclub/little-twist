<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<div class="filter-block-checkbox-list">
    <div class="filter-block-header">
        <label>
            <?= CHtml::link(Yii::t('StoreModule.store', 'Price filter'), '#', [ 'class' => 'attr-panel', 'data-id' => 'price-filter']); ?>
        </label>
    </div>
    <div class="filter-block-body hidden" id="price-filter">
        <div class="price-flex">
            <?= CHtml::textField('price[from]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'from', Yii::app()->getRequest()) , ['class' => 'form-control', 'placeholder' => Yii::t('StoreModule.store', 'from')]); ?>
        </div>
        <div class="price-flex">
            <?= CHtml::textField('price[to]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'to', Yii::app()->getRequest()), ['class' => 'form-control', 'placeholder' => Yii::t('StoreModule.store', 'to')]); ?>
        </div>
    </div>
</div>