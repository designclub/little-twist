<?php $filter = Yii::app()->getComponent('attributesFilter');?>
<?php if ($attribute->name == 'razmer' || $attribute->name == 'sezon'): ?>
<div class="filter-block-checkbox-list">
    <div class="filter-block-header">
        <label>
            <?= CHtml::link($attribute->title, '#', [ 'class' => 'attr-panel', 'data-id' => $attribute->name . '-' . $attribute->id ]); ?>
        </label>
    </div>
    <div class="filter-block-body hidden" id="<?= $attribute->name; ?>-<?= $attribute->id; ?>">
        <?php foreach ($attribute->options as $option): ?>
            <div class="checkbox">
                <label>
                    <?= CHtml::checkBox($filter->getDropdownOptionName($option), $filter->getIsDropdownOptionChecked($option, $option->id), ['value' => $option->id]) ?>
                    <?= $option->value ?>
                </label>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>
