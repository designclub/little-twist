<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php if ($dataProvider->getTotalItemCount()): ?>
    <h3>К товару идеально подходят:</h3>
    <section class="list-category-products">
        <?php $this->widget(
            'bootstrap.widgets.TbListView',
            [
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'template' => '{items}',
                'summaryText' => '',
                'enableHistory' => true,
                'cssFile' => false,
                'itemsCssClass' => 'row items',
                'sortableAttributes' => [
                    'name',
                    'price',
                ],
            ]
        ); ?>
    </section>
<?php endif; ?>