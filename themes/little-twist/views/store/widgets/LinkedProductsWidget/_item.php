<div class="col-blg-3 col-sm-4 product-item-block">
	<div class="product-item item-product">
        <div class="product-image">
            <?php if($data->video): ?>
                <?= CHtml::link('<i class="icon-youtube-pink"></i>', "JavaScript:html5Lightbox.showLightbox(3, '" .$data->video. "', '');"); ?>
            <?php endif; ?>
            
            <?= CHtml::link(CHtml::image($data->getImageUrl(280, 350, true), CHtml::encode($data->getImageAlt()), ['title' => CHtml::encode($data->getImageTitle())]), ProductHelper::getUrl($data)); ?>
        </div>
        
        <div class="product-info-block product-title">
            <?= CHtml::link(CHtml::encode($data->getName()), ProductHelper::getUrl($data)); ?>
        </div>
        
        <div class="product-info-block product-price-more">
            <div class="product-price">
                <?= number_format($data->getResultPrice(), 0, '.', ' '); ?><i class="icon-ruble-currency-sign"></i>
            </div> 
            <div class="product-link-more">
                <?= CHtml::link('Подробнее', ProductHelper::getUrl($data), ['class' => 'btn-pink']); ?>
            </div>
        </div>
        
    </div>
</div>