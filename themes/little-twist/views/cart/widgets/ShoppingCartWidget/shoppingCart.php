<div id="cart-widget" data-cart-widget-url="<?= Yii::app()->createUrl('/cart/cart/widget');?>">
    <a href="<?= Yii::app()->createUrl('cart/cart/index'); ?>">
        <i class="icon--2"></i>
        <div class="count-product-cart"><?= Yii::app()->cart->getCount(); ?></div>
    </a>
</div>