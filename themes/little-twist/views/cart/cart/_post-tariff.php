<div class="post-container">
    <div class="flex-tariff">
        <label>Индекс почтового отделения куда будет направлен Ваш заказ:</label><b><?= $index ?></b>
    </div>
    <!--<div class="flex-tariff">
        <p><label><i class="fa fa-rub" aria-hidden="true"></i> Стоимость доставки:</label></p>
        <p> <?= $totalRateRub ?> руб.</p>
    </div>-->
    <!--<div class="flex-tariff">
        <p><strong>НДС:</strong></p>
        <p> <?= $totalVatRub ?> руб.</p>
    </div>-->
    <h4>Стоимость доставки:</h4>
    <div class="flex-tariff">
        <i class="fa fa-rub" aria-hidden="true"></i><p><?= $cost ?> руб.</p>
    </div>
    <!--<div class="flex-tariff">
        <p><strong>Минимальный срок (дней)</strong></p>
        <p> <?= $minDays ?></p>
    </div>-->
    <h4>Срок:</h4>
    <div class="flex-tariff">
        <i class="fa fa-calendar" aria-hidden="true"></i><p><?= $maxDays ?> дн.</p>
    </div>
</div>

<script>
    $('.block-delivery input:checked').attr('data-price', <?= $cost ?>);
    $('#Order_post_id').val(<?= $model->id ?>);
</script>