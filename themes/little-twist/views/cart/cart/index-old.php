    <div class="right-panel-shopcart">
        <div class="panel-shopcart-delivery">
             <?php if(!empty($deliveryTypes)): ?>
                <h3>Выберите вариант доставки:</h3>
                <fieldset class="row block-delivery">
                    <?php foreach ($deliveryTypes as $key => $delivery): ?>
                        <div class="item-delivery <?= $delivery->class_delivery; ?> col-sm-4 col-xs-12">
                            <input type="radio" name="Order[delivery_id]" id="delivery-<?= $delivery->id; ?>"
                                   value="<?= $delivery->id; ?>"
                                   data-url-sdek="<?= Yii::app()->createUrl('/order/order/saveSDEK'); ?>"
                                   data-price="<?= $delivery->price; ?>"
                                   data-free-from="<?= $delivery->free_from; ?>"
                                   data-available-from="<?= $delivery->available_from; ?>"
                                   data-separate-payment="<?= $delivery->separate_payment; ?>">
                            <label class="radio" for="delivery-<?= $delivery->id; ?>">
                                <p class="delivery-name"><?= $delivery->name; ?></p>
                                <p class="delivery-description"><?= $delivery->description; ?></p>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </fieldset>
            <?php else: ?>
                <?= Yii::t("CartModule.cart", "Delivery method aren't selected! The ordering is impossible!") ?>
            <?php endif;?>
        </div>
        <div class="panel-order">
            <?php //if ((count(Yii::app()->getComponent('cdekRepository')->getListRegionsForSelect()) > 0) &&  (count(Yii::app()->getComponent('cdekRepository')->getListCitiesForSelect()) > 0)): ?>
            <div class="panel-sdek hidden">
                    <?= $form->select2Group($order, 'region_id', [
                        'widgetOptions' => [
                            'data' => Yii::app()->getComponent('cdekRepository')->getListRegionsForSelect(),
                            'options' => [
                                'placeholder' => $order->getAttributeLabel('region_id'),
                            ],
                            'htmlOptions' => [
                                'data-url' => Yii::app()->createUrl('/order/order/getCities'),
                                'options'=> [
                                    56 => ['selected' => 'selected']
                                ]
                            ],
                        ],
                    ]); ?>

                    <div class="cities-list">
                        <?= $form->select2Group($order, 'city', [
                            'widgetOptions' => [
                                'data' => Yii::app()->getComponent('cdekRepository')->getListCitiesForSelect(),
                                'options' => [
                                    'placeholder' => $order->getAttributeLabel('city'),
                                ],
                                'htmlOptions' => [
                                    'data-url' => Yii::app()->createUrl('/order/order/getPrices'),
                                    'options'=> [
                                         //261 => ['selected' => 'selected']
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                
                    <div class="courier-field hidden">
                        <?= $form->textFieldGroup($order, 'street'); ?>
                        <?= $form->textFieldGroup($order, 'house'); ?>
                    </div>
                
            </div>

            <div class="panel-address-delivery">
                <?= $form->hiddenField($order, 'post_id'); ?>
                
                <div class="hide js-post-rf">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->textFieldGroup($order, 'post_address', [
                                'widgetOptions' => [
                                    'htmlOptions' => [
                                        'placeholder' => 'Пример: г. Оренбург, ул. Советская, д. 15',
                                        'data-address' => Yii::app()->createUrl('/cart/cart/postAddress'),
                                        'data-postoffice' => Yii::app()->createUrl('/cart/cart/searchPostoffice'),
                                        'data-info' => CJSON::encode($cartInfo),
                                    ]
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
                
                <div class="list-prices hidden"></div>
             
                <?= $form->hiddenField($order, 'sdek_id'); ?>
                <?= $form->textFieldGroup($order, 'family'); ?>
                <?= $form->textFieldGroup($order, 'name'); ?>
                <?= $form->textFieldGroup($order, 'email'); ?>
				<div class="form-group">
					<?= $form->labelEx($order, 'phone', ['class' => 'control-label']) ?>
					<?php $this->widget('CMaskedTextFieldPhone', [
                            'model' => $order,
                            'attribute' => 'phone',
                            'mask' => '+7(999)999-99-99',
                            'htmlOptions'=>[
                                'class' => 'data-mask form-control',
                                'data-mask' => 'phone',
                                'placeholder' => 'Телефон',
                                'autocomplete' => 'off'
                            ]
                        ]) ?>
					<?php echo $form->error($order, 'phone'); ?>
				</div>

            </div>
        </div>
    </div>
