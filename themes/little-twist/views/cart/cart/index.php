<?php
/* @var $this CartController */
/* @var $positions Product[] */
/* @var $order Order */
/* @var $coupons Coupon[] */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store.js');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/order.js');
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/cart-frontend.css');

$cartInfo = $order->getProductsInfo();

$this->title = Yii::t('CartModule.cart', 'Cart');
$this->breadcrumbs = [
    Yii::t("CartModule.cart", 'Catalog') => ['/store/product/index'],
    Yii::t("CartModule.cart", 'Cart')
];
?>

<script type="text/javascript">
    var yupeCartDeleteProductUrl = '<?= Yii::app()->createUrl('/cart/cart/delete/')?>';
    var yupeCartUpdateUrl = '<?= Yii::app()->createUrl('/cart/cart/update/')?>';
    var yupeCartWidgetUrl = '<?= Yii::app()->createUrl('/cart/cart/widget/')?>';
    var yupeCartEmptyMessage = '<h1 class="title-store"><?= Yii::t("CartModule.cart", "Cart is empty"); ?></h1><?= Yii::t("CartModule.cart", "There are no products in cart"); ?>';
</script>

<div class="store-container product-container" id="cart-body">
    <h1 class="title-store">Корзина товаров</h1>
<?php if (Yii::app()->cart->isEmpty()): ?>
    <?= Yii::t("CartModule.cart", "There are no products in cart"); ?>
<?php else: ?>
    <?php
        $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            [
                'type' => 'vertical',
                'action' => ['/order/order/create'],
                'id' => 'order-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'clientOptions' => [
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => false,
                    'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                    'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
                ],
                'htmlOptions' => [
                    'hideErrorMessage' => false,
                    'class' => 'order-form',
                    'data-url-delivery-types' => Yii::app()->createUrl('/order/order/deliveryTypes'),
                ]
            ]
        ); ?>

    <div class="left-panel-shopcart">
        <div class="header-shopcart">
            <div class="col-flex cart-image"><label>Модель</label></div>
            <div class="col-flex cart-title"><label>Описание</label></div>
            <div class="col-flex cart-attribute hidden-xs"><label>Атрибуты товара</label></div>
            <div class="col-flex cart-price-item hidden-xs"><label>Цена</label></div>
            <div class="col-flex cart-count-item hidden-xs"><label>Количество</label></div>
            <div class="col-flex cart-sum-item hidden-xs"><label>Итого</label></div>
            <div class="col-flex cart-button"></div>
        </div>

        <div class="body-shopcart">
            <?php foreach ($positions as $position): ?>
                <?php
                    $productModel = $position->getProductModel();
                    if (is_null($productModel)) continue;
                ?>
            <?php $positionId = $position->getId(); ?>
            <?php $productUrl = ProductHelper::getUrl($position); ?>
            
            <div class="shopcart-line-item cart-position cart-position-<?= $positionId; ?>">
                    
                <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][product_id]', $position->id); ?>
                <input type="hidden" class="position-id" value="<?= $positionId; ?>"/>

                <div class="col-flex cart-image">
                    <?= CHtml::link(CHtml::image(StoreImage::product($productModel, 100, 120)), ProductHelper::getUrl($position)); ?>
                </div>

                <div class="col-flex cart-title">
                    <span><?= $position->sku; ?></span>
                    <h4 class="shopcart-title-product">
                        <?= CHtml::link($position->name, $productUrl); ?>
                    </h4>
                </div>

                <div class="col-flex cart-attribute">
                    <?php foreach ($position->selectedVariants as $variant): ?>
                        <label><?= $variant->attribute->title; ?>: <span><?= $variant->getOptionValue(); ?></span></label>
                        <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id); ?>
                    <?php endforeach; ?>
                </div>

                <div class="col-flex cart-price-item hidden-xs">
                    <div class="position-price price-cart"><?= $position->getPrice(); ?><i class="icon-ruble-currency-sign"></i></div>
                </div>

                <div class="col-flex cart-count-item hidden-xs">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button class="btn btn-default cart-quantity-decrease" type="button" data-target="#cart_<?= $positionId; ?>">-</button>
                        </div>
                        <?= CHtml::textField('OrderProduct[' . $positionId . '][quantity]',$position->getQuantity(),['id' => 'cart_' . $positionId, 'class' => 'form-control text-center position-count']); ?>
                        <div class="input-group-btn">
                            <button class="btn btn-default cart-quantity-increase" type="button" data-target="#cart_<?= $positionId; ?>">+</button>
                        </div>
                    </div>
                </div>

                <div class="col-flex cart-sum-item">
                    <span class="position-sum-price"><?= $position->getSumPrice(); ?></span>
                </div>

                <div class="col-flex cart-button">
                    <button type="button" class="btn btn-danger cart-delete-product" data-position-id="<?= $positionId; ?>" data-product-id="<?= $position->id; ?>">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="footer-shopcart">
                 Итого: <span id="cart-total-product-count">
                <?= Yii::app()->cart->getItemsCount(); ?></span> товар(ов) на сумму <span id="cart-full-cost-with-shipping">0</span>
                <span class="ruble"> <i class="icon-ruble-currency-sign"></i></span>
            </div>
        </div>
    </div>
    
    <div class="panel-order-form">
        <h3>Заполните поля ниже:</h3>

        <?= $form->hiddenField($order, 'sdek_id'); ?>
        <?= $form->textFieldGroup($order, 'family'); ?>
        <?= $form->textFieldGroup($order, 'name'); ?>
        <?= $form->textFieldGroup($order, 'email'); ?>
        <div class="form-group">
            <?= $form->labelEx($order, 'phone', ['class' => 'control-label']) ?>
            <?php $this->widget('CMaskedTextFieldPhone', [
                    'model' => $order,
                    'attribute' => 'phone',
                    'mask' => '+7(999)999-99-99',
                    'htmlOptions'=>[
                        'class' => 'data-mask form-control',
                        'data-mask' => 'phone',
                        'placeholder' => 'Телефон',
                        'autocomplete' => 'off'
                    ]
                ]) ?>
            <?php echo $form->error($order, 'phone'); ?>
        </div>
        <?= $form->textFieldGroup($order, 'city',
        [
            'widgetOptions' => [
                'htmlOptions' => [
                    'placeholder' => 'Введите город или населенный пункт',
                ]
            ] 
        ]); ?>

        <?= $form->hiddenField($order, 'zipcode'); ?>
        <?= $form->hiddenField($order, 'longitude'); ?>
        <?= $form->hiddenField($order, 'latitude'); ?>
        <?= $form->hiddenField($order, 'pvz_address'); ?>
        <?= $form->hiddenField($order, 'tariff_id'); ?>

        <div class="panel-shopcart-delivery">
            <?php /*if(!empty($order->delivery_id) && !empty(Yii::app()->request->cookies['order']->value)): ?>
                <?php $this->renderPartial('application.modules.order.views.order._delivery-types', [ 'deliveryTypes' => $deliveryTypes, 'deliveryTariffs' => Order::model()->getTariffMin($order), 'order' => $order ], false, true); ?>
            <?php endif;*/ ?>
        </div>

        <div class="panel-shopcart-delivery-data">

        </div>
        
        <div class="field-address hidden">
            <h3>Заполните адрес доставки:</h3>
            <?= $form->textFieldGroup($order, 'street'); ?>
            <?= $form->textFieldGroup($order, 'house'); ?>
            <?= $form->textFieldGroup($order, 'apartment'); ?>
        </div>
 
        <div class="total-cost-block hidden">
            <label>
                Полная стоимость с доставкой: <span id="total-cost-delivery"></span> <i class="icon-ruble-currency-sign"></i>
            </label>
        </div>
        <button type="submit" class="btn btn-order hidden js-button-order">Оформить заказ</button>
        
    </div>

    <?php $this->endWidget(); ?>
<?php endif; ?>
</div>