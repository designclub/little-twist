<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="content-site">
    <div class="page-txt page-site">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <h1><?= $model->title; ?></h1>
            </div>
        </div>
    	<div class="row">
    		<div class="col-sm-6 col-xs-12">
        		<?= $model->body; ?>
    		</div>
    		<div class="col-sm-6 col-xs-12">
    			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2468.7343526132536!2d55.196892615978996!3d51.77446339896181!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x417bf7636472d2d5%3A0x44b0bf6517979ff8!2z0KLQpiDCq9CQ0YDQvNCw0LTQsCDQmtCw0L_QuNGC0LDQu8K7!5e0!3m2!1sru!2sru!4v1569479080188!5m2!1sru!2sru" width="570" height="700" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    		</div>
    	</div>
    </div>
</div>