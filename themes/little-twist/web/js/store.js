$(document).ajaxError(function () {
    $('#notifications').notify({message: {text: 'Произошла ошибка =('}, 'type': 'danger'}).show();
});

$(document).ready(function () {
    var cartWidgetSelector = '#shopping-cart-widget';

    /*страница продукта*/
    var priceElement = $('#result-price'); //итоговая цена на странице продукта
    var basePrice = parseFloat($('#base-price').val()); //базовая цена на странице продукта
    var quantityElement = $('#product-quantity');

    /*корзина*/
    var shippingCostElement = $('#cart-shipping-cost');
    var cartFullCostElement = $('#cart-full-cost');
    var cartFullCostWithShippingElement = $('#cart-full-cost-with-shipping');
    var cartFullCostWithDeliveryElement = $('#total-cost-delivery');

    function showNotify(element, result, message) {
        if ($.isFunction($.fn.notify)) {
            $("#notifications").notify({message: {text: message}, 'type': result}).show();
        }
    }

    function updatePrice(){
        var _basePrice = basePrice;
        var variants = [];
        var varElements = $('select[name="ProductVariant[]"]');
        /* выбираем вариант, меняющий базовую цену максимально*/
        var hasBasePriceVariant = false;
        $.each(varElements, function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                switch (variant.type) {
                    case 2: // base price
                        // еще не было варианта
                        if (!hasBasePriceVariant) {
                            _basePrice = variant.amount;
                            hasBasePriceVariant = true;
                        }
                        else {
                            if (_basePrice < variant.amount) {
                                _basePrice = variant.amount;
                            }
                        }
                        break;
                }
            }
        });
        var newPrice = _basePrice;
        $.each(varElements, function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                variants.push(variant);
                switch (variant.type) {
                    case 0: // sum
                        newPrice += variant.amount;
                        break;
                    case 1: // percent
                        newPrice += _basePrice * ( variant.amount / 100);
                        break;
                }
            }
        });

        priceElement.html(parseFloat(newPrice.toFixed(2)));
    }

    $('select[name="ProductVariant[]"]').change(function () {
        updatePrice();
    });

    $('.product-quantity-increase').click(function () {
        quantityElement.val(parseInt(quantityElement.val()) + 1);
    });

    $('.product-quantity-decrease').click(function () {
        if (parseInt(quantityElement.val()) > 1) {
            quantityElement.val(parseInt(quantityElement.val()) - 1);
        }
    });

    quantityElement.change(function (event) {
        var el = $(this);
        quantity = parseInt(el.val());

        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
        }

        el.val(quantity);
    });

    function updateCartWidget() {
        $(cartWidgetSelector).load($('#cart-widget').data('cart-widget-url'));
    }

    $('#add-product-to-cart').click(function (e) {
        e.preventDefault();
        var button = $(this);
        button.button('loading');
        var form = $(this).parents('form');
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            url: form.attr('action'),
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                showNotify(button, data.result ? 'success' : 'danger', data.data);
            }
        }).always(function () {
            button.button('reset');
        });
    });

    $('body').on('click', '.quick-add-product-to-cart', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'Product[id]': el.data('product-id')};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: el.data('cart-add-url'),
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                showNotify(el, data.result ? 'success' : 'danger', data.data);
            }
        });
    });


    /*cart*/
    function getCoupons(){
        var coupons = [];
        $.each($('.coupon-input'), function (index, elem) {
            var $elem = $(elem);
            coupons.push({
                code: $elem.data('code'),
                name: $elem.data('name'),
                value: $elem.data('value'),
                type: $elem.data('type'),
                min_order_price: $elem.data('min-order-price'),
                free_shipping: $elem.data('free-shipping')
            })
        });
        return coupons;
    }

    function updatePositionSumPrice(tr) {
        var count = parseInt(tr.find('.position-count').val());
        var price = parseFloat(tr.find('.position-price').text());
        tr.find('.position-sum-price').html(price * count);
        updateCartTotalCost();
    }

    function changePositionQuantity(productId, quantity) {
        var data = {'quantity': quantity, 'id': productId};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: yupeCartUpdateUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
            }
        });
    }

    $('.cart-quantity-increase').click(function () {
        var target = $($(this).data('target'));
        target.val(parseInt(target.val()) + 1).trigger('change');
    });

    $('.cart-quantity-decrease').click(function () {
        var target = $($(this).data('target'));
        if (parseInt(target.val()) > 1) {
            target.val(parseInt(target.val()) - 1).trigger('change');
        }
    });

    $('.cart-delete-product').click(function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'id': el.attr('data-position-id')};
        
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: yupeCartDeleteProductUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                updateDeliveryData();
                if (data.result) {
                    $('.cart-position-'+el.attr('data-position-id')).remove();
                    updateCartTotalCost();
                    if ($('.cart-position').length > 0) {
                        updateCartWidget();
                    } else {
                        $('#cart-body').html(yupeCartEmptyMessage);
                    }
                }
            }
        });
    });

    $('.position-count').change(function () {
        var tr = $(this).parents('tr');
        var itemLine = $(this).parents('.shopcart-line-item');

        var quantity = parseInt(tr.find('.position-count').val());
        var quantity = parseInt(itemLine.find('.position-count').val());
        var productId = tr.find('.position-id').val();
        var productId = itemLine.find('.position-id').val();
        
        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
            tr.find('.position-count').val(quantity);
            itemLine.find('.position-count').val(quantity);
        }

        updatePositionSumPrice(tr);
        updatePositionSumPrice(itemLine);
        changePositionQuantity(productId, quantity);
        updateDeliveryData();
    });

    function getCartTotalCost() {
        var cost = 0;
        $.each($('.position-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });
        var delta = 0;
        var coupons = getCoupons();
        $.each(coupons, function (index, el) {
            if (cost >= el.min_order_price) {
                switch (el.type) {
                    case 0: // руб
                        delta += parseFloat(el.value);
                        break;
                    case 1: // %
                        delta += (parseFloat(el.value) / 100) * cost;
                        break;
                }
            }
        });

        return delta > cost ? 0 : cost - delta;
    }

    function updateCartTotalCost() {
        cartFullCostElement.html(getCartTotalCost());
        refreshDeliveryTypes();
        updateShippingCost();
        updateFullCostWithShipping();
    }

    function refreshDeliveryTypes() {
        var cartTotalCost = getCartTotalCost();
        $.each($('input[name="Order[delivery_id]"]'), function (index, el) {
            var elem = $(el);
            var availableFrom = elem.data('available-from');

            if (availableFrom.length && parseFloat(availableFrom) >= cartTotalCost) {
                if (elem.prop('checked')) {
                    checkFirstAvailableDeliveryType();
                }
                elem.prop('disabled', true);
            } else {
                elem.prop('disabled', false);
            }
        });
    }

    function checkFirstAvailableDeliveryType() {
        $('input[name="Order[delivery_id]"]:not(:disabled):first').prop('checked', true);
    }


    function getShippingCost() {
        var cartTotalCost = getCartTotalCost();
        var coupons = getCoupons();
        var freeShipping = false;
        $.each(coupons, function (index, el) {
            if (el.free_shipping && cartTotalCost >= el.min_order_price) {
                freeShipping = true;
            }
        });
        if (freeShipping) {
            return 0;
        }
        var selectedDeliveryType = $('input[name="Order[delivery_id]"]:checked');
        if (!selectedDeliveryType[0]) {
            return 0;
        }
        if (parseInt(selectedDeliveryType.data('separate-payment')) || parseFloat(selectedDeliveryType.data('free-from')) <= cartTotalCost) {
            return 0;
        } else {
            return parseFloat(selectedDeliveryType.attr('data-price'));
        }
    }

    function updateShippingCost() {
        shippingCostElement.html(getShippingCost());
        updateFullCostWithShipping();
    }

    function updateFullCostWithShipping() {
        cartFullCostWithShippingElement.html(getShippingCost() + getCartTotalCost());
        cartFullCostWithDeliveryElement.html(getShippingCost() + getCartTotalCost());
    }

    refreshDeliveryTypes();
    //checkFirstAvailableDeliveryType();
    //updateFullCostWithShipping();
    //updateCartTotalCost();

    function updateAllCosts() {
        updateCartTotalCost();
    }
    
    function updateDeliveryCost(){
        var selectedDeliveryType = $('input[name="Order[delivery_id]"]:checked').data('price');
        if(!isNaN(parseFloat(selectedDeliveryType))){
            cartFullCostWithDeliveryElement.html(parseFloat(selectedDeliveryType) + getCartTotalCost());
        }
    }

    checkFirstAvailableDeliveryType();
    updateAllCosts();

    $('#start-payment').on('click', function () {
        $('.payment-method-radio:checked').parents('.payment-method').find('form').submit();
    });

    $('body').on('click', '.clear-cart', function (e) {
        e.preventDefault();
        var data = {};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: '/coupon/clear',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
            }
        });
    });

    $('#add-coupon-code').click(function (e) {
        e.preventDefault();
        var code = $('#coupon-code').val();
        var button = $(this);
        if (code) {
            var data = {'code': code};
            data[yupeTokenName] = yupeToken;
            $.ajax({
                url: '/coupon/add',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        window.location.reload();
                    }
                    showNotify(button, data.result ? 'success' : 'danger', data.data.join('; '));
                }
            });
            $('#coupon-code').val('');
        }
    });

    $('.coupon .close').click(function (e) {
        e.preventDefault();
        var code = $(this).siblings('input[type="hidden"]').data('code');
        var data = {'code': code};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: '/coupon/remove',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                showNotify(this, data.result ? 'success' : 'danger', data.data);
                if (data.result) {
                    updateAllCosts();
                }
            }
        });
    });

    $('#coupon-code').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $('#add-coupon-code').click();
        }
    });

    $('.order-form').submit(function () {
        $(this).find("button[type='submit']").prop('disabled', true);
    });

    $('body').on('click', '.attr-panel', function(e){
        e.preventDefault();
        var idBlock = $(this).attr('data-id');
        $('#'+idBlock).toggleClass('hidden');
        $(this).toggleClass('active');
    });
    
    $(document).on('click', '.image-thumb a', function(e){
		e.preventDefault();
		var idProduct = $(this).attr('data-id');
		$('.product-image a img[data-id=' +idProduct+']').attr('src', $(this).attr('href'));
	});

	$(document).on('mouseenter', '.image-thumb', function(e){
		e.preventDefault();
		var idProduct = $(this).children('a').attr('data-id');
		$('.product-image a img[data-id=' +idProduct+']').attr('src', $(this).children('a').attr('href'));
	});

	$(document).on('mouseleave', '.image-thumb', function(e){
		e.preventDefault();
		var idProduct = $(this).children('a').attr('data-id');
		$('.product-image a img[data-id=' +idProduct+']').attr('src', $('.product-image a img[data-id=' +idProduct+']').attr('data-src'));
	});

    $(document).on("mouseenter", "[data-src-images]", function (){
        var e = $(this).data("src-images"),
            t = $(this).parents('.product-image').find(".img-main-hover"),
            i = $(this).siblings();
        t.attr("src", e), i.removeClass("active"), $(this).addClass("active"), e = null, t = null, i = null
    });
        
        
/*    $('#order-form').change(function(){
        var form = $(this);
        var region = form.find('#Order_region_id').val();
        var city = form.find('#Order_city').val();
        var delivery = form.find('input[name="Order[delivery_id]"]:checked');
        var tariff = form.find('input[name="Order[tariff_id]"]:checked').val();
        var pvz = form.find('input[name="Order[pvz_id]"]:checked').val();
        var sdekUrl = delivery.data('url-sdek');
        var deliveryId = delivery.val();
        
        if(deliveryId == 2 || deliveryId == 4){
            $.ajax({
                url: sdekUrl,
                type: 'post',
                data: form.serialize(),
                dataType: 'json',
                success: function (data){
                    $('#Order_sdek_id').val(data);
                }
            });
        }
        
    });*/
    
/**
* Delivery Function's
*
*
*/

    $('input[name="Order[delivery_id]"]').change(function () {
        updateShippingCost();
    });
    
    function updateDeliveryData(){
        
        var form = $('#order-form');
        var url = '/order/updateDeliveryData';
        
        $.ajax({
            url: url,
            type: 'post',
            data: form.serialize(),
            dataType: 'html',
            beforeSend: function () {
                $('.lds-roller').removeClass('hidden');
            },
            complete: function(data){
                $('.lds-roller').addClass('hidden');
            },
            success: function (data) {
                $('.panel-shopcart-delivery').html(data);
                updateDeliveryCost();
            }
        });
    }    
    
    $(document).on('change', 'input:radio[name="Order[delivery_id]"]', function(){
        
        var deliveryType = $(this).val();
        var url = $(this).data('url');
        
        var form = $('#order-form');
        
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            dataType: 'html',
            beforeSend: function () {
                $('.lds-roller').removeClass('hidden');
            },
            complete: function(data){
                $('.lds-roller').addClass('hidden');
            }
        });
        
        if(deliveryType != 1 && deliveryType == 2){
            
            $('.field-address').addClass('hidden');
            
            $.ajax({
                url: url,
                type: 'post',
                data: form.serialize(),
                dataType: 'html',
                beforeSend: function () {
                    $('.lds-roller').removeClass('hidden');
                },
                complete: function(data){
                    $('.lds-roller').addClass('hidden');
                    $('.panel-shopcart-delivery-data').removeClass('hidden');
                    $('.js-button-order').removeClass('hidden');
                    $('html, body').animate({
                        scrollTop: $(".panel-shopcart-delivery-data").offset().top
                    }, 300);
                },
                success: function (data) {
                    $('.panel-shopcart-delivery-data').html(data);
                }
            });
        }
        else{
            $('.panel-shopcart-delivery-data').addClass('hidden');
            $('.field-address').removeClass('hidden');
            $('.js-button-order').removeClass('hidden');
            $('html, body').animate({
                scrollTop: $(".field-address").offset().top
            }, 300);
        }
        
        if(deliveryType == 1){
            $('.panel-shopcart-delivery-data').addClass('hidden');
            $('.field-address').addClass('hidden');
            $('.js-button-order').removeClass('hidden');
        }
        
        $('.total-cost-block').removeClass('hidden');
        
        $('#Order_tariff_id').val($(this).data('tariff'));
        
        updateShippingCost();
        
        return false;
    });

        
    $(document).on('change', 'input:radio[name="Order[pvz_id]"]', function(){
        $('#Order_pvz_address').val($(this).data('address'));
    });
    
/*    $('#Order_region_id').change(function(){
        var idRegion = $(this).val();
        var url = $(this).attr('data-url');
        var data = $('#order-form').serialize();
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data){
                var select = $('#Order_city');
                select.find('option').remove();
                
                $.each(data.results, function($key, $value){
                    var newOption = new Option($value, $key, false, false);
                    select.append(newOption);
                });
                
                select.trigger('change');
            }
        });
    });*/

    function getDeliveryTypes(){
        var form = $('#order-form');
        var data = form.serialize();
        var url = form.data('url-delivery-types');
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            dataType: 'html',
            beforeSend: function () {
                $('.lds-roller').removeClass('hidden');
            },
            complete: function (data) {
				$('.lds-roller').addClass('hidden');
                refreshDeliveryTypes();
			},
            success: function (data) {
                $('.panel-shopcart-delivery').html(data);
            }
        });
    };

    $(document).delegate('.js-tarif-item', 'click', function() {
        var item = $(this);
        var price = parseInt(item.data('price'));
        var container = item.parents('.conteiner-tarif');
/*        container
            .find('.js-tarif-item')
            .removeClass('active');*/

        $('.block-delivery input:checked').attr('data-price', price);
        updateShippingCost();

        //item.addClass('active');

        return false;
    });
    

    /*$('#Order_post_address').on('change', function() {
        var elem = this;
        var address = elem.value;
        var parent = $(elem).parent('.form-group');
        var addressUrl = $(elem).data('address');
        var postofficeUrl = $(elem).data('postoffice');
        var info = $(elem).data('info');
        var deliveryType = $('#order-form').find('input[name="Order[delivery_id]"]:checked').val();

        $.ajax({
            url: addressUrl,
            data: {'address': address},
            dataType: 'json',
            success: function(data) {
                parent.removeClass('has-success');
                parent.removeClass('has-error');

                if ('address' in data) {
                    elem.value = data.address;
                    $(elem).attr('data-json', data.data);
                }

                //console.log(data.data['validation-code']);
                if (data.data['validation-code'] === 'VALIDATED' || data.data['validation-code'] === 'NOT_VALIDATED_HAS_ASSUMPTION') {
                    // Если адрес удалось нормализовать
                    parent.addClass('has-success'); // подсветить форму зеленым
                    // поиск почтовых отделений по адресу
                    $.ajax({
                        url: postofficeUrl,
                        data: {'address': data.address, info: info, deliveryType: deliveryType},
                        dataType: 'html',
                        success: function(data) {
                            $('.list-prices').html(data);
                            updateShippingCost();
                        }
                    })
                } else {
                    parent.addClass('has-error');
                }
            }
        })
        return false;
    });*/

    /**
    *
    * hints DADATA service
    * suggestions settlements
    *
    */

    // Замените на свой API-ключ
    var token = "85986aed4115e317a88a5288e91de4a3c3b5f0bb";
    var $city = $("#Order_city");
    var $street = $("#Order_street");
    var $house  = $("#Order_house");
    var type  = "ADDRESS";
 
    // удаляет районы города и всё с 65 уровня
    function removeNonCity(suggestions) {
        return suggestions.filter(function(suggestion) {
            return suggestion.data.fias_level !== "5" && suggestion.data.fias_level !== "65";
        });
    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function(n){return n}).join(separator);
    }

    function cityToString(address) {
      return join([
          join([address.city_type, address.city], " "),
          join([address.settlement_type, address.settlement], " ")
        ]);
    }

    
    function setDataAddress(address) {
        $('#Order_zipcode').val(address.postal_code);
        $('#Order_longitude').val(address.geo_lon);
        $('#Order_latitude').val(address.geo_lat);
    }
    
    function getDataDelviery(suggestion) {
        var address = suggestion.data;
        
        $('.panel-shopcart-delivery').removeClass('hidden');
        
        setDataAddress(address);
        getDeliveryTypes();
    }
    
  
    if($city.length > 0 && $street .length > 0 && $house .length > 0) {
        
        //Ограничиваем область поиска от города до населенного пункта
        $city.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "city-settlement",
            onSuggestionsFetch: removeNonCity,
            onSelect: getDataDelviery
        });

        // Определяем город по IP-адресу
/*        $city.suggestions().getGeoLocation()
            .done(function(locationData) {
                var sgt = {
                value: null,
                date: locationData
            };
            //console.log(locationData);
            $city.suggestions().setSuggestion(sgt);
            $city.val(cityToString(locationData));
        });*/

        // улица
        $street.suggestions({
            token: token,
            type: type,
            hint: false,
            bounds: "street",
            constraints: $city,
            count: 15
        });

        // дом
        $house.suggestions({
            token: token,
            type: type,
            hint: false,
            noSuggestionsHint: false,
            bounds: "house",
            constraints: $street
        });
    }
});

