<h3 class="text-center"><label>Политика обработки  персональных данных </label>
</h3>
<h4 class="text-center"><label>Индивидуальный  предприниматель Загидуллина Анастасия Сергеевна</label></h4>
<p>
	1. Общие положения<br>
	1.1. Настоящая  Политика обработки персональных данных определяет порядок и условия,  применяемые  <label>ИП Загидуллиной А.С. </label>, (далее – «Продавец») при обработке  персональных данных (далее – «Политика»).<br>
	Настоящая Политика размещается на Сайте продавца в  телекоммуникационной сети «Интернет» по адресу: <a href="http://www.little-twist.ru">www.little-twist.ru</a> <br>
	Изменение положений настоящей Политики осуществляется  путем размещения на Сайте продавца новой редакции Политики обработки  персональных данных.<br>
	1.2. Под персональными данными понимается любая  информация, относящаяся прямо или косвенно к определенному или определяемому  физическому лицу (гражданину), т.е. к такой информации, в частности, относятся:  фамилия, имя, отчество, электронная почта, номер телефона, ссылка на  персональный сайт или соцсети, ip адрес (далее – «Персональные данные»).<br>
	1.3. Под обработкой Персональных данных понимается любое  действие (операция) или совокупность действий (операций) с Персональными  данными, совершаемых с использованием средств автоматизации и/или без  использования таких средств. К таким действиям (операциям) относятся: сбор,  запись, систематизация, накопление, хранение, уточнение (обновление,  изменение), извлечение, использование, передача (распространение,  предоставление, доступ), обезличивание, блокирование, удаление, уничтожение  Персональных данных.<br>
	1.4. Под безопасностью Персональных данных понимается их  защищенность от неправомерного и/или несанкционированного доступа к ним,  уничтожения, изменения, блокирования, копирования, предоставления,  распространения Данных, а также от иных неправомерных действий в отношении  Данных.<br>
	1.5. Субъектами персональных данных, обрабатываемых  Продавцом Компанией, являются: покупатели и иные посетители сайтов,  принадлежащих Продавцу: <a href="http://www.little-twist.ru">www.little-twist.ru</a> <br>
	1.6. Продавец осуществляет обработку Персональных данных  субъектов в следующих целях:
</p>
<ul>
	<li>предоставления информации по товарам/услугам, проходящим  акциям и специальным предложениям;</li>
	<li>анализа качества предоставляемого Компанией сервиса и  улучшению качества обслуживания клиентов Компании;</li>
	<li>информирования о статусе заказа;</li>
	<li>исполнения договора, в т.ч. договора купли-продажи, в.т.ч.  заключенного дистанционным способом на Сайте, возмездного оказания услуг;  предоставления услуг, а также учета оказанных покупателям услуг для  осуществления взаиморасчетов;</li>
	<li>доставки заказанного товара покупателю, совершившему заказ  на Сайте продавца, возврата товара.</li>
</ul>
<p>2.Принципы и условия обработки Персональных данных:<br>
	2.1. При обработке Продавец придерживается следующих  принципов обработки Персональных данных:
</p>
<ul>
	<li>обработка осуществляется на законной и справедливой  основе;</li>
	<li>Персональные данные не раскрываются третьим лицам и не  распространяются без согласия субъекта Персональных данных, за исключением  случаев, требующих их раскрытия по запросу уполномоченных государственных  органов, судопроизводства;</li>
	<li>определение конкретных законных целей до начала обработки  (в т.ч. сбора) Персональных данных;</li>
	<li>ведется сбор только тех Персональных данных, которые  являются необходимыми и достаточными для заявленной цели обработки;</li>
	<li>объединение баз данных, содержащих Персональные данные,  обработка которых осуществляется в целях, несовместимых между собой не  допускается;</li>
	<li>обработка Персональных данных ограничивается достижением  конкретных, заранее определенных и законных целей;</li>
	<li>обрабатываемые персональные данные подлежат уничтожению  или обезличиванию по достижению целей обработки или в случае утраты  необходимости в достижении этих целей, если иное не предусмотрено федеральным  законом.</li>
</ul>
<p>2.2. Продавец может включать Персональные данные субъектов  в общедоступные источники персональных данных, при этом Продавец берет  письменное согласие субъекта на обработку его персональных данных, либо путем  выражения согласия через форму Сайта продавца (чекбокс), нажатием которого  субъект персональных данных выражает свое согласие.<br>
	2.3. Продавец не осуществляет обработку Персональных  данных, касающихся расовой, национальной принадлежности, политических взглядов,  религиозных, философских и иных убеждений, интимной жизни, членства в  общественных объединениях, в том числе в профессиональных союзах.<br>
	2.4. Биометрические Персональные данные (сведения, которые  характеризуют физиологические и биологические особенности человека, на  основании которых можно установить его личность и которые используются  оператором для установления личности субъекта Данные) Продавцом не  обрабатываются.<br>
	2.5. Продавец может осуществлять трансграничную передачу  Персональных данных. продавец подтверждает, что иностранным государством, на  территорию которого осуществляется передача персональных данных, обеспечивается  адекватная защита прав субъектов персональных данных в соответствии с уровнем  безопасности, определенным Конвенцией Совета Европы о защите физических лиц при  автоматизированной обработке персональных данных.<br>
	2.6. В случаях, установленных законодательством Российской  Федерации, Продавец вправе осуществлять передачу Персональных данных третьим  лицам (федеральной налоговой службе, государственному пенсионному фонду и иным  государственным органам) в случаях, предусмотренных законодательством  Российской Федерации.<br>
	2.7. Продавец вправе поручить обработку Персональных  данных субъектов таких данных третьим лицам с согласия субъекта Персональных  данных, на основании заключаемого с этими лицами договора, в том числе при  заключении договора розничной купли-продажи дистанционным способом на основании  публичной оферты и политики обработки персональных данных размещенных на Сайте  продавца.<br>
	2.8. Лица, осуществляющие обработку Персональных данных на  основании заключаемого с Продавцом договора (поручения оператора), обязуются  соблюдать принципы и правила обработки и защиты Персональных данных,  предусмотренные Законом. Для каждого третьего лица в договоре определяются  перечень действий (операций) с Персональными данными, которые будут совершаться  третьим лицом, осуществляющим обработку Персональных данных, цели обработки,  устанавливается обязанность такого лица соблюдать конфиденциальность и  обеспечивать безопасность Персональных данных при их обработке, указываются  требования к защите обрабатываемых Персональных данных в соответствии с Законом.<br>
	2.9. В целях исполнения требований действующего  законодательства Российской Федерации и своих договорных обязательств обработка  Персональных данных Продавцом осуществляется как с использованием, так и без  использования средств автоматизации.<br>
	2.10. Продавец не принимает никаких решений на основании  исключительно автоматизированной обработки Персональных данных, порождающих  юридические последствия в отношении субъекта Персональных данных или иным  образом затрагивающих его права и законные интересы, за исключением случаев,  предусмотренных законодательством Российской Федерации.<br>
	3. Права и обязанности субъектов Персональных данных и  Продавца при их обработке.<br>
	3.1. Субъект, Персональные данные которого обрабатываются  Продавцом, имеет право:<br>
	Получать от Продавца:
</p>
<ul>
	<li>подтверждение факта обработки Персональных данных и  сведения о наличии Персональных данных, относящихся к соответствующему субъекту  Персональных данных;</li>
	<li>сведения о правовых основаниях и целях обработки  Персональных данных;</li>
	<li>сведения о применяемых Продавцом способах обработки  Персональных данных;</li>
	<li>сведения о наименовании и местонахождении Продавца;</li>
	<li>сведения о лицах (за исключением работников Продавца),  которые имеют доступ к Персональным данным или которым могут быть раскрыты  Персональные данные на основании договора с Продавцом или на основании  федерального закона;</li>
	<li>перечень обрабатываемых Персональных данных, относящихся к  субъекту данных, и информацию об источнике их получения, если иной порядок  предоставления таких данных не предусмотрен федеральным законом;</li>
	<li>сведения о сроках обработки Персональных данных, в том  числе о сроках их хранения; сведения о порядке осуществления субъектом  Персональных данных прав, предусмотренных Законом;</li>
	<li>наименование (Ф.И.О.) и адрес лица, осуществляющего  обработку Персональных данных по поручению Компании; иные сведения,  предусмотренные Законом или другими нормативно-правовыми актами Российской  Федерации;</li>
</ul>
<p>Требовать от Продавца:
</p>
<ul>
	<li>уточнения своих Персональных данных, их блокирования или  уничтожения в случае, если Данные являются неполными, устаревшими, неточными,  незаконно полученными или не являются необходимыми для заявленной цели  обработки;</li>
	<li>отозвать свое согласие на обработку Персональных данных в  любой момент;</li>
	<li>требовать устранения неправомерных действий Продавца в  отношении его Персональных данных;</li>
	<li>обжаловать действия или бездействие Продавца в Федеральную  службу по надзору в сфере связи, информационных технологий и массовых  коммуникаций (Роскомнадзор) или в судебном порядке в случае, если субъект  Персональных данных считает, что Продавец осуществляет обработку его  Персональных данных с нарушением требований Федерального закона РФ от 27 июля  2006 года № 152-ФЗ «О персональных данных» (далее – «Закон») или иным образом  нарушает его права и свободы;</li>
	<li>на защиту своих прав и законных интересов, в том числе на  возмещения убытков и/или компенсацию морального вреда в судебном порядке.</li>
</ul>
<p>3.2. Продавец в процессе обработки Персональных данных  обязан:<br>
	- предоставлять субъекту Персональных данных по его  запросу информацию, касающуюся обработки его Персональные Данные, либо на  законных основаниях предоставить отказ в течение тридцати дней с момента (даты)  получения запроса субъекта Персональных данных или его представителя;<br>
	- разъяснить субъекту Персональных данных юридические  последствия отказа предоставить Персональные данные, если их предоставление  является обязательным в соответствии с федеральным законом;<br>
	- до начала обработки Персональных данных (если данные  получены не от субъекта Персональных данных) предоставить субъекту Персональных  данных следующую информацию, за исключением случаев, предусмотренных частью 4  статьи 18 Закона:<br>
	1) наименование либо фамилия, имя, отчество и адрес  Продавца или его представителя;<br>
	2) цель обработки Персональных данных и ее правовое  основание;<br>
	3) предполагаемые пользователи Персональных данных;<br>
	4) установленные Законом права субъектов Персональных  данных;<br>
	5)источник получения Персональных данных.<br>
	Продавец также обязан:
</p>
<ul>
	<li>принимать необходимые правовые, организационные и  технические меры или обеспечивать их принятие для защиты Персональных данных от  неправомерного или случайного доступа к ним, уничтожения, изменения,  блокирования, копирования, предоставления, распространения Персональных данных,  а также от иных неправомерных действий в отношении Персональных данных;</li>
	<li>опубликовать в сети Интернет и обеспечить неограниченный  доступ с использованием сети Интернет к документу, определяющему его политику в  отношении обработки Персональных данных, к сведениям о реализуемых требованиях  к защите Персональных данных;</li>
	<li>предоставить субъектам Персональных данных и/или их  представителям безвозмездно возможность ознакомления с Персональными данными  при обращении с соответствующим запросом в течение 30 дней с момента (даты)  получения подобного запроса;</li>
	<li>осуществить блокирование неправомерно обрабатываемых  Персональных данных, относящихся к субъекту Персональных данных, или обеспечить  их блокирование (если обработка Персональных данных осуществляется другим  лицом, действующим по поручению Продавца) с момента обращения или получения  запроса на период проверки, в случае выявления неправомерной обработки  Персональных данных при обращении субъекта Персональных данных или его  представителя либо по запросу субъекту Персональных данных или его  представителя либо уполномоченного органа по защите прав субъектов персональных  данных;</li>
	<li>уточнить Персональные данные либо обеспечить их уточнение  (если обработка данных осуществляется другим лицом, действующим по поручению  Продавца Компании) в течение 7 рабочих дней со дня представления сведений и  снять блокирование Персональных данных, в случае подтверждения факта неточности  Персональных данных на основании сведений, представленных субъектом  Персональных данных или его представителем;</li>
	<li>прекратить неправомерную обработку Персональных данных или  обеспечить прекращение неправомерной обработки Персональных данных лицом,  действующим по поручению Продавца, в случае выявления неправомерной обработки  Персональных данных, осуществляемой Продавцом или лицом, действующим на  основании договора с Продавцом, в срок, не превышающий 3 рабочих дней с момента  (даты) этого выявления; </li>
	<li>прекратить обработку Персональных данных или обеспечить ее  прекращение (если обработка данных осуществляется другим лицом, действующим по  договору с Продавцом) и уничтожить Персональные данные или обеспечить их  уничтожение (если обработка данных осуществляется другим лицом, действующим по  договору с Продавцом) по достижения цели обработки Персональных данных, если  иное не предусмотрено договором, стороной которого, выгодоприобретателем или  поручителем по которому является субъект Персональных данных;</li>
	<li>прекратить обработку Персональных данных или обеспечить ее  прекращение и уничтожить Персональных данных или обеспечить их уничтожение в  случае отзыва субъектом Персональных данных согласия на обработку Персональных  данных, если Продавец не вправе осуществлять обработку Персональных данных без  согласия субъекта данных;</li>
	<li>вести журнал учета обращений субъектов Персональных  Данных, в котором должны фиксироваться запросы субъектов Персональных данных на  получение данных, а также факты предоставления Персональных данных по этим  запросам.</li>
</ul>
<p>4. Требования к защите Персональных данных, выполняемые  Продавцом<br>
	4.1. Продавец при обработке Персональных данных принимает  необходимые правовые, организационные и технические меры для защиты данных от  неправомерного и/или несанкционированного доступа к ним, уничтожения,  изменения, блокирования, копирования, предоставления, распространения  Персональных данных, а также от иных неправомерных действий в их отношении.<br>
	4.1. К таким мерам в соответствии с Законом, в частности,  относятся:
</p>
<ul>
	<li>назначение лица, ответственного за организацию обработки  Персональных данных, и лица, ответственного за обеспечение безопасности  Персональных данных;</li>
	<li>разработка и утверждение локальных актов по вопросам  обработки и защиты Персональных  данных ;</li>
	<li>применение правовых, организационных и технических мер по обеспечению  безопасности Персональных данных:</li>
	<li>определение  угроз безопасности Персональных данных при их обработке в информационных  системах персональных данных;</li>
	<li>применение  организационных и технических мер по обеспечению безопасности Персональных  данных при их обработке в информационных системах персональных данных,  необходимых для выполнения требований к защите Персональных данных, исполнение  которых обеспечивает установленных Правительством Российской Федерации уровни  защищенности Персональных данных;</li>
	<li>применение  прошедших в установленном порядке процедуру оценки соответствия средств защиты  информации и оценка эффективности принимаемых мер по обеспечению безопасности  Персональных данных до ввода в эксплуатацию информационной системы персональных  данных;</li>
	<li>учет  машинных носителей Персональных данных, если их хранение осуществляется на  машинных носителях, а также обнаружение фактов несанкционированного доступа к  Персональным данным и принятие мер по недопущению подобных инцидентов в  дальнейшем;</li>
	<li>восстановление  Персональных данных, модифицированных или уничтоженных вследствие  несанкционированного доступа к ним;</li>
	<li>установление  правил доступа к Персональным данным, обрабатываемым в информационной системе  персональных данных, а также обеспечение регистрации и учета всех действий,  совершаемых с Персональными данными в информационной системе персональных  данных.</li>
	<li>контроль  за принимаемыми мерами по обеспечению безопасности Персональных данных и  уровнем защищенности информационных систем персональных данных;</li>
	<li>оценка  вреда, который может быть причинен субъектам Персональным данным в случае  нарушения требований Закона, соотношение указанного вреда и принимаемых  Продавцом мер, направленных на обеспечение выполнения обязанностей,  предусмотренных Законом;</li>
	<li>соблюдение  условий, исключающих несанкционированный доступ к материальным носителям  Персональных данных и обеспечивающих сохранность Данных;</li>
	<li>ознакомление  работников Продавца, непосредственно осуществляющих обработку Персональных  данных, с положениями законодательства Российской Федерации о персональных  данных, в том числе с требованиями к защите персональных данных, локальными  актами по вопросам обработки и защиты персональных данных, и обучение  работников Продавца.</li>
</ul>
<p>5. Сроки обработки (хранения) Персональных данных<br>
	5.1. Сроки обработки (хранения) Персональных данных  определяются исходя из целей обработки данных, в соответствии со сроком  действия договора с субъектом Персональных данных, требованиями федеральных  законов, требованиями операторов Персональных данных, по поручению которых  Продавец осуществляет обработку Персональных данных, основными правилами работы  архивов организаций, сроками исковой давности.<br>
	;5.2. Персональные данные, срок обработки (хранения)  которых истек, должны быть уничтожены, если иное не предусмотрено федеральным  законом. Хранение Персональных данных после прекращения их обработки  допускается только после их обезличивания.<br>
	6. Порядок получения разъяснений по вопросам обработки  Персональных данных         <br>
	6.1. Лица, чьи Персональные данные обрабатываются  Продавцом, могут получить разъяснения по вопросам обработки своих Персональных  данных, обратившись лично к Продавцу или направив соответствующий письменный  запрос по адресу местонахождения Продавца: 460000 Оренбург, переулок Скорняжный, д. 12.<br>
	6.2. В случае направления официального запроса Продавцу в  тексте запроса необходимо указать:
</p>
<ul>
	<li>фамилию, имя, отчество субъекта Персональных данных или  его представителя;</li>
	<li>номер основного документа, удостоверяющего личность  субъекта Персональных данных или его представителя, сведения о дате выдачи  указанного документа и выдавшем его органе;</li>
	<li>сведения, подтверждающие наличие у субъекта Персональных  данных отношений с Продавцом;</li>
	<li>информацию для обратной связи с целью направления  Продавцом ответа на запрос;</li>
	<li>подпись субъекта Персональных данных (или его представителя).</li>
</ul>
<p>Если запрос отправляется в электронном виде, то он должен  быть оформлен в виде электронного документа и подписан электронной подписью в  соответствии с законодательством Российской Федерации.<br>
	7. Особенности обработки и защиты Персональных данных,  собираемых с использованием телекоммуникационной сети «Интернет»<br>
	7.1. Продавец обрабатывает Персональные данные,  поступающие от пользователей Cайта продавца с ресурса: www.little-twist.ru, а также поступающие на телефоны  Продавца 8-912-840-41-41, в личные сообщения в социальных сетях на официальных  страницах Продавца (фейсбук, контакт, инстаграмм), на адреса электронной почты  Продавца: office@little-twist.ru.<br>
	7.2. Получение Продавцом Персональных данных с помощью  телекоммуникационной сети «Интернет» осуществляется двумя основными  способами:<br>
	7.2.1. Предоставление Персональных данных (фамилия, имя, электронная  почта, номер телефона, ссылка на персональный сайт или адрес в социальной  сети) ;субъектами Персональных данных путем поступления заявки на Сайт  продавца, на страницы Продавца в социальных сетях, на адрес электронной почты  Продавца. <br>
	7.2.2. Автоматически собираемая  информация <br>
	Продавец может собирать и обрабатывать сведения, не  являющимися персональными данными:
</p>
<ul>
	<li>ip адрес;</li>
	<li>информацию об интересах пользователей на Сайте продавца на  основе введенных поисковых запросов пользователей сайта о реализуемых и  предлагаемых к продаже товаров Продавца с целью предоставления актуальной  информации клиентам Продавца при использовании Сайта продавца, а также  обобщения и анализа информации, о том какие разделы Сайта продавца и товары  пользуются наибольшим спросом у Покупателей Продавца;</li>
	<li>обработка и хранение поисковых запросов пользователей  Сайта продавца с целью обобщения и создания клиентской статистики об  использовании разделов Сайта продавца</li>
</ul>
<p>7.3. Продавец автоматически получает некоторые виды  информации, получаемой в процессе взаимодействия пользователей с Cайтом  продавца, переписки по электронной почте и т. п., речь идет о технологиях и  сервисах, таких как веб-протоколы, куки, веб-отметки, а также приложения и  инструменты указанной третьей  стороны. <br>
	При этом веб-отметки, куки и другие мониторинговые  технологии не дают возможность автоматически получать Персональные данные. Если  пользователь Сайта по своему усмотрению предоставляет свои Персональные данные,  например, при заполнении формы обратной связи или при отправке электронного  письма, то только тогда запускаются процессы автоматического сбора подробной  информации для удобства пользования веб-сайтами и/или для совершенствования  взаимодействия с пользователями. ;<br>
	7.4. Продавец вправе пользоваться предоставленными  Персональными данными в соответствии с заявленными целями их сбора при наличии  согласия субъекта Персональных данных, если такое согласие требуется в  соответствии с требованиями законодательства Российской Федерации в области  Персональных данных.<br>
	Полученные Персональные данные в обобщенном и обезличенном  виде могут использоваться для лучшего понимания потребностей покупателей  товаров и услуг, реализуемых Продавцом и улучшения качества обслуживания.<br>7.5. Продавец может поручать обработку Персональных  данных третьим лицам исключительно с согласия субъекта Персональных данных.  Также Персональные данные могут передаваться третьим лицам в следующих случаях:
</p>
<ul>
	<li>в качестве ответа на правомерные запросы уполномоченных  государственных органов, в соответствии с законами, решениями суда и пр.;</li>
	<li>Персональные данные не могут передаваться третьим лицам  для маркетинговых, коммерческих целей.     </li>
</ul>
<p>7.6. Продавец может  использовать сторонние сервисы. Эти сторонние сервисы имеют свои собственные  политики конфиденциальности, описывающие как они используют предоставленную  информацию.
</p>