-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 21 2019 г., 00:05
-- Версия сервера: 5.6.39-83.1
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `littletwist_lw`
--

-- --------------------------------------------------------

--
-- Структура таблицы `lw_blog_blog`
--

CREATE TABLE IF NOT EXISTS `lw_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_lw_blog_blog_category_id` (`category_id`),
  KEY `ix_lw_blog_blog_create_date` (`create_time`),
  KEY `ix_lw_blog_blog_create_user` (`create_user_id`),
  KEY `ix_lw_blog_blog_lang` (`lang`),
  KEY `ix_lw_blog_blog_slug` (`slug`),
  KEY `ix_lw_blog_blog_status` (`status`),
  KEY `ix_lw_blog_blog_type` (`type`),
  KEY `ix_lw_blog_blog_update_date` (`update_time`),
  KEY `ix_lw_blog_blog_update_user` (`update_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_blog_post`
--

CREATE TABLE IF NOT EXISTS `lw_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_lw_blog_post_access_type` (`access_type`),
  KEY `ix_lw_blog_post_blog_id` (`blog_id`),
  KEY `ix_lw_blog_post_category_id` (`category_id`),
  KEY `ix_lw_blog_post_comment_status` (`comment_status`),
  KEY `ix_lw_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_lw_blog_post_lang` (`lang`),
  KEY `ix_lw_blog_post_publish_date` (`publish_time`),
  KEY `ix_lw_blog_post_slug` (`slug`),
  KEY `ix_lw_blog_post_status` (`status`),
  KEY `ix_lw_blog_post_update_user_id` (`update_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_blog_post_to_tag`
--

CREATE TABLE IF NOT EXISTS `lw_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_lw_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_lw_blog_post_to_tag_tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_blog_tag`
--

CREATE TABLE IF NOT EXISTS `lw_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_blog_user_to_blog`
--

CREATE TABLE IF NOT EXISTS `lw_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_lw_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_lw_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  KEY `ix_lw_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_lw_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_callback`
--

CREATE TABLE IF NOT EXISTS `lw_callback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `url` text,
  `agree` int(11) DEFAULT '0',
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_category_category`
--

CREATE TABLE IF NOT EXISTS `lw_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_lw_category_category_parent_id` (`parent_id`),
  KEY `ix_lw_category_category_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_comment_comment`
--

CREATE TABLE IF NOT EXISTS `lw_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_lw_comment_comment_level` (`level`),
  KEY `ix_lw_comment_comment_lft` (`lft`),
  KEY `ix_lw_comment_comment_model` (`model`),
  KEY `ix_lw_comment_comment_model_id` (`model_id`),
  KEY `ix_lw_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_lw_comment_comment_parent_id` (`parent_id`),
  KEY `ix_lw_comment_comment_rgt` (`rgt`),
  KEY `ix_lw_comment_comment_root` (`root`),
  KEY `ix_lw_comment_comment_status` (`status`),
  KEY `ix_lw_comment_comment_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_contentblock_content_block`
--

CREATE TABLE IF NOT EXISTS `lw_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_contentblock_content_block_code` (`code`),
  KEY `ix_lw_contentblock_content_block_status` (`status`),
  KEY `ix_lw_contentblock_content_block_type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=9 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_contentblock_content_block`
--

INSERT INTO `lw_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(1, 'logo', 'logo', 3, '<p><a href=\"/\"><img src=\"/uploads/image/dbaf7fe62434e991e073aa97ea368a67.png\"></a>\r\n</p>', '', NULL, 1),
(2, 'phone', 'phone', 3, '<a href=\"tel:79128404141\" class=\"number\">+7 (912) 840 41 41</a>', '', NULL, 1),
(3, 'policy', 'policy', 3, '<p>Согласен на обработку персональных данных с <a href=\"/\">договором публичной оферты<a><br>\r\nи <a href=\"/\">политикой конфиденциальности</a> ознакомлен и принимаю</p>', '', NULL, 1),
(4, 'email', 'email', 3, '<a href=\"mailto:little-twist@info.ru\" class=\"email\">Little-twist@info.ru</a>', '', NULL, 1),
(5, 'banner-middle', 'banner-middle', 3, '<p><img src=\"/uploads/image/4e36425754245e4081a72eaccbd24971.jpg\">\r\n</p>', '', NULL, 1),
(6, 'banner-bottom', 'banner-bottom', 3, '<p><img src=\"/uploads/image/8792ebe1e8a565987f9c82bb3b320c3f.jpg\">\r\n</p>', '', NULL, 1),
(7, 'copyright', 'copyright', 4, '“Little twist”. <label>Все права защищены.</label>', '', NULL, 1),
(8, 'main-social', 'main-social', 3, '<div class=\"social-flex\">\r\n	<a href=\"https://vk.com/littletwist\" target=\"_blank\"><i class=\"fa fa-vk\" aria-hidden=\"true\"></i></a>\r\n	<a href=\"https://www.instagram.com/little_twist.ru/\" target=\"_blank\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a>\r\n	<a href=\"https://www.facebook.com/littletwist.ru\" target=\"_blank\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>\r\n</div>', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_feedback_feedback`
--

CREATE TABLE IF NOT EXISTS `lw_feedback_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_lw_feedback_feedback_answer_user` (`answer_user`),
  KEY `ix_lw_feedback_feedback_category` (`category_id`),
  KEY `ix_lw_feedback_feedback_isfaq` (`is_faq`),
  KEY `ix_lw_feedback_feedback_status` (`status`),
  KEY `ix_lw_feedback_feedback_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_gallery_gallery`
--

CREATE TABLE IF NOT EXISTS `lw_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_lw_gallery_gallery_owner` (`owner`),
  KEY `ix_lw_gallery_gallery_sort` (`sort`),
  KEY `ix_lw_gallery_gallery_status` (`status`),
  KEY `fk_lw_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_lw_gallery_gallery_gallery_to_category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_gallery_gallery`
--

INSERT INTO `lw_gallery_gallery` (`id`, `name`, `description`, `status`, `owner`, `preview_id`, `category_id`, `sort`) VALUES
(1, 'Слайдер на главной', '<p>Слайдер на главной</p>', 1, 1, NULL, NULL, 1),
(2, 'Сертификаты', '<p>Сертификаты компании</p>', 1, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_gallery_image_to_gallery`
--

CREATE TABLE IF NOT EXISTS `lw_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_lw_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  KEY `ix_lw_gallery_image_to_gallery_gallery_to_image_image` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_gallery_image_to_gallery`
--

INSERT INTO `lw_gallery_image_to_gallery` (`id`, `image_id`, `gallery_id`, `create_time`, `position`) VALUES
(1, 2, 1, '2019-09-20 16:28:24', 1),
(2, 3, 1, '2019-09-20 16:28:25', 2),
(3, 4, 1, '2019-09-20 16:28:25', 3),
(4, 23, 2, '2019-10-11 12:17:50', 4),
(5, 24, 2, '2019-10-11 12:17:50', 5),
(6, 25, 2, '2019-10-11 12:17:51', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_image_image`
--

CREATE TABLE IF NOT EXISTS `lw_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_lw_image_image_category_id` (`category_id`),
  KEY `ix_lw_image_image_status` (`status`),
  KEY `ix_lw_image_image_type` (`type`),
  KEY `ix_lw_image_image_user` (`user_id`),
  KEY `fk_lw_image_image_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_image_image`
--

INSERT INTO `lw_image_image` (`id`, `category_id`, `parent_id`, `name`, `description`, `file`, `create_time`, `user_id`, `alt`, `type`, `status`, `sort`) VALUES
(1, NULL, NULL, 'logo.png', '', 'dbaf7fe62434e991e073aa97ea368a67.png', '2019-09-16 16:50:13', 1, 'logo.png', 0, 1, 1),
(2, NULL, NULL, 'Акция на новую коллекцию', '', '1a06673e606f5e821accc0c8d33a4cc8.jpg', '2019-09-20 16:28:24', 1, 'Акция на новую коллекцию', 0, 1, 2),
(3, NULL, NULL, 'Акция на новую коллекцию', '', '0594f3d07ab9909b4ee9060b97a5fc48.jpg', '2019-09-20 16:28:25', 1, 'Акция на новую коллекцию', 0, 1, 3),
(4, NULL, NULL, 'Акция на новую коллекцию', '', '5a596916a0835234dff87ec0458de084.jpg', '2019-09-20 16:28:25', 1, 'Акция на новую коллекцию', 0, 1, 4),
(5, NULL, NULL, 'collection1.jpg', '', '1bbc34801143bd4e58b8fdb8c39c3966.jpg', '2019-09-25 15:19:21', 1, 'collection1.jpg', 0, 1, 5),
(6, NULL, NULL, 'collection2.jpg', '', '7433c13bef0da0a6577b6583c7397b9c.jpg', '2019-09-25 15:19:53', 1, 'collection2.jpg', 0, 1, 6),
(7, NULL, NULL, 'col1.jpg', '', '4e36425754245e4081a72eaccbd24971.jpg', '2019-09-25 15:29:06', 1, 'col1.jpg', 0, 1, 7),
(8, NULL, NULL, 'col2.jpg', '', '8792ebe1e8a565987f9c82bb3b320c3f.jpg', '2019-09-25 15:29:24', 1, 'col2.jpg', 0, 1, 8),
(9, NULL, NULL, '1132.jpg', '', 'e19fb82b0777564ef9263a08f2f880ab.jpg', '2019-09-26 12:09:17', 1, '1132.jpg', 0, 1, 9),
(10, NULL, NULL, '12.jpg', '', '70f9e7f18247b02b7ba8e825261af951.jpg', '2019-09-26 12:24:13', 1, '12.jpg', 0, 1, 10),
(11, NULL, NULL, 'Dior1.jpg', '', 'fb447abe9b47a9969d24a787f0ee24b9.jpg', '2019-09-26 12:31:36', 1, 'Dior1.jpg', 0, 1, 11),
(12, NULL, NULL, '12.jpg', '', '2520e0f406f11915da71289803dbd626.jpg', '2019-09-26 12:32:24', 1, '12.jpg', 0, 1, 12),
(13, NULL, NULL, '12.jpg', '', 'b3e0584eb6839e7d6d6661e974483a00.jpg', '2019-09-26 12:46:48', 1, '12.jpg', 0, 1, 13),
(14, NULL, NULL, '12.jpg', '', 'd1200606f81846c6c0863a7ab361c339.jpg', '2019-09-27 10:15:27', 1, '12.jpg', 0, 1, 14),
(15, NULL, NULL, 'яяя.jpg', '', '8ebfa49af1d5142cce4585dc683d250b.jpg', '2019-09-27 16:18:51', 1, 'яяя.jpg', 0, 1, 15),
(16, NULL, NULL, 'qwe.jpg', '', '60684282d3c5f1a123bf08da3d6bb2b2.jpg', '2019-09-27 16:23:14', 1, 'qwe.jpg', 0, 1, 16),
(17, NULL, NULL, 'qwe.jpg', '', 'f045bf2cd66312ac81f0494f12c6a982.jpg', '2019-09-27 16:25:42', 1, 'qwe.jpg', 0, 1, 17),
(18, NULL, NULL, 'new.jpg', '', 'a34cd4aa41b81826144e0b855657b483.jpg', '2019-09-27 16:42:38', 1, 'new.jpg', 0, 1, 18),
(19, NULL, NULL, 'new.jpg', '', '03d63714dc3496e997545d5ba262afa1.jpg', '2019-09-27 16:43:38', 1, 'new.jpg', 0, 1, 19),
(20, NULL, NULL, 'IMG_5326.JPG', '', '605a24e89979ec42d1308a8d71add921.JPG', '2019-10-11 12:08:42', 1, 'IMG_5326.JPG', 0, 1, 20),
(21, NULL, NULL, 'IMG_5327.JPG', '', 'bbf35dabab4adef3e047628e0474da57.JPG', '2019-10-11 12:09:06', 1, 'IMG_5327.JPG', 0, 1, 21),
(22, NULL, NULL, 'IMG_5328.JPG', '', 'de8eb8d117a600ae1e1b3c42710361e7.JPG', '2019-10-11 12:09:29', 1, 'IMG_5328.JPG', 0, 1, 22),
(23, NULL, NULL, 'Сертификат соответствия', '', 'f0d31c71903e92a4a09f311d5600bcb0.jpg', '2019-10-11 12:17:50', 1, 'Сертификат соответствия', 0, 1, 23),
(24, NULL, NULL, 'Сертификат соответствия', '', '9fa2eb5e8d74dffb432ec9287ed990b9.jpg', '2019-10-11 12:17:50', 1, 'Сертификат соответствия', 0, 1, 24),
(25, NULL, NULL, 'Декларация о соответствии', '', 'db15ff3f9e2d685ca22a17405890068e.jpg', '2019-10-11 12:17:51', 1, 'Декларация о соответствии', 0, 1, 25);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_mail_mail_event`
--

CREATE TABLE IF NOT EXISTS `lw_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_mail_mail_event_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_mail_mail_template`
--

CREATE TABLE IF NOT EXISTS `lw_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_mail_mail_template_code` (`code`),
  KEY `ix_lw_mail_mail_template_event_id` (`event_id`),
  KEY `ix_lw_mail_mail_template_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_menu_menu`
--

CREATE TABLE IF NOT EXISTS `lw_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_menu_menu_code` (`code`),
  KEY `ix_lw_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_menu_menu`
--

INSERT INTO `lw_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(1, 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_menu_menu_item`
--

CREATE TABLE IF NOT EXISTS `lw_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_lw_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_lw_menu_menu_item_sort` (`sort`),
  KEY `ix_lw_menu_menu_item_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=18 AVG_ROW_LENGTH=1489 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_menu_menu_item`
--

INSERT INTO `lw_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`, `entity_module_name`, `entity_name`, `entity_id`) VALUES
(12, 0, 1, 1, 'Доставка и оплата', '/dostavka-i-oplata', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 15, 1, NULL, NULL, NULL),
(13, 0, 1, 1, 'Наши магазины', '/nashi-magaziny', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 18, 1, NULL, NULL, NULL),
(14, 0, 1, 1, 'Контакты', '/kontakty', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 19, 1, NULL, NULL, NULL),
(15, 0, 1, 1, 'О компании', '/o-kompanii', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 14, 1, NULL, NULL, NULL),
(16, 0, 1, 1, 'Возврат', '/vozvrat', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 16, 1, NULL, NULL, NULL),
(17, 0, 1, 1, 'Где мой заказ?', '/gde-moy-zakaz', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 17, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_migrations`
--

CREATE TABLE IF NOT EXISTS `lw_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=145 AVG_ROW_LENGTH=128 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_migrations`
--

INSERT INTO `lw_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1568627941),
(2, 'user', 'm131019_212911_user_tokens', 1568627942),
(3, 'user', 'm131025_152911_clean_user_table', 1568627951),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1568627954),
(5, 'user', 'm131106_111552_user_restore_fields', 1568627955),
(6, 'user', 'm131121_190850_modify_tokes_table', 1568627957),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1568627958),
(8, 'user', 'm150416_113652_rename_fields', 1568627959),
(9, 'user', 'm151006_000000_user_add_phone', 1568627959),
(10, 'yupe', 'm000000_000000_yupe_base', 1568627962),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1568627962),
(12, 'yupe', 'm150416_125517_rename_fields', 1568627962),
(13, 'yupe', 'm160204_195213_change_settings_type', 1568627962),
(14, 'category', 'm000000_000000_category_base', 1568627965),
(15, 'category', 'm150415_150436_rename_fields', 1568627966),
(16, 'image', 'm000000_000000_image_base', 1568627972),
(17, 'image', 'm150226_121100_image_order', 1568627972),
(18, 'image', 'm150416_080008_rename_fields', 1568627973),
(19, 'comment', 'm000000_000000_comment_base', 1568627980),
(20, 'comment', 'm130704_095200_comment_nestedsets', 1568627981),
(21, 'comment', 'm150415_151804_rename_fields', 1568627981),
(22, 'gallery', 'm000000_000000_gallery_base', 1568627987),
(23, 'gallery', 'm130427_120500_gallery_creation_user', 1568627988),
(24, 'gallery', 'm150416_074146_rename_fields', 1568627989),
(25, 'gallery', 'm160514_131314_add_preview_to_gallery', 1568627990),
(26, 'gallery', 'm160515_123559_add_category_to_gallery', 1568627991),
(27, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1568627991),
(28, 'gallery', 'm181224_072816_add_sort_to_gallery', 1568627992),
(29, 'store', 'm140812_160000_store_attribute_group_base', 1568627992),
(30, 'store', 'm140812_170000_store_attribute_base', 1568627995),
(31, 'store', 'm140812_180000_store_attribute_option_base', 1568627997),
(32, 'store', 'm140813_200000_store_category_base', 1568628000),
(33, 'store', 'm140813_210000_store_type_base', 1568628002),
(34, 'store', 'm140813_220000_store_type_attribute_base', 1568628004),
(35, 'store', 'm140813_230000_store_producer_base', 1568628005),
(36, 'store', 'm140814_000000_store_product_base', 1568628012),
(37, 'store', 'm140814_000010_store_product_category_base', 1568628016),
(38, 'store', 'm140814_000013_store_product_attribute_eav_base', 1568628021),
(39, 'store', 'm140814_000018_store_product_image_base', 1568628023),
(40, 'store', 'm140814_000020_store_product_variant_base', 1568628027),
(41, 'store', 'm141014_210000_store_product_category_column', 1568628033),
(42, 'store', 'm141015_170000_store_product_image_column', 1568628035),
(43, 'store', 'm141218_091834_default_null', 1568628035),
(44, 'store', 'm150210_063409_add_store_menu_item', 1568628035),
(45, 'store', 'm150210_105811_add_price_column', 1568628036),
(46, 'store', 'm150210_131238_order_category', 1568628036),
(47, 'store', 'm150211_105453_add_position_for_product_variant', 1568628036),
(48, 'store', 'm150226_065935_add_product_position', 1568628036),
(49, 'store', 'm150416_112008_rename_fields', 1568628036),
(50, 'store', 'm150417_180000_store_product_link_base', 1568628044),
(51, 'store', 'm150825_184407_change_store_url', 1568628044),
(52, 'store', 'm150907_084604_new_attributes', 1568628048),
(53, 'store', 'm151218_081635_add_external_id_fields', 1568628048),
(54, 'store', 'm151218_082939_add_external_id_ix', 1568628049),
(55, 'store', 'm151218_142113_add_product_index', 1568628049),
(56, 'store', 'm151223_140722_drop_product_type_categories', 1568628051),
(57, 'store', 'm160210_084850_add_h1_and_canonical', 1568628052),
(58, 'store', 'm160210_131541_add_main_image_alt_title', 1568628052),
(59, 'store', 'm160211_180200_add_additional_images_alt_title', 1568628053),
(60, 'store', 'm160215_110749_add_image_groups_table', 1568628054),
(61, 'store', 'm160227_114934_rename_producer_order_column', 1568628055),
(62, 'store', 'm160309_091039_add_attributes_sort_and_search_fields', 1568628055),
(63, 'store', 'm160413_184551_add_type_attr_fk', 1568628056),
(64, 'store', 'm160602_091243_add_position_product_index', 1568628056),
(65, 'store', 'm160602_091909_add_producer_sort_index', 1568628056),
(66, 'store', 'm160713_105449_remove_irrelevant_product_status', 1568628057),
(67, 'store', 'm160805_070905_add_attribute_description', 1568628057),
(68, 'store', 'm161015_121915_change_product_external_id_type', 1568628061),
(69, 'store', 'm161122_090922_add_sort_product_position', 1568628061),
(70, 'store', 'm161122_093736_add_store_layouts', 1568628062),
(71, 'store', 'm181218_121815_store_product_variant_quantity_column', 1568628062),
(72, 'mail', 'm000000_000000_mail_base', 1568628066),
(73, 'payment', 'm140815_170000_store_payment_base', 1568628067),
(74, 'delivery', 'm140815_190000_store_delivery_base', 1568628071),
(75, 'delivery', 'm140815_200000_store_delivery_payment_base', 1568628073),
(76, 'order', 'm140814_200000_store_order_base', 1568628092),
(77, 'order', 'm150324_105949_order_status_table', 1568628097),
(78, 'order', 'm150416_100212_rename_fields', 1568628097),
(79, 'order', 'm150514_065554_change_order_price', 1568628097),
(80, 'order', 'm151209_185124_split_address', 1568628097),
(81, 'order', 'm151211_115447_add_appartment_field', 1568628098),
(82, 'order', 'm160415_055344_add_manager_to_order', 1568628100),
(83, 'order', 'm160618_145025_add_status_color', 1568628100),
(84, 'notify', 'm141031_091039_add_notify_table', 1568628103),
(85, 'blog', 'm000000_000000_blog_base', 1568628138),
(86, 'blog', 'm130503_091124_BlogPostImage', 1568628139),
(87, 'blog', 'm130529_151602_add_post_category', 1568628142),
(88, 'blog', 'm140226_052326_add_community_fields', 1568628142),
(89, 'blog', 'm140714_110238_blog_post_quote_type', 1568628144),
(90, 'blog', 'm150406_094809_blog_post_quote_type', 1568628147),
(91, 'blog', 'm150414_180119_rename_date_fields', 1568628148),
(92, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1568628156),
(93, 'blog', 'm180421_143937_update_blog_meta_column', 1568628156),
(94, 'blog', 'm180421_143938_add_post_meta_title_column', 1568628157),
(95, 'coupon', 'm140816_200000_store_coupon_base', 1568628160),
(96, 'coupon', 'm150414_124659_add_order_coupon_table', 1568628165),
(97, 'coupon', 'm150415_153218_rename_fields', 1568628165),
(98, 'page', 'm000000_000000_page_base', 1568628175),
(99, 'page', 'm130115_155600_columns_rename', 1568628175),
(100, 'page', 'm140115_083618_add_layout', 1568628176),
(101, 'page', 'm140620_072543_add_view', 1568628176),
(102, 'page', 'm150312_151049_change_body_type', 1568628179),
(103, 'page', 'm150416_101038_rename_fields', 1568628179),
(104, 'page', 'm180224_105407_meta_title_column', 1568628179),
(105, 'page', 'm180421_143324_update_page_meta_column', 1568628180),
(106, 'sitemap', 'm141004_130000_sitemap_page', 1568628182),
(107, 'sitemap', 'm141004_140000_sitemap_page_data', 1568628182),
(108, 'yml', 'm141110_090000_yandex_market_export_base', 1568628184),
(109, 'yml', 'm160119_084800_rename_yandex_market_table', 1568628184),
(110, 'menu', 'm000000_000000_menu_base', 1568628188),
(111, 'menu', 'm121220_001126_menu_test_data', 1568628188),
(112, 'menu', 'm160914_134555_fix_menu_item_default_values', 1568628200),
(113, 'menu', 'm181214_110527_menu_item_add_entity_fields', 1568628201),
(114, 'news', 'm000000_000000_news_base', 1568628207),
(115, 'news', 'm150416_081251_rename_fields', 1568628207),
(116, 'news', 'm180224_105353_meta_title_column', 1568628207),
(117, 'news', 'm180421_142416_update_news_meta_column', 1568628208),
(118, 'callback', 'm150926_083350_callback_base', 1568628209),
(119, 'callback', 'm160621_075232_add_date_to_callback', 1568628209),
(120, 'callback', 'm161125_181730_add_url_to_callback', 1568628209),
(121, 'callback', 'm161204_122528_update_callback_encoding', 1568628209),
(122, 'callback', 'm180224_103745_add_agree_column', 1568628209),
(123, 'callback', 'm181213_214512_add_type_column', 1568628209),
(124, 'feedback', 'm000000_000000_feedback_base', 1568628217),
(125, 'feedback', 'm150415_184108_rename_fields', 1568628217),
(126, 'contentblock', 'm000000_000000_contentblock_base', 1568628220),
(127, 'contentblock', 'm140715_130737_add_category_id', 1568628220),
(128, 'contentblock', 'm150127_130425_add_status_column', 1568628221),
(129, 'yupe', 'm190916_122628_add_parent_id_column_to_store_product_variant_table', 1568801631),
(130, 'rbac', 'm140115_131455_auth_item', 1569927972),
(131, 'rbac', 'm140115_132045_auth_item_child', 1569927973),
(132, 'rbac', 'm140115_132319_auth_item_assign', 1569927973),
(133, 'rbac', 'm140702_230000_initial_role_data', 1569927973),
(134, 'sberbank', 'm151211_115450_add_sberbank_fields', 1569928380),
(135, 'order', 'm191007_090158_add_region_order', 1571338901),
(136, 'order', 'm191007_090158_add_region_order', 1571338901),
(137, 'order', 'm191007_091721_add_region_city_order', 1571338901),
(138, 'order', 'm191017_190431_add_fields_for_order', 1571339493),
(139, 'order', 'm191017_191427_rename_fields_for_order', 1571339813),
(140, 'order', 'm191018_090038_create_order_post_price_table', 1571498584),
(141, 'delivery', 'm191019_192322_add_column_class_delivery', 1571513335),
(142, 'order', 'm191020_181356_create_table_order_sdek', 1571596621),
(143, 'order', 'm191020_183335_add_column_tariff_pvz_order', 1571596621),
(144, 'order', 'm191020_203319_add_column_order_id_order_sdek_data', 1571603704);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_news_news`
--

CREATE TABLE IF NOT EXISTS `lw_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_lw_news_news_category_id` (`category_id`),
  KEY `ix_lw_news_news_date` (`date`),
  KEY `ix_lw_news_news_status` (`status`),
  KEY `ix_lw_news_news_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_notify_settings`
--

CREATE TABLE IF NOT EXISTS `lw_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_lw_notify_settings_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_order_post_price`
--

CREATE TABLE IF NOT EXISTS `lw_order_post_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(6,2) DEFAULT NULL,
  `index` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `min_deys` int(11) DEFAULT NULL,
  `max_deys` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_page_page`
--

CREATE TABLE IF NOT EXISTS `lw_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_lw_page_page_category_id` (`category_id`),
  KEY `ix_lw_page_page_change_user_id` (`change_user_id`),
  KEY `ix_lw_page_page_is_protected` (`is_protected`),
  KEY `ix_lw_page_page_menu_order` (`order`),
  KEY `ix_lw_page_page_status` (`status`),
  KEY `ix_lw_page_page_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_page_page`
--

INSERT INTO `lw_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`) VALUES
(1, NULL, 'ru', NULL, '2019-09-16 16:53:17', '2019-10-17 10:42:58', 1, 1, '', 'О компании', 'o-kompanii', '<div class=\"row\">\r\n	<div class=\"col-sm-8 col-xs-12\">\r\n		<p>\r\n			<strong>LITTLE TWIST</strong>\r\n		</p>\r\n		<p>Хотим рассказать Вам про наше творческое пространство - Литл Твист <a href=\"https://www.instagram.com/explore/tags/%D0%BB%D0%B8%D1%82%D0%BB%D1%81%D0%BF%D1%8D%D0%B9%D1%81/\"  target=\"_blank\">#литлспэйс!</a> Что же это для нас и как видим это мы?\r\n		</p>\r\n		<p>Для нас Little Twist - это почти семейное дело)), наша дружная команда и любимый цех, наша любовь к детям, много труда и заботы о маленьких клиентах, сертифицированная одежда для деток от 0 			до 8 лет, зарегистрированная марка, функциональность и комфорт, неожиданные решения и смелые цвета, это мелкие приятные детали и полный образ, возможность выбора для покупателя, это\r\n		</p>\r\n		<p>БОЛЬШИЕ ИДЕИ в маленьких вещах!\r\n		</p>\r\n		<p><strong>ВСЯ ПРОДУКЦИЯ СЕРТИФИЦИРОВАНА</strong>\r\n		</p>\r\n		<p><strong>НАШЕ ПРОИЗВОДСТВО</strong>\r\n		</p>\r\n	</div>\r\n	<div class=\"col-sm-4 col-xs-12\">\r\n	</div>\r\n</div>\r\n<div class=\"row\">\r\n	<div class=\"col-sm-6 col-xs-12\">\r\n		<table class=\"table table-condensed\">\r\n		<tbody>\r\n		<tr>\r\n			<td><strong>РЕКВИЗИТЫ</strong>\r\n			</td>\r\n			<td><strong>ИП Загидуллина А.С.</strong>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ИНН\r\n			</td>\r\n			<td>561011497229\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОГРНИП\r\n			</td>\r\n			<td>319565800004437\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Фактический адрес\r\n			</td>\r\n			<td>460000, Оренбургская область, г. Оренбург, пер. Скорняжный 12\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Телефоны:\r\n			</td>\r\n			<td>+ 7 912 840 41 41\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКВЭД\r\n			</td>\r\n			<td>14.13\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКАТО\r\n			</td>\r\n			<td>53234806001\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКПО\r\n			</td>\r\n			<td>0143852884\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКТМО\r\n			</td>\r\n			<td>53634406101\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Деятельность\r\n			</td>\r\n			<td>Производство и розничная торговля\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>БИК\r\n			</td>\r\n			<td>045354601\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			</td>\r\n			<td>Оренбургское отделение №8623 ПАО СБЕРБАНК, 461300, г. Оренбург, ул. Володарского, 16\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Р/С\r\n			</td>\r\n			<td>40802.810.8.46000.016313\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>К/С\r\n			</td>\r\n			<td>30101810600000000601\r\n			</td>\r\n		</tr>\r\n		</tbody>\r\n		</table>\r\n	</div>\r\n</div>', 'детская одежда', 'LITTLE TWIST – это российская марка детской одежды от 0 до 8 лет. Мы создаем и производим одежду, в которой сбалансированы качество, цена и стиль.', 1, 0, 1, 'page', '', 'Little Twist'),
(2, NULL, 'ru', NULL, '2019-09-16 16:54:40', '2019-10-11 12:33:05', 1, 1, '', 'Доставка и оплата', 'dostavka-i-oplata', '<h2>ДОСТАВКА ТОВАРА</h2><p><label>САМОВЫВОЗ</label>\r\n</p><p>Самовывоз товара возможен из пунктов:\r\n</p><ul>\r\n	<li>  460000 Оренбург, пер. Скорняжный, д. 12,\r\n	корпус 1, второй этаж\r\n	</li>\r\n</ul><p><label>ПЛАТНАЯ ДОСТАВКА КУРЬЕРОМ СДЭК </label>\r\n</p><p>Стоимость доставки рассчитывается автоматически при оформлении заказа на нашем сайте. Ваш заказ будет доставлен курьером лично в руки по адресу, указанному при оформлении заказа. При получении товара необходимо предоставить свой паспорт курьеру\r\n</p><p><label>ПЛАТНАЯ ДОСТАВКА ДО ПУНКТА САМОВЫВОЗА  СДЭК</label>\r\n</p><p>Стоимость доставки рассчитывается автоматически при оформлении заказа на нашем сайте. Ваш заказ будет доставлен в пункт самовывоза по адресу, указанному при оформлении заказа. Заказ будет бесплатно храниться в пункте самовывоза в течение 7 дней. При получении товара необходимо предоставить свой паспорт.\r\n</p><p><label>ПЛАТНАЯ ДОСТАВКА КУРЬЕРОМ ПОЧТЫ РФ (EMC)</label>\r\n</p><p>Стоимость доставки рассчитывается автоматически при оформлении заказа на нашем сайте. Ваш заказ будет доставлен курьером лично в руки по адресу, указанному при оформлении заказа. При получении товара необходимо предоставить свой паспорт курьеру.\r\n</p><p><label>ПЛАТНАЯ ДОСТАВКА ДО БЛИЖАЙШЕГО ОТДЕЛЕНИЯ ПОЧТЫ РФ</label>\r\n</p><p>Стоимость доставки рассчитывается автоматически при оформлении заказа на нашем сайте. Если в ваш населенный пункт нет возможности курьерской доставки, при оформлении заказа вам будет доступен только вариант доставки Почтой РФ. С 1 июня 2018 года срок бесплатного хранения посылок на почтовом отделении сокращен до 15 дней. Если вам требуется хранение заказа свыше установленного срока, необходимо составить заявление и оплатить данную услугу на почтовом отделении.\r\n</p><p><label>После оформления заказа трек номер заказа бдет выслан вам по почте, указанной при оформлении заказа. Если у Вас возникли проблемы с получением трек номера заказа, Вы всегда можете обратиться к нашим менеджерам по телефонам, указанным в разделе «КОНТАКТЫ».</label>\r\n</p><p><label>Оперативно отследить местоположение Вашего заказа вы можете, указав трек номер заказа по ссылкам, указанным здесь и в разделе «ГДЕ МОЙ ЗАКАЗ»: </label>\r\n</p><ul>\r\n	<li><a href=\"https://www.cdek.ru/track.html\">Отследить заказ (СДЭК)</a></li>\r\n	<li><a href=\"https://www.pochta.ru/tracking\">Отследить заказ (Почта России)</a></li>\r\n</ul><div class=\"attention\">\r\n	Примерка товара при доставке не осуществляется. Вскрытие посылки возможно после полной оплаты доставки и товара.\r\n</div><h2>ОПЛАТА ТОВАРА и ДОСТАВКИ</h2><p><label>НАЛИЧНЫЕ или БАНКОВСКАЯ КАРТА</label>\r\n</p><p>Оплата наличными деньгами или банковской картой может быть произведена в пунктах самовывоза товара, указанных выше («САМОВЫВОЗ»).\r\n</p><p><label>ОНЛАЙН ОПЛАТА НА САЙТЕ</label>\r\n</p><p>Мы работаем по 100% предоплате товара и доставки. После выбора товара и способа доставки заказа вы увидите общую сумму и будете перенаправлены в раздел «Оплата», где необходимо заполнить все обязательные поля. После чего на почту, указанную вами, будет отправлен чек и трек номер вашего заказа.\r\n</p><p>\r\n	Для оплаты покупки Вы будете перенаправлены\r\nна платежный шлюз ПАО \"Сбербанк\r\nРоссии\" для ввода реквизитов Вашей\r\nкарты. Пожалуйста, приготовьте Вашу\r\nпластиковую карту заранее. Соединение\r\nс платежным шлюзом и передача информации\r\nосуществляется в защищенном режиме с\r\nиспользованием протокола шифрования\r\nSSL. В случае если Ваш банк поддерживает\r\nтехнологию безопасного проведения\r\nинтернет-платежей Verified By Visa или MasterCard\r\nSecure Code для проведения платежа также\r\nможет потребоваться ввод специального\r\nпароля. Способы и возможность получения\r\nпаролей для совершения интернет-платежей\r\nВы можете уточнить в банке, выпустившем\r\nкарту.\r\n</p><p>Конфиденциальность сообщаемой персональной информации обеспечивается ПАО \"Сбербанк России\". Введенная информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платежных систем Visa Int. и MasterCard Europe Sprl.\r\n</p><p>Что нужно знать для оплаты товара банковской картой на сайте:\r\n</p><ol>\r\n	<li>номер вашей кредитной карты;</li>\r\n	<li>cрок окончания действия вашей кредитной карты, месяц/год;</li>\r\n	<li>CVV код для карт Visa / CVC код для Master Card: 3 последние цифры на полосе для подписи на обороте карты.</li>\r\n</ol><p>Если на вашей карте код CVC / CVV отсутствует, то, возможно, карта не пригодна для CNP транзакций (т.е. таких транзакций, при которых сама карта не присутствует, а используются её реквизиты), и вам следует обратиться в банк для получения подробной информации.\r\n</p><p>\r\n	Если Банк-Эмитент вашей пластиковой\r\nкарты поддерживает технологию безопасного\r\nпроведения интернет-платежей Verified\r\nBy VISA или MasterCard SecureCode, будьте готовы\r\nуказать специальный пароль, необходимый\r\nдля успешной оплаты. Способы и возможность\r\nполучения пароля для совершения\r\nинтернет-платежа Вы можете уточнить в\r\nбанке, выпустившем Вашу карту.\r\n</p><div class=\"attention\">\r\n	При выборе формы оплаты с помощью банковской карты проведение платежа по заказу производится непосредственно после подтверждения его менеджером. В случае подтверждения авторизации Ваш заказ будет автоматически выполняться в соответствии с заданными Вами условиями. В случае отказа в авторизации карты Вы сможете повторить процедуру оплаты.\r\n</div><p><label>ОТМЕНА ЗАКАЗА</label>\r\n</p><p>При аннулировании заказа целиком Вы можете полностью вернуть всю сумму на карту, обратившись по телефонам менеджеров, указанных в разделе «КОНТАКТЫ». Оплаченные заказы отправляются в течение 2-х дней после оплаты, поэтому аннулировать заказ необходимо в течение 2-х часов после совершения оплаты, пока заказ еще не отправлен.\r\n</p>', '', '', 1, 0, 2, 'page', '', ''),
(3, NULL, 'ru', NULL, '2019-09-16 16:55:03', '2019-10-17 09:05:04', 1, 1, '', 'Возврат', 'vozvrat', '<p>Если товар не соответствует Вашим ожиданиям, Вы можете вернуть его в течение 14 дней от даты получения. Товары, которые вы хотите вернуть должны иметь первоначальный вид, все этикетки и бирки должны быть на месте. Согласно Законодательству РФ, некоторые изделия обмену и возврату не подлежат.\r\n</p>\r\n<p><label>ПРОЦЕДУРА ВОЗВРАТА ТОВАРА:</label>\r\n</p>\r\n<ul>\r\n	<li>упакуйте товары, которые хотите вернуть;</li>\r\n	<li>распечатайте и заполните <a href=\"/uploads/files/2019/10/17/akt-vozvrata_1571292276.doc\" target=\"_blank\">заявление-акт</a> о возврате товара, вложите документы в посылку;\r\n	</li>\r\n	<li>  отправьте возвращаемый товар вместе\r\n	с вложенными документами СДЭК или\r\n	Почтой РФ;\r\n	</li>\r\n	<li>в случае, если Вы отправляете возврат Почтой РФ, для ускорения процедуры возврата, просим Вас выслать трек-номер Вашей посылки и номер Вашего заказа на адрес little-twist@info.ru. Адрес для возврата посылок Почтой РФ:\r\n460000 Оренбургская область. Оренбург,\r\nКирова 16;\r\n	</li>\r\n	<li>мы совершим возврат денежных средств* по указанным Вами банковским реквизитам в течение 10 дней после получения Вашего возврата.</li>\r\n</ul>\r\n<p> Возврат денежных средств осуществляется за вычетом стоимости возврата товара на наш склад.\r\n</p>\r\n<div class=\"attention\">\r\n	При возврате некачественного товара или товара, не соответствующего Вашему заказу в наш адрес через пересылку Почтой России, просим Вас сохранить квитанцию об оплате. Копия данной квитанции может быть запрошена у Вас в целях компенсации понесенных затрат.\r\n</div>', '', '', 1, 0, 3, 'page', '', ''),
(4, NULL, 'ru', NULL, '2019-09-16 16:55:27', '2019-10-11 11:59:18', 1, 1, '', 'Где мой заказ?', 'gde-moy-zakaz', '<p>В случае выбора платной доставки СДЭК или Почта РФ, вы всегда можете отследить статус своего заказа на сайте или приложениях компаний:\r\n</p><ul>\r\n<li><a href=\"https://www.cdek.ru/track.html\">Отследить заказ (СДЭК)</a></li>\r\n<li><a href=\"https://www.pochta.ru/tracking\">Отследить заказ (Почта России)</a></li>\r\n</ul>', '', '', 1, 0, 4, 'page', '', ''),
(5, NULL, 'ru', NULL, '2019-09-16 16:56:41', '2019-10-07 06:08:19', 1, 1, '', 'Наши магазины', 'nashi-magaziny', '<div class=\"row\">\r\n	<div class=\"col-sm-12 col-xs-12\">\r\n		<p><label>АДРЕС:</label> ТЦ Армада 2, 460051 Оренбург, Нежинское шоссе 2А, 2-ой этаж, фирменный магазин LITTLE TWIST\r\n		</p>\r\n<img src=\"/uploads/image/03d63714dc3496e997545d5ba262afa1.jpg\" style=\"background-color: initial;\">\r\n	</div>\r\n</div>', '', '', 1, 0, 5, 'page', 'contacts', ''),
(6, NULL, 'ru', NULL, '2019-09-16 16:56:52', '2019-10-17 11:56:11', 1, 1, '', 'Контакты', 'kontakty', '<div class=\"row\">\r\n	<div class=\"col-sm-12 col-xs-12 contacts-text\">\r\n		<div class=\"flex-block\">\r\n			<p><label>МАГАЗИНЫ</label>\r\n			</p>\r\n			<p>ТЦ Армада 2, 460051 Оренбург, Нежинское шоссе 2А, 2-ой этаж, фирменный магазин LITTLE TWIST\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block\">\r\n			<p><label>ПРОИЗВОДСТВО</label>\r\n			</p>\r\n			<p>460000 Оренбург, пер. Скорняжный 12, строение 1\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block\">\r\n			<p><label>По вопросам оформления заказов / доставки / оплаты / возврата:</label>\r\n			</p>\r\n			<p>+7 912 840 41 41\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block\">\r\n			<p><label>По вопросам сотрудничества:</label>\r\n			</p>\r\n			<p>+7 912 840 44 22\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block\">\r\n			<p><label>Мы в социальных сетях:</label>\r\n			</p>\r\n			<div class=\"social-flex\">\r\n				<a href=\"https://vk.com/littletwist\" target=\"_blank\"><i class=\"fa fa-vk\" aria-hidden=\"true\"></i></a>\r\n				<a href=\"https://www.instagram.com/little_twist.ru/\" target=\"_blank\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a>\r\n				<a href=\"https://www.facebook.com/littletwist.ru\" target=\"_blank\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>\r\n			</div>\r\n		</div>\r\n	</div><strong>\r\n	</strong>\r\n</div>', '', '', 1, 0, 6, 'page', 'contacts', ''),
(7, NULL, 'ru', NULL, '2019-09-20 22:06:11', '2019-10-07 06:12:15', 1, 1, '', 'Little Twist', 'little-twist', '<div class=\"row\">\r\n	<div class=\"col-sm-8 col-xs-12\">\r\n		<p>\r\n			<strong>LITTLE TWIST</strong> – это российская марка детской одежды от 0 до 8 лет. Мы создаем и производим одежду, в которой сбалансированы качество, цена и стиль. Вся наша одежда комфортна и функциональна, в ней мы постарались учесть все потребности не только детей, но и родителей! В наших изделиях много приятных мелких деталей! Большая часть наших коллекций создано по концепции total look, что позволит создать яркий и запоминающийся образ для Вашего ребенка. С нами все просто, просто попробуйте! <i class=\"fa fa-smile-o\" aria-hidden=\"true\"></i>\r\n		</p>\r\n		<p><strong>ВСЯ ПРОДУКЦИЯ СЕРТИФИЦИРОВАНА</strong>\r\n		</p>\r\n		Здесь будут фото сертификатов\r\n		<p><strong>НАШЕ ПРОИЗВОДСТВО</strong>\r\n		</p>\r\n		Здесь будут фото производства\r\n	</div>\r\n	<div class=\"col-sm-4 col-xs-12\">\r\n	</div>\r\n</div>\r\n<div class=\"row\">\r\n	<div class=\"col-sm-6 col-xs-12\">\r\n		<table class=\"table table-condensed\">\r\n		<tbody>\r\n		<tr>\r\n			<td><strong>РЕКВИЗИТЫ</strong>\r\n			</td>\r\n			<td><strong>ИП Загидуллина А.С.</strong>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ИНН\r\n			</td>\r\n			<td>561011497229\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОГРНИП\r\n			</td>\r\n			<td>319565800004437\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Фактический адрес\r\n			</td>\r\n			<td>460000, Оренбургская область, г. Оренбург, пер. Скорняжный 12\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Телефоны:\r\n			</td>\r\n			<td>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКВЭД\r\n			</td>\r\n			<td>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКАТО\r\n			</td>\r\n			<td>53234806001\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКПО\r\n			</td>\r\n			<td>0143852884\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ОКТМО\r\n			</td>\r\n			<td>53634406101\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Деятельность\r\n			</td>\r\n			<td>Производство и розничная торговля\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>БИК\r\n			</td>\r\n			<td>045354601\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			</td>\r\n			<td>Оренбургское отделение №8623 ПАО СБЕРБАНК, 461300, г. Оренбург, ул. Володарского, 16\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Р/С\r\n			</td>\r\n			<td>40802.810.8.46000.016313\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>К/С\r\n			</td>\r\n			<td>30101810600000000601\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Директор\r\n			</td>\r\n			<td>Загидуллина Анастасия Сергеевна\r\n			</td>\r\n		</tr>\r\n		</tbody>\r\n		</table>\r\n	</div>\r\n</div>', '', '', 1, 0, 7, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_sitemap_page`
--

CREATE TABLE IF NOT EXISTS `lw_sitemap_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL,
  `changefreq` varchar(20) NOT NULL,
  `priority` float NOT NULL DEFAULT '0.5',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_sitemap_page_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_sitemap_page`
--

INSERT INTO `lw_sitemap_page` (`id`, `url`, `changefreq`, `priority`, `status`) VALUES
(1, '/', 'daily', 0.5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_attribute`
--

CREATE TABLE IF NOT EXISTS `lw_store_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_store_attribute_name_group` (`name`,`group_id`),
  KEY `ix_lw_store_attribute_title` (`title`),
  KEY `fk_lw_store_attribute_group` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_attribute`
--

INSERT INTO `lw_store_attribute` (`id`, `group_id`, `name`, `title`, `type`, `unit`, `required`, `sort`, `is_filter`, `description`) VALUES
(1, 1, 'cvet', 'ЦВЕТ', 1, '', 0, 1, 0, ''),
(2, NULL, 'razmer', 'Размер', 2, '', 0, 2, 1, ''),
(3, NULL, 'sezon', 'Сезон', 2, '', 0, 3, 1, ''),
(4, NULL, 'otdelka-kapyushona', 'Отделка капюшона', 2, '', 0, 4, 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_attribute_group`
--

CREATE TABLE IF NOT EXISTS `lw_store_attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_attribute_group`
--

INSERT INTO `lw_store_attribute_group` (`id`, `name`, `position`) VALUES
(1, 'Основное', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_attribute_option`
--

CREATE TABLE IF NOT EXISTS `lw_store_attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ix_lw_store_attribute_option_attribute_id` (`attribute_id`),
  KEY `ix_lw_store_attribute_option_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=36 AVG_ROW_LENGTH=1024 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_attribute_option`
--

INSERT INTO `lw_store_attribute_option` (`id`, `attribute_id`, `position`, `value`) VALUES
(1, 2, 1, '56'),
(2, 2, 2, '62'),
(3, 2, 3, '68'),
(4, 2, 4, '74'),
(5, 2, 5, '80'),
(6, 2, 6, '86'),
(7, 3, 7, 'лето'),
(8, 3, 8, 'осень'),
(9, 3, 9, 'зима'),
(10, 3, 10, 'весна'),
(11, 3, 11, 'веста-лето'),
(12, 3, 12, 'осень-зима'),
(13, 4, 13, 'Вязаный помпон'),
(14, 4, 14, 'Меховой помпон'),
(15, 4, 15, 'Опушка'),
(16, 4, 16, 'Без отделки'),
(17, 2, 17, '58'),
(18, 3, 18, 'деми'),
(19, 2, 19, '0-4 мес'),
(20, 2, 20, '0-6 мес'),
(21, 2, 21, '44'),
(22, 2, 22, '44-46'),
(23, 2, 23, '46-48'),
(24, 2, 24, '50-52'),
(25, 2, 25, '40-42'),
(26, 2, 26, '42-44'),
(27, 2, 27, '52-54'),
(28, 4, 28, 'Ушки'),
(29, 2, 29, '92'),
(30, 2, 30, '98'),
(31, 2, 31, '104'),
(32, 2, 32, '110'),
(33, 2, 33, '116'),
(34, 2, 34, '86-104'),
(35, 2, 35, '110-116');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_category`
--

CREATE TABLE IF NOT EXISTS `lw_store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_store_category_alias` (`slug`),
  KEY `ix_lw_store_category_parent_id` (`parent_id`),
  KEY `ix_lw_store_category_status` (`status`),
  KEY `lw_store_category_external_id_ix` (`external_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 AVG_ROW_LENGTH=780 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_category`
--

INSERT INTO `lw_store_category` (`id`, `parent_id`, `slug`, `name`, `image`, `short_description`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `sort`, `external_id`, `title`, `meta_canonical`, `image_alt`, `image_title`, `view`) VALUES
(1, 3, 'kombinezony', 'Комбинезоны', NULL, '', '', '', '', '', 1, 2, NULL, '', '', '', '', ''),
(2, 3, 'kurtki', 'Куртки', NULL, '', '', '', '', '', 1, 3, NULL, '', '', '', '', ''),
(3, NULL, 'verhnyaya-odezhda', 'Верхняя одежда', NULL, '', '', '', '', '', 1, 5, NULL, '', '', '', '', ''),
(4, NULL, 'uteplennaya-odezhda-ot-0-do-1-goda', 'Утепленная одежда от 0 до 1 года', NULL, '', '', '', '', '', 1, 6, NULL, '', '', '', '', ''),
(5, NULL, 'povsednevnaya-odezhda', 'Повседневная одежда', NULL, '', '', '', '', '', 1, 9, NULL, '', '', '', '', ''),
(6, NULL, 'golovnye-ubory-i-sharfy', 'Головные уборы и шарфы', NULL, '', '', '', '', '', 1, 7, NULL, '', '', '', '', ''),
(7, NULL, 'kragi-remni-pinetki', 'Краги / ремни / пинетки', NULL, '', '', '', '', '', 1, 8, NULL, '', '', '', '', ''),
(8, NULL, 'termobele', 'Термобелье', NULL, '', '', '', '', '', 1, 10, NULL, '', '', '', '', ''),
(10, 3, 'konverty', 'Конверты', NULL, '', '', '', '', '', 1, 1, NULL, '', '', '', '', ''),
(11, 3, 'polukombinezony', 'Полукомбинезоны', NULL, '', '', '', '', '', 1, 4, NULL, '', '', '', '', ''),
(15, 4, 'kokony', 'Коконы', NULL, '', '', '', '', '', 1, 14, NULL, '', '', '', '', ''),
(16, 4, 'kombinezony-uteplennie', 'Комбинезоны', NULL, '', '', '', '', '', 1, 15, NULL, '', '', '', '', ''),
(17, 5, 'kombinezony-povsednevnie', 'Комбинезоны', NULL, '', '', '', '', '', 1, 16, NULL, '', '', '', '', ''),
(20, 5, 'yubki-fatin', 'Юбки фатин', NULL, '', '', '', '', '', 1, 17, NULL, '', '', '', '', ''),
(23, 6, 'zima-do-goda', 'ЗИМА до года', NULL, '', '', '', '', '', 1, 19, NULL, '', '', '', '', ''),
(24, 6, 'zima-ot-goda', 'ЗИМА от года', NULL, '', '', '', '', '', 1, 20, NULL, '', '', '', '', ''),
(25, 6, 'demi-do-goda', 'ДЕМИ до года', NULL, '', '', '', '', '', 1, 21, NULL, '', '', '', '', ''),
(26, 6, 'demi-ot-goda', 'ДЕМИ от года', NULL, '', '', '', '', '', 1, 22, NULL, '', '', '', '', ''),
(27, 10, 'gnome-0-6-mes', 'GNOME 0-6 мес', NULL, '', '', '', '', '', 1, 23, NULL, '', '', '', '', ''),
(28, 10, 'blanket-0-6-mes', 'BLANKET 0-6 мес', NULL, '', '', '', '', '', 1, 24, NULL, '', '', '', '', ''),
(29, 10, 'zip-0-6-mes', 'ZIP 0-6 мес', NULL, '', '', '', '', '', 1, 25, NULL, '', '', '', '', ''),
(30, 10, 'wheel-0-1-goda', 'WHEEL 0-1 года', NULL, '', '', '', '', '', 1, 26, NULL, '', '', '', '', ''),
(31, 10, 'summer-0-6-mes', 'SUMMER 0-6 мес', NULL, '', '', '', '', '', 1, 27, NULL, '', '', '', '', ''),
(32, 1, 'mini-0-6-mes', 'MINI 0-6 мес', NULL, '', '', '', '', '', 1, 28, NULL, '', '', '', '', ''),
(33, 1, 'little-6-15-mes', 'LITTLE 6-15 мес', NULL, '', '', '', '', '', 1, 29, NULL, '', '', '', '', ''),
(34, 1, 'step-2-6-let', 'STEP 2-6 лет', NULL, '', '', '', '', '', 1, 30, NULL, '', '', '', '', ''),
(35, 2, 'ski-j-2-8-let', 'SKI J 2-8 лет', NULL, '', '', '', '', '', 1, 31, NULL, '', '', '', '', ''),
(36, 2, 'parka-2-8-let', 'PARKA 2-8 лет', NULL, '', '', '', '', '', 1, 32, NULL, '', '', '', '', ''),
(37, 15, 's-kapyushonom-0-6-mes', 'С КАПЮШОНОМ 0-6 мес', NULL, '', '', '', '', '', 1, 33, NULL, '', '', '', '', ''),
(38, 15, 'bez-kapyushona-0-6-mes', 'БЕЗ КАПЮШОНА 0-6 мес', NULL, '', '', '', '', '', 1, 34, NULL, '', '', '', '', ''),
(39, 16, 'star-s-kapyushonom-0-15-mes', 'STAR С КАПЮШОНОМ 0-15 мес', NULL, '', '', '', '', '', 1, 35, NULL, '', '', '', '', ''),
(40, 16, 'star-bez-kapyushona0-15-mes', 'STAR БЕЗ КАПЮШОНА0-15 мес', NULL, '', '', '', '', '', 1, 36, NULL, '', '', '', '', ''),
(41, 17, 'ziptwist-2-8-let', 'ZIPTWIST 2-8 лет', NULL, '', '', '', '', '', 1, 37, NULL, '', '', '', '', ''),
(42, 20, 'tu-tu-0-6-let', 'TU-TU 0-6 лет', NULL, '', '', '', '', '', 1, 38, NULL, '', '', '', '', ''),
(43, 20, 'american', 'AMERICAN', NULL, '', '', '', '', '', 1, 39, NULL, '', '', '', '', ''),
(44, 20, 'clasic', 'CLASIC', NULL, '', '', '', '', '', 1, 40, NULL, '', '', '', '', ''),
(45, 20, 'party', 'PARTY', NULL, '', '', '', '', '', 1, 41, NULL, '', '', '', '', ''),
(46, 8, 'cool', 'COOL', NULL, '', '', '', '', '', 1, 42, NULL, '', '', '', '', ''),
(47, 8, 'optima', 'OPTIMA', NULL, '', '', '', '', '', 1, 43, NULL, '', '', '', '', ''),
(48, 8, 'woolly', 'WOOLLY', NULL, '', '', '', '', '', 1, 44, NULL, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_coupon`
--

CREATE TABLE IF NOT EXISTS `lw_store_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `registered_user` tinyint(4) NOT NULL DEFAULT '0',
  `free_shipping` tinyint(4) NOT NULL DEFAULT '0',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_per_user` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_delivery`
--

CREATE TABLE IF NOT EXISTS `lw_store_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT NULL,
  `available_from` float(10,2) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `separate_payment` tinyint(4) DEFAULT '0',
  `class_delivery` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lw_store_delivery_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_delivery`
--

INSERT INTO `lw_store_delivery` (`id`, `name`, `description`, `price`, `free_from`, `available_from`, `position`, `status`, `separate_payment`, `class_delivery`) VALUES
(1, 'Самовывоз', 'Самовывоз товара возможен по адресу: 460000, г. Оренбург, пер. Скорняжный, д. 12, корпус 1, 2-ой этаж\r\n', 0.00, NULL, NULL, 1, 1, 0, 'self'),
(2, 'СДЭК (самовывоз из пункта доставки)', '<p>Ваш заказ будет доставлен в пункт самовывоза по адресу, указанному при оформлении заказа.</p>', 0.00, NULL, NULL, 2, 1, 0, 'sdek-pvz'),
(3, 'Почта России', '<p>Платная доставка до ближайшего отделения Почты России</p>', 0.00, NULL, NULL, 4, 1, 0, 'post-rf'),
(4, 'СДЭК (курьерская доставка)', '<p>Ваш заказ будет доставлен курьером лично в руки по адресу, указанному при оформлении заказа.</p>', 0.00, NULL, NULL, 3, 1, 0, 'sdek-courier'),
(5, 'Доставка курьером Почты России (EMS)', '<p>  Платная доставка курьером Почты России (EMS)</p>', 0.00, NULL, NULL, 5, 1, 0, 'post-rf-ems');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_delivery_payment`
--

CREATE TABLE IF NOT EXISTS `lw_store_delivery_payment` (
  `delivery_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`delivery_id`,`payment_id`),
  KEY `fk_lw_store_delivery_payment_payment` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_delivery_payment`
--

INSERT INTO `lw_store_delivery_payment` (`delivery_id`, `payment_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_order`
--

CREATE TABLE IF NOT EXISTS `lw_store_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_method_id` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `payment_time` datetime DEFAULT NULL,
  `payment_details` text,
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `separate_delivery` tinyint(4) DEFAULT '0',
  `status_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(1024) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `note` varchar(1024) NOT NULL DEFAULT '',
  `modified` datetime DEFAULT NULL,
  `zipcode` varchar(30) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `house` varchar(50) DEFAULT NULL,
  `apartment` varchar(10) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `orderId` varchar(150) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `regionName` varchar(255) DEFAULT NULL,
  `regionCode` varchar(255) DEFAULT NULL,
  `regionCodeEx` varchar(255) DEFAULT NULL,
  `cityName` varchar(255) DEFAULT NULL,
  `cityCode` int(11) DEFAULT NULL,
  `family` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `pvz_id` varchar(100) DEFAULT NULL,
  `pvz_address` varchar(1000) DEFAULT NULL,
  `tariff_id` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `udx_lw_store_order_url` (`url`),
  KEY `idx_lw_store_order_date` (`date`),
  KEY `idx_lw_store_order_paid` (`paid`),
  KEY `idx_lw_store_order_status` (`status_id`),
  KEY `idx_lw_store_order_user_id` (`user_id`),
  KEY `fk_lw_store_order_delivery` (`delivery_id`),
  KEY `fk_lw_store_order_manager` (`manager_id`),
  KEY `fk_lw_store_order_payment` (`payment_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_order_coupon`
--

CREATE TABLE IF NOT EXISTS `lw_store_order_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lw_store_order_coupon_coupon` (`coupon_id`),
  KEY `fk_lw_store_order_coupon_order` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_order_product`
--

CREATE TABLE IF NOT EXISTS `lw_store_order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `variants` text,
  `variants_text` varchar(1024) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lw_store_order_product_order_id` (`order_id`),
  KEY `idx_lw_store_order_product_product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_order_sdek_data`
--

CREATE TABLE IF NOT EXISTS `lw_store_order_sdek_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `pvz_id` varchar(100) DEFAULT NULL,
  `pvz_address` varchar(1000) DEFAULT NULL,
  `tariff_id` int(11) DEFAULT NULL,
  `min_days` int(11) DEFAULT NULL,
  `max_days` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_order_status`
--

CREATE TABLE IF NOT EXISTS `lw_store_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_order_status`
--

INSERT INTO `lw_store_order_status` (`id`, `name`, `is_system`, `color`) VALUES
(1, 'Новый', 1, 'default'),
(2, 'Принят', 1, 'info'),
(3, 'Выполнен', 1, 'success'),
(4, 'Удален', 1, 'danger');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_payment`
--

CREATE TABLE IF NOT EXISTS `lw_store_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `settings` text,
  `currency_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_lw_store_payment_position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_payment`
--

INSERT INTO `lw_store_payment` (`id`, `module`, `name`, `description`, `settings`, `currency_id`, `position`, `status`) VALUES
(1, 'sberbank', 'Онлайн на сайте', '<p>Банковскими картами Visa, Master Card</p>', 'a:8:{s:8:\"userName\";s:13:\"araks56_1-api\";s:8:\"password\";s:9:\"araks56_1\";s:6:\"server\";s:39:\"https://3dsec.sberbank.ru/payment/rest/\";s:8:\"merchant\";s:0:\"\";s:18:\"sessionTimeoutSecs\";s:0:\"\";s:9:\"returnUrl\";s:38:\"http://little-twist.ru/payment/success\";s:7:\"failUrl\";s:35:\"http://little-twist.ru/payment/fail\";s:8:\"language\";s:2:\"ru\";}', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_producer`
--

CREATE TABLE IF NOT EXISTS `lw_store_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_lw_store_producer_slug` (`slug`),
  KEY `ix_lw_store_producer_sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product`
--

CREATE TABLE IF NOT EXISTS `lw_store_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `video` varchar(255) DEFAULT NULL,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_store_product_alias` (`slug`),
  KEY `ix_lw_store_product_create_time` (`create_time`),
  KEY `ix_lw_store_product_discount_price` (`discount_price`),
  KEY `ix_lw_store_product_name` (`name`),
  KEY `ix_lw_store_product_position` (`position`),
  KEY `ix_lw_store_product_price` (`price`),
  KEY `ix_lw_store_product_producer_id` (`producer_id`),
  KEY `ix_lw_store_product_sku` (`sku`),
  KEY `ix_lw_store_product_status` (`status`),
  KEY `ix_lw_store_product_type_id` (`type_id`),
  KEY `ix_lw_store_product_update_time` (`update_time`),
  KEY `lw_store_product_external_id_ix` (`external_id`),
  KEY `fk_lw_store_product_category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_product`
--

INSERT INTO `lw_store_product` (`id`, `type_id`, `producer_id`, `category_id`, `sku`, `name`, `slug`, `price`, `discount_price`, `discount`, `description`, `short_description`, `data`, `video`, `is_special`, `length`, `width`, `height`, `weight`, `quantity`, `in_stock`, `status`, `create_time`, `update_time`, `meta_title`, `meta_keywords`, `meta_description`, `image`, `average_price`, `purchase_price`, `recommended_price`, `position`, `external_id`, `title`, `meta_canonical`, `image_alt`, `image_title`, `view`) VALUES
(19, 1, NULL, 38, 'COC0219', 'КОКОН сер велюр/бел', 'kokon-ser-velyurbel', '990.000', NULL, NULL, '<p>Кокон пригодится зимой (под конверт), а также в теплое демисезонное время или прохладное лето. Верх выполнен из трикотажного хлопка, хлопкового велюра или плюша, утеплитель Termofinn, подклад - 100% хлопок высокого качества пенье. Крой выполнен с учетом потребностей малыша в этом возрасте. В Коконе детки будут чувствовать себя очень уютно и тепло.</p>', '', '', 'https://www.youtube.com/watch?v=x7UC4YW2k2U', 0, '0.400', '0.400', '0.200', '0.500', NULL, 1, 1, '2019-10-11 13:08:09', '2019-10-17 09:18:10', '', '', '', 'bacd9192248de592e13d2b4443d77308.jpg', NULL, NULL, NULL, 1, NULL, '', '', '', '', ''),
(20, 1, NULL, 27, 'GN0319', 'КОНВЕРТ ГНОМ черный/горчица', 'konvert-gnom-chernyygorchica', '2290.000', NULL, NULL, '<p>Конверт ГНОМ можно использовать с рождения и до 6 месяцев. Верхняя ткань ДЮСПО с пропитками WR PU обладает водо- и грязеотталкивающими, а также ветрозащитными свойствами. Технологичный утеплитель Холлофайбер, в зимнем исполнении до -30 градусов. Подклад выполнен из хлопкового трикотажа, велюра или плюша. Молния проходит по всему краю конверта, капюшон и отворот конверта на стягивающейся мягкой резинке с фиксаторами – все это позволяет не беспокоить спящего малыша при раздевании и одевании;  Помпоны на завязках, съемные. Меховые помпоны натуральные, в зависимости от выбранного вами цвета: енот, лиса или песец.</p>', '', '', 'https://www.youtube.com/watch?v=x7UC4YW2k2U', 0, '0.400', '0.400', '0.200', '0.500', NULL, 1, 1, '2019-10-11 13:12:41', '2019-10-17 08:07:16', '', '', '', '8f5d88247086d7ad12e690feee58bd37.jpg', NULL, NULL, NULL, 2, NULL, '', '', '', '', ''),
(21, 1, NULL, 38, 'COC0119', 'КОКОН ирис/белый', 'kokon-irisbelyy', '990.000', NULL, NULL, '<p>Кокон пригодится зимой (под конверт), а также в теплое демисезонное время или прохладное лето. Верх выполнен из трикотажного хлопка, хлопкового велюра или плюша, утеплитель Termofinn, подклад - 100% хлопок высокого качества пенье. Крой выполнен с учетом потребностей малыша в этом возрасте. В Коконе детки будут чуствовать себя очень уютно и тепло.</p>', '', '', '', 0, '0.200', '0.200', '0.050', '0.500', NULL, 1, 1, '2019-10-11 13:29:15', '2019-10-17 09:18:18', '', '', '', '572dd0272c3a811330eb89e1a6c18215.jpg', NULL, NULL, NULL, 3, NULL, '', '', '', '', ''),
(22, 1, NULL, 27, 'GN0219', 'КОНВЕРТ ГНОМ НЭВИ/СЕР ВЕЛЮР', 'konvert-gnom-neviser-velyur', '2290.000', NULL, NULL, '<p>Конверт ГНОМ можно использовать с рождения и до 6 месяцев. Верхняя ткань ДЮСПО с пропитками WR PU обладает водо- и грязеотталкивающими, а также ветрозащитными свойствами. Технологичный утеплитель Холлофайбер, в зимнем исполнении до -30 градусов. Подклад выполнен из хлопкового трикотажа, велюра или плюша. Молния проходит по всему краю конверта, капюшон и отворот конверта на стягивающейся мягкой резинке с фиксаторами – все это позволяет не беспокоить спящего малыша при раздевании и одевании;  Помпоны на завязках, съемные. Меховые помпоны натуральные, в зависимости от выбранного вами цвета: енот, лиса или песец.</p>', '', '', 'https://www.youtube.com/watch?v=x7UC4YW2k2U', 0, '0.400', '0.400', '0.200', '0.500', NULL, 1, 1, '2019-10-11 13:33:57', '2019-10-17 07:35:18', '', '', '', 'a16d7322676aad2e7677f9ed3bf44754.jpg', NULL, NULL, NULL, 4, NULL, '', '', '', '', ''),
(23, 1, NULL, 27, 'GN0119', 'КОНВЕРТ ГНОМ ФИОЛЕТ/СЕР ВЕЛЮР', 'konvert-gnom-fioletser-velyur', '2290.000', NULL, NULL, '<p>Конверт ГНОМ можно использовать с рождения и до 6 месяцев. Верхняя ткань ДЮСПО с пропитками WR PU обладает водо- и грязеотталкивающими, а также ветрозащитными свойствами. Технологичный утеплитель Холлофайбер, в зимнем исполнении до -30 градусов. Подклад выполнен из хлопкового трикотажа, велюра или плюша. Молния проходит по всему краю конверта, капюшон и отворот конверта на стягивающейся мягкой резинке с фиксаторами – все это позволяет не беспокоить спящего малыша при раздевании и одевании;  Помпоны на завязках, съемные. Меховые помпоны натуральные, в зависимости от выбранного вами цвета: енот, лиса или песец.</p>', '', '', 'https://www.youtube.com/watch?v=x7UC4YW2k2U', 0, '0.400', '0.400', '0.200', '0.500', NULL, 1, 1, '2019-10-11 13:37:45', '2019-10-17 09:52:19', '', '', '', '1ddce4fbcb27113132ce827c275919ac.jpg', NULL, NULL, NULL, 5, NULL, '', '', '', '', ''),
(25, 1, NULL, 41, 'ZT0119', 'КОМБИНЕЗОН ЗИП ТВИСТ ФУКСИЯ', 'kombinezon-zip-tvist-fuksiya', '1290.000', NULL, NULL, '<p>Комбинезон ЗИП ТВИСТ выполнен из мягкого и приятного на ощупь флис. Флис имеет специальную обработку - антипилинг, которая позволяет полотну не \"скатываться\" даже после многочисленных стирок и использования. Все молнии имеют защитные планки, к телу ребенка фурнитура не касается. Для 80-86 размера предусмотрена молния сзади - очень удобно в использовании для маленьких деток.</p>', '', '', '', 0, '0.300', '0.200', '0.100', '0.500', NULL, 1, 1, '2019-10-15 13:35:12', '2019-10-17 09:21:30', '', '', '', '9f68e59d5ff014791fe696146489218e.jpg', NULL, NULL, NULL, 6, NULL, '', '', '', '', ''),
(26, 1, NULL, 23, '', 'ШАПКА СЮРПРИЗ голубой', 'shapka-syurpriz-goluboy', '590.000', NULL, NULL, '<p>30%шерсть, 70%ПАН; подклад: 95%хлопок, 5%эластан: утеплитель изософт Зимняя шапочка для малышей. Верхний слой связан полушерстяной пряжи, нижний слой выполнен из трикотажного хлопкового полотна. Внутри утеплитель изософт. На макушке 2 помпона, сбоку пришит лейбл. Аккуратные завязки без узлов и наконечников. Выбирайте размер, ориентируясь на меньшее число (из-за утеплителя модель обладает умеренной растяжимостью). Подойдет на температуру до минус 20 градусов.</p>', '', '', '', 0, '0.150', '0.200', '0.050', '0.200', NULL, 1, 1, '2019-10-15 13:59:12', '2019-10-16 10:10:13', '', '', '', 'ed00ae283628baf1489604872686de4d.jpg', NULL, NULL, NULL, 7, NULL, '', '', '', '', ''),
(27, 1, NULL, 24, '', 'ШАПКА ИМПУЛЬС', 'shapka-impuls', '490.000', NULL, NULL, '<p>100% ПАН, подклад 95%хлопок, 5% эластан, утеплитель изософт. Модная зимняя шапочка для мальчиков. Верхний слой связан из мягкой акриловой пряжи, нижний слой из хлопкового полотна и полностью пришит. Внутри утеплитель изософт. Сбоку фирменный лейбл. Ушки-завязки пришиты вручную, при желании их можно легко отсоединить. Выбирая размер, ориентируйтесь на меньшее число (из-за подклада и утеплителя модель обладает умеренной растяжимостью). Подойдет на температуру до минус 25 градусов.</p>', '', '', '', 0, '0.150', '0.200', '0.050', '0.200', NULL, 1, 1, '2019-10-15 14:01:31', '2019-10-16 10:10:33', '', '', '', '0340b7ac95e486b4c5ee21db0bd3b60d.jpg', NULL, NULL, NULL, 8, NULL, '', '', '', '', ''),
(28, 1, NULL, 23, '', 'ШАПКА КАПОР елочка серый', 'shapka-kapor-elochka-seryy', '890.000', NULL, NULL, '<p>30% шерсть, 70% ПАН; подклад: 95%хлопок, 5% эластан; утеплитель изософт Малыш не любит шапки? Попробуйте капор! Шапочка застегивается на удобную пуговку, и ее легко снимать даже со спящего ребенка. Верхний слой связан из полушерстяной мериносовой пряжи. Подклад выкроен из хлопкового полотна и полностью пришит. Утеплитель изософт. На макушке два небольших помпона. Они привязаны, можно отвязать. Выбирайте размер, ориентируясь на меньшее число (из-за утеплителя модель обладает умеренной растяжимостью). Подойдет на температуру до -20.</p>', '', '', '', 0, '0.150', '0.200', '0.050', '0.200', NULL, 1, 1, '2019-10-15 14:03:47', '2019-10-16 10:10:53', '', '', '', '7888c23c0a37230c70c7325e72c17504.jpg', NULL, NULL, NULL, 9, NULL, '', '', '', '', ''),
(29, 1, NULL, 24, '', 'ШАПКА ШЛЕМ СЕРЕБРО БЕЛЫЙ', 'shapka-shlem-serebro-belyy', '990.000', NULL, NULL, '<p>30% шерсть, 70% пан, подклад 95%хлопок, 5% эластан, утеплитель изософт. Очень теплый зимний шлем для девочек. Вырез вокруг лица не просто вырезан и подшит, а обработан по специальной технологии эластичной вязаной бейкой. Благодаря ей вырез не вытягивается в процессе носки, шлемик плотно облегает лицо. Когда ребенок вертит головой, уши надежно защищены. Манишка шлема анатомически повторяет форму шеи и плеч, поэтому не выезжает за воротник одежды и не собирается трубой. На макушке 2 помпона. Верх из полушерстяной мериносовой пряжи, подклад выкроен из хлопкового полотна и полностью пришит. Утеплитель - изософт. Модель подойдет на морозную зиму до -25. Наша рекомендация: брать только в размер!</p>', '', '', '', 0, '0.150', '0.200', '0.050', '0.200', NULL, 1, 1, '2019-10-15 14:05:26', '2019-10-16 10:11:11', '', '', '', '9190d43614e104fac2d5f1114f6f3061.jpg', NULL, NULL, NULL, 10, NULL, '', '', '', '', ''),
(30, 1, NULL, 32, 'MINI0119', 'КОМБИНЕЗОН МИНИ НЭВИ/БЕЛЫЙ', 'kombinezon-mini-nevibelyy', '2990.000', NULL, NULL, '<p>Комбинезон МИНИ можно использовать с рождения и до 6 месяцев. Верхняя ткань ДЮСПО с пропитками WR PU обладает водо- и грязеотталкивающими, а также ветрозащитными свойствами. Утеплитель Alpolux, в зимнем исполнении до -30 градусов. Подклад выполнен из хлопкового трикотажа или велюра высокого качества. Благодаря устройству молний (доходит почти до конца ножек) и капюшону на мягкой резинке комбинезон ОЧЕНЬ удобен в использовании, позволяет не беспокоить спящего малыша при раздевании и одевании. Ручки и ножки оснащены удобными легко отворачивающимися антицарапками. Манжеты с эластичной резинкой, эластичная утяжка по спинке. Помпоны на завязках, съемные. Меховые помпоны натуральные, в зависимости от выбранного вами цвета: енот, лиса или песец.Пинетки увеличивают комбинезон на один размер!</p>', '', '', 'https://www.youtube.com/watch?v=W_n7IHnHihc', 0, '0.350', '0.300', '0.200', '0.500', NULL, 1, 1, '2019-10-15 14:09:08', '2019-10-17 09:54:06', '', '', '', '41c6e57774f679675a9b0fa6a732c31d.jpg', NULL, NULL, NULL, 11, NULL, '', '', '', '', ''),
(31, 1, NULL, 34, 'ST0119', 'КОМБИНЕЗОН СТЭП ЧЕРНЫЙ', 'kombinezon-step-chernyy', '3290.000', NULL, NULL, '<p>Комбинезон СТЭП можно приобрести в зимнем и демисезонном вариантах. Преимущества: Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами; Утеплитель Alpolux, в зимнем варианте до -30 градусов. Благодаря устройству молнии (доходит до середины ножки) ОЧЕНЬ удобен в использовании, позволяет ребенку самому беспрепятственно одеться;          Манжеты с эластичной резинкой, эластичная утяжка по спинке, ветрозащитная планка, светоотражающие элементы, съемные штрипки (позволяют сохранять натянутую форму штанины);   ОЧЕНЬ УДОБНЫЕ лямки indoors на внутренней стороне спинки, могут пригодится в помещении;          На рукавах есть специальные крепления для краг; Капюшон съемный, на кнопках. Опушка легко отстегивается. Меховые опушки натуральные, в зависимости от выбранного вами цвета: енот/лиса/песец.</p>', '', '', 'https://www.youtube.com/watch?v=ZIrVXLS3NH8', 0, '0.400', '0.400', '0.200', '1.000', NULL, 1, 1, '2019-10-16 07:20:38', '2019-10-17 09:54:46', '', '', '', 'ec84b3a49b0f02bfc9e40ad66b55d221.jpg', NULL, NULL, NULL, 12, NULL, '', '', '', '', ''),
(32, 1, NULL, 34, 'ST0219', 'КОМБИНЕЗОН СТЭП БОРДО', 'kombinezon-step-bordo', '3290.000', NULL, NULL, '<p>Комбинезон СТЭП можно приобрести в зимнем и демисезонном вариантах. Преимущества: Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами; Утеплитель Alpolux, в зимнем варианте до -30 градусов. Благодаря устройству молнии (доходит до середины ножки) ОЧЕНЬ удобен в использовании, позволяет ребенку самому беспрепятственно одеться;          Манжеты с эластичной резинкой, эластичная утяжка по спинке, ветрозащитная планка, светоотражающие элементы, съемные штрипки (позволяют сохранять натянутую форму штанины);   ОЧЕНЬ УДОБНЫЕ лямки indoors на внутренней стороне спинки, могут пригодится в помещении;          На рукавах есть специальные крепления для краг; Капюшон съемный, на кнопках. Опушка легко отстегивается. Меховые опушки натуральные, в зависимости от выбранного вами цвета: енот/лиса/песец.</p>', '', '', 'https://www.youtube.com/watch?v=ZIrVXLS3NH8', 0, '0.400', '0.400', '0.200', '1.000', NULL, 1, 1, '2019-10-16 07:24:20', '2019-10-17 15:49:40', '', '', '', '392bb2d683f064303b4dcad0a3efecaf.jpg', NULL, NULL, NULL, 13, NULL, '', '', '', '', ''),
(33, 1, NULL, 34, 'ST0419', 'КОМБИНЕЗОН СТЭП СВЕТЛО-СЕРЫЙ', 'kombinezon-step-svetlo-seryy', '3290.000', NULL, NULL, '<p>Комбинезон СТЭП можно приобрести в зимнем и демисезонном вариантах. Преимущества: Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами; Утеплитель Alpolux, в зимнем варианте до -30 градусов. Благодаря устройству молнии (доходит до середины ножки) ОЧЕНЬ удобен в использовании, позволяет ребенку самому беспрепятственно одеться;          Манжеты с эластичной резинкой, эластичная утяжка по спинке, ветрозащитная планка, светоотражающие элементы, съемные штрипки (позволяют сохранять натянутую форму штанины);   ОЧЕНЬ УДОБНЫЕ лямки indoors на внутренней стороне спинки, могут пригодится в помещении;          На рукавах есть специальные крепления для краг; Капюшон съемный, на кнопках. Опушка легко отстегивается. Меховые опушки натуральные, в зависимости от выбранного вами цвета: енот/лиса/песец.</p>', '', '', 'https://www.youtube.com/watch?v=ZIrVXLS3NH8', 0, '0.400', '0.400', '0.200', '1.000', NULL, 1, 1, '2019-10-16 07:26:25', '2019-10-17 08:48:22', '', '', '', '5736092a63b5977fa9c95d7bca2c4518.jpg', NULL, NULL, NULL, 14, NULL, '', '', '', '', ''),
(34, 1, NULL, 34, 'ST1119', 'КОМБИНЕЗОН СТЭП ГРАФФИТИ', 'kombinezon-step-graffiti', '3690.000', NULL, NULL, '<p>Комбинезон СТЭП можно приобрести в зимнем и демисезонном вариантах. Преимущества: Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами; Утеплитель Alpolux, в зимнем варианте до -30 градусов. Благодаря устройству молнии (доходит до середины ножки) ОЧЕНЬ удобен в использовании, позволяет ребенку самому беспрепятственно одеться;          Манжеты с эластичной резинкой, эластичная утяжка по спинке, ветрозащитная планка, светоотражающие элементы, съемные штрипки (позволяют сохранять натянутую форму штанины);   ОЧЕНЬ УДОБНЫЕ лямки indoors на внутренней стороне спинки, могут пригодится в помещении;          На рукавах есть специальные крепления для краг; Капюшон съемный, на кнопках. Опушка легко отстегивается. Меховые опушки натуральные, в зависимости от выбранного вами цвета: енот/лиса/песец.</p>', '', '', 'https://www.youtube.com/watch?v=ZIrVXLS3NH8', 0, '0.400', '0.400', '0.200', '1.000', NULL, 1, 1, '2019-10-16 07:28:29', '2019-10-17 15:51:46', '', '', '', '1997141829a8bf3793b7236e7d199f82.jpg', NULL, NULL, NULL, 15, NULL, '', '', '', '', ''),
(35, 1, NULL, 40, 'STAR0319', 'КОМБИНЕЗОН СТАР бирюза/бел', 'kombinezon-star-biryuzabel', '1490.000', NULL, NULL, '<p>Комбинезон СТАР пригодится зимой (под конверт или комбинезон), а также в теплое демисезонное время или прохладное лето. Верх выполнен из трикотажного хлопка, хлопкового велюра или плюша, утеплитель Termofinn, подклад - 100% хлопок высокого качества пенье. Крой выполнен с учетом потребностей малыша в этом возрасте. Ручки и ножки оснащены удобными легко отворачивающимися антицарапками (только для 62 размера). Удлиненные манжеты выполнены из хлопковой рибаны высокого качества пенье, позволяют использовать комбинезон дольше (по мере роста ребенка).</p>', '', '', 'https://www.youtube.com/watch?v=3gTHYKgly4Q', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-16 07:30:35', '2019-10-17 12:14:32', '', '', '', '6b5ca20fcf9e7cbc2ac068d8e65ba444.jpg', NULL, NULL, NULL, 16, NULL, '', '', '', '', ''),
(36, 1, NULL, 40, 'STAR0519', 'КОМБИНЕЗОН СТАР ирисы/бел', 'kombinezon-star-irisybel', '1390.000', NULL, NULL, '<p>Комбинезон СТАР пригодится зимой (под конверт или комбинезон), а также в теплое демисезонное время или прохладное лето. Верх выполнен из трикотажного хлопка, хлопкового велюра или плюша, утеплитель Termofinn, подклад - 100% хлопок высокого качества пенье. Крой выполнен с учетом потребностей малыша в этом возрасте. Ручки и ножки оснащены удобными легко отворачивающимися антицарапками (только для 62 размера). Удлиненные манжеты выполнены из хлопковой рибаны высокого качества пенье, позволяют использовать комбинезон дольше (по мере роста ребенка).</p>', '', '', 'https://www.youtube.com/watch?v=SvCKdhNJAQ0', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-16 07:34:15', '2019-10-17 12:15:03', '', '', '', '6304af4406dd89d8be0cada18df6c6b3.jpg', NULL, NULL, NULL, 17, NULL, '', '', '', '', ''),
(37, 1, NULL, 40, 'STAR0619', 'КОМБИНЕЗОН СТАР сер велюр/белый', 'kombinezon-star-ser-velyurbelyy', '1390.000', NULL, NULL, '<p>Комбинезон СТАР пригодится зимой (под конверт или комбинезон), а также в теплое демисезонное время или прохладное лето. Верх выполнен из трикотажного хлопка, хлопкового велюра или плюша, утеплитель Termofinn, подклад - 100% хлопок высокого качества пенье. Крой выполнен с учетом потребностей малыша в этом возрасте. Ручки и ножки оснащены удобными легко отворачивающимися антицарапками (только для 62 размера). Удлиненные манжеты выполнены из хлопковой рибаны высокого качества пенье, позволяют использовать комбинезон дольше (по мере роста ребенка).</p>', '', '', 'https://www.youtube.com/watch?v=SvCKdhNJAQ0', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-16 07:37:02', '2019-10-17 12:15:14', '', '', '', 'add875e4fc6fd167461d7fa89edb3a1a.jpg', NULL, NULL, NULL, 18, NULL, '', '', '', '', ''),
(38, 1, NULL, 7, 'BL01', 'РЕМЕНЬ МЕМБРАНА ЧЕРНЫЙ', 'remen-membrana-chernyy', '190.000', NULL, NULL, '<p>Ремни подходят к комбинезонам СТЭП, куртке СКАЙ ДЖИ и Парке. Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами. Утеплитель Alpolux или Холофайбер, подклад выполнен из флиса антипилинг. На внешней стороне краг нашита светоотражающая полоска.</p>', '', '', '', 0, '0.150', '0.150', '0.050', '0.200', NULL, 1, 1, '2019-10-16 07:39:56', '2019-10-17 08:39:36', '', '', '', 'b7f921a5ffc32ae836b1b7567b4be1dd.jpg', NULL, NULL, NULL, 19, NULL, '', '', '', '', ''),
(39, 1, NULL, 7, 'BL03', 'РЕМЕНЬ МЕМБРАНА БОРДО', 'remen-membrana-bordo', '390.000', NULL, NULL, '<p>Ремни подходят к комбинезонам СТЭП, куртке СКАЙ ДЖИ и Парке. Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами. Утеплитель Alpolux или Холофайбер, подклад выполнен из флиса антипилинг. На внешней стороне краг нашита светоотражающая полоска.</p>', '', '', '', 0, '0.150', '0.150', '0.050', '0.200', NULL, 1, 1, '2019-10-16 07:43:17', '2019-10-17 08:40:46', '', '', '', 'b22be0ab3f993ee0a715770d763f73a8.jpg', NULL, NULL, NULL, 20, NULL, '', '', '', '', ''),
(40, 1, NULL, 7, 'KR01', 'КРАГИ МЕМБРАНА ЧЕРНЫЕ', 'kragi-membrana-chernye', '390.000', NULL, NULL, '<p>Краги подходят к комбинезонам СТЭП, куртке СКАЙ ДЖИ и Парке. Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами. Утеплитель Alpolux или Холофайбер, подклад выполнен из флиса антипилинг. На внешней стороне краг нашита светоотражающая полоска.</p>', '', '', '', 0, '0.150', '0.150', '0.050', '0.200', NULL, 1, 1, '2019-10-16 07:43:58', '2019-10-17 09:14:26', '', '', '', 'ec3bf907e5fde5529dde1061239961af.jpg', NULL, NULL, NULL, 21, NULL, '', '', '', '', ''),
(41, 1, NULL, 7, 'KR03', 'КРАГИ МЕМБРАНА БОРДО', 'kragi-membrana-bordo', '390.000', NULL, NULL, '<p>Краги подходят к комбинезонам СТЭП, куртке СКАЙ ДЖИ и Парке. Верхняя мембранная ткань «дышит», в тоже время обладая водо-грязеотталкивающими и ветрозащитными свойствами. Утеплитель Alpolux или Холофайбер, подклад выполнен из флиса антипилинг. На внешней стороне краг нашита светоотражающая полоска.</p>', '', '', '', 0, '0.150', '0.150', '0.050', '0.200', NULL, 1, 1, '2019-10-16 07:44:58', '2019-10-17 09:15:32', '', '', '', 'ed9d274525e8e42eb924b972f951e7af.jpg', NULL, NULL, NULL, 22, NULL, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_attribute_value`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_attribute_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lw_ix_product_attribute_number_value` (`number_value`),
  KEY `lw_ix_product_attribute_string_value` (`string_value`),
  KEY `lw_fk_product_attribute_attribute` (`attribute_id`),
  KEY `lw_fk_product_attribute_option` (`option_value`),
  KEY `lw_fk_product_attribute_product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_product_attribute_value`
--

INSERT INTO `lw_store_product_attribute_value` (`id`, `product_id`, `attribute_id`, `number_value`, `string_value`, `text_value`, `option_value`, `create_time`) VALUES
(34, 19, 2, NULL, NULL, NULL, NULL, NULL),
(35, 19, 3, NULL, NULL, NULL, NULL, NULL),
(36, 19, 4, NULL, NULL, NULL, NULL, NULL),
(37, 19, 1, NULL, '', NULL, NULL, NULL),
(38, 20, 2, NULL, NULL, NULL, NULL, NULL),
(39, 20, 3, NULL, NULL, NULL, NULL, NULL),
(40, 20, 4, NULL, NULL, NULL, NULL, NULL),
(41, 20, 1, NULL, '', NULL, NULL, NULL),
(42, 21, 2, NULL, NULL, NULL, NULL, NULL),
(43, 21, 3, NULL, NULL, NULL, NULL, NULL),
(44, 21, 4, NULL, NULL, NULL, NULL, NULL),
(45, 21, 1, NULL, '', NULL, NULL, NULL),
(46, 22, 2, NULL, NULL, NULL, NULL, NULL),
(47, 22, 3, NULL, NULL, NULL, NULL, NULL),
(48, 22, 4, NULL, NULL, NULL, NULL, NULL),
(49, 22, 1, NULL, '', NULL, NULL, NULL),
(50, 23, 2, NULL, NULL, NULL, NULL, NULL),
(51, 23, 3, NULL, NULL, NULL, NULL, NULL),
(52, 23, 4, NULL, NULL, NULL, NULL, NULL),
(53, 23, 1, NULL, '', NULL, NULL, NULL),
(58, 25, 2, NULL, NULL, NULL, NULL, NULL),
(59, 25, 3, NULL, NULL, NULL, NULL, NULL),
(60, 25, 4, NULL, NULL, NULL, NULL, NULL),
(61, 25, 1, NULL, '', NULL, NULL, NULL),
(62, 26, 2, NULL, NULL, NULL, NULL, NULL),
(63, 26, 3, NULL, NULL, NULL, NULL, NULL),
(64, 26, 4, NULL, NULL, NULL, NULL, NULL),
(65, 26, 1, NULL, '', NULL, NULL, NULL),
(66, 27, 2, NULL, NULL, NULL, NULL, NULL),
(67, 27, 3, NULL, NULL, NULL, NULL, NULL),
(68, 27, 4, NULL, NULL, NULL, NULL, NULL),
(69, 27, 1, NULL, '', NULL, NULL, NULL),
(70, 28, 2, NULL, NULL, NULL, NULL, NULL),
(71, 28, 3, NULL, NULL, NULL, NULL, NULL),
(72, 28, 4, NULL, NULL, NULL, NULL, NULL),
(73, 28, 1, NULL, '', NULL, NULL, NULL),
(74, 29, 2, NULL, NULL, NULL, NULL, NULL),
(75, 29, 3, NULL, NULL, NULL, NULL, NULL),
(76, 29, 4, NULL, NULL, NULL, NULL, NULL),
(77, 29, 1, NULL, '', NULL, NULL, NULL),
(78, 30, 2, NULL, NULL, NULL, NULL, NULL),
(79, 30, 3, NULL, NULL, NULL, NULL, NULL),
(80, 30, 4, NULL, NULL, NULL, NULL, NULL),
(81, 30, 1, NULL, '', NULL, NULL, NULL),
(82, 31, 2, NULL, NULL, NULL, NULL, NULL),
(83, 31, 3, NULL, NULL, NULL, NULL, NULL),
(84, 31, 4, NULL, NULL, NULL, NULL, NULL),
(85, 31, 1, NULL, '', NULL, NULL, NULL),
(86, 32, 2, NULL, NULL, NULL, NULL, NULL),
(87, 32, 3, NULL, NULL, NULL, NULL, NULL),
(88, 32, 4, NULL, NULL, NULL, NULL, NULL),
(89, 32, 1, NULL, '', NULL, NULL, NULL),
(90, 33, 2, NULL, NULL, NULL, NULL, NULL),
(91, 33, 3, NULL, NULL, NULL, NULL, NULL),
(92, 33, 4, NULL, NULL, NULL, NULL, NULL),
(93, 33, 1, NULL, '', NULL, NULL, NULL),
(94, 34, 2, NULL, NULL, NULL, NULL, NULL),
(95, 34, 3, NULL, NULL, NULL, NULL, NULL),
(96, 34, 4, NULL, NULL, NULL, NULL, NULL),
(97, 34, 1, NULL, '', NULL, NULL, NULL),
(98, 35, 2, NULL, NULL, NULL, NULL, NULL),
(99, 35, 3, NULL, NULL, NULL, NULL, NULL),
(100, 35, 4, NULL, NULL, NULL, NULL, NULL),
(101, 35, 1, NULL, '', NULL, NULL, NULL),
(102, 36, 2, NULL, NULL, NULL, NULL, NULL),
(103, 36, 3, NULL, NULL, NULL, NULL, NULL),
(104, 36, 4, NULL, NULL, NULL, NULL, NULL),
(105, 36, 1, NULL, '', NULL, NULL, NULL),
(106, 37, 2, NULL, NULL, NULL, NULL, NULL),
(107, 37, 3, NULL, NULL, NULL, NULL, NULL),
(108, 37, 4, NULL, NULL, NULL, NULL, NULL),
(109, 37, 1, NULL, '', NULL, NULL, NULL),
(110, 38, 2, NULL, NULL, NULL, NULL, NULL),
(111, 38, 3, NULL, NULL, NULL, NULL, NULL),
(112, 38, 4, NULL, NULL, NULL, NULL, NULL),
(113, 38, 1, NULL, '', NULL, NULL, NULL),
(114, 39, 2, NULL, NULL, NULL, NULL, NULL),
(115, 39, 3, NULL, NULL, NULL, NULL, NULL),
(116, 39, 4, NULL, NULL, NULL, NULL, NULL),
(117, 39, 1, NULL, '', NULL, NULL, NULL),
(118, 40, 2, NULL, NULL, NULL, NULL, NULL),
(119, 40, 3, NULL, NULL, NULL, NULL, NULL),
(120, 40, 4, NULL, NULL, NULL, NULL, NULL),
(121, 40, 1, NULL, '', NULL, NULL, NULL),
(122, 41, 2, NULL, NULL, NULL, NULL, NULL),
(123, 41, 3, NULL, NULL, NULL, NULL, NULL),
(124, 41, 4, NULL, NULL, NULL, NULL, NULL),
(125, 41, 1, NULL, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_category`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_lw_store_product_category_category_id` (`category_id`),
  KEY `ix_lw_store_product_category_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_image`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lw_store_product_image_group` (`group_id`),
  KEY `fk_lw_store_product_image_product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_product_image`
--

INSERT INTO `lw_store_product_image` (`id`, `product_id`, `name`, `title`, `alt`, `group_id`) VALUES
(17, 30, '48e4054f900fcd71177592920491dbbd.jpg', '', '', NULL),
(18, 30, '07eef8575da7bb4d71614e97d3855638.jpg', '', '', NULL),
(19, 30, '9044fc00f55923bbee5632cf32d019e5.jpg', '', '', NULL),
(24, 31, 'eea4da4ab267193b2e8a160c654d588e.JPG', '', '', NULL),
(25, 31, '15de0edb16df6984e6b4c79de8551868.jpg', '', '', NULL),
(26, 31, 'fded502622da7d35b74db28cd87c4ea9.jpg', '', '', NULL),
(27, 31, '08c935b2d97a87a2cd6a58f479b37eb6.jpg', '', '', NULL),
(31, 33, 'de408a9988c2d8b2c47ed1f78c0f4f59.jpg', '', '', NULL),
(38, 33, '22ead879bfdfa0cd52c56190f1f09beb.jpg', '', '', NULL),
(41, 37, '662ac2a7ef972d0c898104f2145d89c0.jpg', '', '', NULL),
(42, 37, '4e1e910b91824631b0e614e84a11406c.jpg', '', '', NULL),
(44, 30, 'cd0e9e941ac17ec9091222b651b488c0.jpg', '', '', NULL),
(47, 22, '90cdfc2d92c884705da2216530066b81.jpg', '', '', NULL),
(48, 20, '57b8974788d159a3a8ed99c4e918aeb0.jpg', '', '', NULL),
(49, 23, 'ed4aca5b3b043d867df5f5c7dcc0af06.jpg', '', '', NULL),
(51, 19, '7aab7bb17e3673a0619d2de9a0c6b624.jpg', '', '', NULL),
(52, 21, '95e537c8a7fe16ec6959b30ae7a045aa.jpg', '', '', NULL),
(53, 36, 'f1de60a926605baa6f0968ead6c51b3f.jpg', '', '', NULL),
(54, 36, '457d49f099087bd688b138990fac96f8.jpg', '', '', NULL),
(55, 35, '92ae19a453d784e76cbcd69d5d357e20.jpg', '', '', NULL),
(56, 35, 'f30a8681215d3799d3a02db0bbc9ac48.jpg', '', '', NULL),
(57, 34, '6e62717616430d9f18c341db59189840.jpg', '', '', NULL),
(58, 34, '30d5dd1cdb03bf2f19173d4510ca8890.jpg', '', '', NULL),
(60, 32, '97a80c383f354f23b042b1cf517b270d.jpg', '', '', NULL),
(61, 32, 'e236f7f1d625bfdfe4a4633f2bdd6eb4.jpg', '', '', NULL),
(62, 32, '65347bf17449320aad51f85468b79362.jpg', '', '', NULL),
(63, 37, '506f9093997cdc2a05cb3be60d306e4a.jpg', '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_image_group`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_image_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_link`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_store_product_link_product` (`product_id`,`linked_product_id`),
  KEY `fk_lw_store_product_link_linked_product` (`linked_product_id`),
  KEY `fk_lw_store_product_link_type` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_product_link`
--

INSERT INTO `lw_store_product_link` (`id`, `type_id`, `product_id`, `linked_product_id`, `position`) VALUES
(4, 2, 23, 21, 1),
(5, 2, 32, 41, 2),
(6, 2, 31, 40, 3),
(7, 2, 23, 36, 4),
(8, 2, 30, 37, 5),
(9, 2, 32, 39, 6),
(10, 2, 31, 38, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_link_type`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_link_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_store_product_link_type_code` (`code`),
  UNIQUE KEY `ux_lw_store_product_link_type_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_product_link_type`
--

INSERT INTO `lw_store_product_link_type` (`id`, `code`, `title`) VALUES
(1, 'similar', 'Похожие'),
(2, 'related', 'Сопутствующие');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_product_variant`
--

CREATE TABLE IF NOT EXISTS `lw_store_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lw_store_product_variant_attribute` (`attribute_id`),
  KEY `idx_lw_store_product_variant_product` (`product_id`),
  KEY `idx_lw_store_product_variant_value` (`attribute_value`),
  KEY `fk_lw_store_product_variant_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=399 AVG_ROW_LENGTH=910 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_product_variant`
--

INSERT INTO `lw_store_product_variant` (`id`, `product_id`, `attribute_id`, `attribute_value`, `amount`, `type`, `sku`, `position`, `quantity`, `parent_id`) VALUES
(295, 19, 2, '0-4 мес', 0, 0, '', 1, NULL, NULL),
(296, 20, 2, '0-6 мес', 0, 0, '', 2, NULL, NULL),
(297, 20, 3, 'деми', 0, 0, '', 3, NULL, NULL),
(298, 20, 3, 'зима', 200, 0, '', 4, NULL, NULL),
(299, 20, 4, 'Вязаный помпон', 80, 0, '', 5, NULL, NULL),
(300, 20, 4, 'Меховой помпон', 390, 0, '', 6, NULL, NULL),
(301, 20, 4, 'Без отделки', 0, 0, '', 7, NULL, NULL),
(302, 21, 2, '0-4 мес', 0, 0, '', 8, NULL, NULL),
(303, 22, 3, 'деми', 0, 0, '', 9, NULL, NULL),
(304, 22, 3, 'зима', 200, 0, '', 10, NULL, NULL),
(305, 22, 4, 'Без отделки', 0, 0, '', 11, NULL, NULL),
(306, 22, 4, 'Вязаный помпон', 80, 0, '', 12, NULL, NULL),
(307, 22, 4, 'Меховой помпон', 390, 0, '', 13, NULL, NULL),
(308, 23, 2, '0-6 мес', 0, 0, '', 14, NULL, NULL),
(309, 23, 3, 'деми', 0, 0, '', 15, NULL, NULL),
(310, 23, 3, 'зима', 200, 0, '', 16, NULL, NULL),
(311, 23, 4, 'Без отделки', 0, 0, '', 17, NULL, NULL),
(312, 23, 4, 'Вязаный помпон', 80, 0, '', 18, NULL, NULL),
(313, 23, 4, 'Меховой помпон', 390, 0, '', 19, NULL, NULL),
(322, 26, 2, '44', 0, 0, '', 20, NULL, NULL),
(323, 26, 2, '44-46', 0, 0, '', 21, NULL, NULL),
(324, 26, 3, 'зима', 0, 0, '', 22, NULL, NULL),
(325, 27, 2, '46-48', 0, 0, '', 23, NULL, NULL),
(326, 27, 2, '50-52', 0, 0, '', 24, NULL, NULL),
(327, 27, 3, 'зима', 0, 0, '', 25, NULL, NULL),
(328, 28, 2, '40-42', 0, 0, '', 26, NULL, NULL),
(329, 28, 2, '42-44', 0, 0, '', 27, NULL, NULL),
(330, 28, 3, 'зима', 0, 0, '', 28, NULL, NULL),
(331, 29, 2, '46-48', 0, 0, '', 29, NULL, NULL),
(332, 29, 2, '50-52', 0, 0, '', 30, NULL, NULL),
(333, 29, 2, '52-54', 0, 0, '', 31, NULL, NULL),
(334, 29, 3, 'зима', 0, 0, '', 32, NULL, NULL),
(335, 30, 2, '68', 0, 0, '', 33, NULL, NULL),
(336, 30, 3, 'деми', 0, 0, '', 34, NULL, NULL),
(337, 30, 3, 'зима', 300, 0, '', 35, NULL, NULL),
(338, 30, 4, 'Без отделки', 0, 0, '', 36, NULL, NULL),
(339, 30, 4, 'Вязаный помпон', 80, 0, '', 37, NULL, NULL),
(340, 30, 4, 'Меховой помпон', 390, 0, '', 38, NULL, NULL),
(341, 30, 4, 'Ушки', 100, 0, '', 39, NULL, NULL),
(342, 31, 2, '86', 0, 0, '', 40, NULL, NULL),
(343, 31, 2, '92', 0, 0, '', 41, NULL, NULL),
(344, 31, 2, '98', 0, 0, '', 42, NULL, NULL),
(345, 31, 2, '104', 0, 0, '', 43, NULL, NULL),
(346, 31, 2, '110', 0, 0, '', 44, NULL, NULL),
(347, 31, 2, '116', 0, 0, '', 45, NULL, NULL),
(348, 31, 4, 'Без отделки', 0, 0, '', 46, NULL, NULL),
(349, 31, 4, 'Опушка', 1100, 0, '', 47, NULL, NULL),
(350, 31, 3, 'деми', 0, 0, '', 48, NULL, NULL),
(351, 31, 3, 'зима', 200, 0, '', 49, NULL, NULL),
(352, 32, 2, '86', 0, 0, '', 50, NULL, NULL),
(353, 32, 2, '92', 0, 0, '', 51, NULL, NULL),
(354, 32, 2, '98', 0, 0, '', 52, NULL, NULL),
(355, 32, 2, '104', 0, 0, '', 53, NULL, NULL),
(356, 32, 2, '110', 0, 0, '', 54, NULL, NULL),
(357, 32, 2, '116', 0, 0, '', 55, NULL, NULL),
(358, 32, 3, 'деми', 0, 0, '', 56, NULL, NULL),
(359, 32, 3, 'зима', 200, 0, '', 57, NULL, NULL),
(360, 32, 4, 'Без отделки', 0, 0, '', 58, NULL, NULL),
(361, 32, 4, 'Опушка', 1100, 0, '', 59, NULL, NULL),
(362, 33, 2, '86', 0, 0, '', 60, NULL, NULL),
(363, 33, 2, '92', 0, 0, '', 61, NULL, NULL),
(364, 33, 2, '98', 0, 0, '', 62, NULL, NULL),
(365, 33, 2, '104', 0, 0, '', 63, NULL, NULL),
(366, 33, 2, '110', 0, 0, '', 64, NULL, NULL),
(367, 33, 2, '116', 0, 0, '', 65, NULL, NULL),
(368, 33, 3, 'деми', 0, 0, '', 66, NULL, NULL),
(369, 33, 3, 'зима', 200, 0, '', 67, NULL, NULL),
(370, 33, 4, 'Без отделки', 0, 0, '', 68, NULL, NULL),
(371, 33, 4, 'Опушка', 1100, 0, '', 69, NULL, NULL),
(372, 34, 2, '86', 0, 0, '', 70, NULL, NULL),
(373, 34, 2, '92', 0, 0, '', 71, NULL, NULL),
(374, 34, 2, '98', 0, 0, '', 72, NULL, NULL),
(375, 34, 2, '104', 0, 0, '', 73, NULL, NULL),
(376, 34, 2, '110', 0, 0, '', 74, NULL, NULL),
(377, 34, 2, '116', 0, 0, '', 75, NULL, NULL),
(378, 34, 3, 'деми', 0, 0, '', 76, NULL, NULL),
(379, 34, 3, 'зима', 300, 0, '', 77, NULL, NULL),
(380, 34, 4, 'Без отделки', 0, 0, '', 78, NULL, NULL),
(381, 34, 4, 'Опушка', 1100, 0, '', 79, NULL, NULL),
(382, 35, 2, '62', 0, 0, '', 80, NULL, NULL),
(383, 35, 2, '68', 0, 0, '', 81, NULL, NULL),
(384, 35, 2, '74', 0, 0, '', 82, NULL, NULL),
(385, 36, 2, '56', 0, 0, '', 83, NULL, NULL),
(386, 36, 2, '68', 0, 0, '', 84, NULL, NULL),
(387, 36, 2, '74', 0, 0, '', 85, NULL, NULL),
(388, 37, 2, '62', 0, 0, '', 86, NULL, NULL),
(389, 37, 2, '68', 0, 0, '', 87, NULL, NULL),
(390, 37, 2, '74', 0, 0, '', 88, NULL, NULL),
(391, 38, 2, '86-104', 0, 0, '', 89, NULL, NULL),
(392, 38, 2, '110-116', 0, 0, '', 90, NULL, NULL),
(393, 39, 2, '86-104', 0, 0, '', 91, NULL, NULL),
(394, 39, 2, '110-116', 0, 0, '', 92, NULL, NULL),
(395, 40, 2, '86-104', 0, 0, '', 93, NULL, NULL),
(396, 40, 2, '110-116', 0, 0, '', 94, NULL, NULL),
(397, 41, 2, '86-104', 0, 0, '', 95, NULL, NULL),
(398, 41, 2, '110-116', 0, 0, '', 96, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_type`
--

CREATE TABLE IF NOT EXISTS `lw_store_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_store_type_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_type`
--

INSERT INTO `lw_store_type` (`id`, `name`) VALUES
(1, 'Общий список фильтров');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_store_type_attribute`
--

CREATE TABLE IF NOT EXISTS `lw_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`attribute_id`),
  KEY `fk_lw_store_type_attribute_attribute` (`attribute_id`)
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_store_type_attribute`
--

INSERT INTO `lw_store_type_attribute` (`type_id`, `attribute_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_user_tokens`
--

CREATE TABLE IF NOT EXISTS `lw_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_lw_user_tokens_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_user_tokens`
--

INSERT INTO `lw_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(37, 1, 'S6sB_qPOdJ_QyYDfZZG3LRm751hfEad9', 4, 0, '2019-10-20 23:53:14', '2019-10-20 23:53:14', '145.255.22.58', '2019-10-27 23:53:14');

-- --------------------------------------------------------

--
-- Структура таблицы `lw_user_user`
--

CREATE TABLE IF NOT EXISTS `lw_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '896514f6d62077cff87b3e1f2ed6cc3b0.86117200 1568627951',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_user_user_email` (`email`),
  UNIQUE KEY `ux_lw_user_user_nick_name` (`nick_name`),
  KEY `ix_lw_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_user_user`
--

INSERT INTO `lw_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`) VALUES
(1, '2019-09-16 15:04:34', '', '', '', 'admin', 'doctordrethe@mail.ru', 0, NULL, '', '', '', 1, 1, '2019-10-20 23:53:14', '2019-09-16 15:04:34', NULL, '$2y$13$6zEhx3/9D7YxPUUeJbLDGOa8lJYDRqjZ3/X/gRVnYp.2Na2Hmz/D2', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_user_user_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `lw_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_lw_user_user_auth_assignment_user` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_user_user_auth_assignment`
--

INSERT INTO `lw_user_user_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_user_user_auth_item`
--

CREATE TABLE IF NOT EXISTS `lw_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_lw_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_user_user_auth_item`
--

INSERT INTO `lw_user_user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `lw_user_user_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `lw_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_lw_user_user_auth_item_child_child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_yml_export`
--

CREATE TABLE IF NOT EXISTS `lw_yml_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `lw_yupe_settings`
--

CREATE TABLE IF NOT EXISTS `lw_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_lw_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_lw_yupe_settings_module_id` (`module_id`),
  KEY `ix_lw_yupe_settings_param_name` (`param_name`),
  KEY `fk_lw_yupe_settings_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lw_yupe_settings`
--

INSERT INTO `lw_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', 'LITTLE TWIST – это российская марка детской одежды от 0 до 8 лет. Мы создаем и производим одежду, в которой сбалансированы качество, цена и стиль.', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(2, 'yupe', 'siteName', 'Little Twist', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(3, 'yupe', 'siteKeyWords', 'детская одежда', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(4, 'yupe', 'email', 'doctordrethe@mail.ru', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(5, 'yupe', 'theme', 'little-twist', '2019-09-16 15:09:25', '2019-09-18 15:15:01', 1, 1),
(6, 'yupe', 'backendTheme', '', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2019-09-16 15:09:25', '2019-09-16 15:09:25', 1, 1),
(9, 'homepage', 'mode', '2', '2019-09-20 16:22:27', '2019-09-20 16:22:27', 1, 1),
(10, 'homepage', 'target', '7', '2019-09-20 16:22:27', '2019-10-07 06:12:43', 1, 1),
(11, 'homepage', 'limit', '', '2019-09-20 16:22:27', '2019-09-20 16:22:27', 1, 1),
(12, 'product', 'pageSize', '100', '2019-10-16 07:30:42', '2019-10-16 07:30:42', 1, 2);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `lw_blog_blog`
--
ALTER TABLE `lw_blog_blog`
  ADD CONSTRAINT `fk_lw_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `lw_user_user` (`id`),
  ADD CONSTRAINT `fk_lw_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `lw_user_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `lw_blog_post`
--
ALTER TABLE `lw_blog_post`
  ADD CONSTRAINT `fk_lw_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `lw_blog_blog` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `lw_user_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `lw_blog_post_to_tag`
--
ALTER TABLE `lw_blog_post_to_tag`
  ADD CONSTRAINT `fk_lw_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `lw_blog_post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `lw_blog_tag` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_blog_user_to_blog`
--
ALTER TABLE `lw_blog_user_to_blog`
  ADD CONSTRAINT `fk_lw_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `lw_blog_blog` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_category_category`
--
ALTER TABLE `lw_category_category`
  ADD CONSTRAINT `fk_lw_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `lw_comment_comment`
--
ALTER TABLE `lw_comment_comment`
  ADD CONSTRAINT `fk_lw_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `lw_comment_comment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_feedback_feedback`
--
ALTER TABLE `lw_feedback_feedback`
  ADD CONSTRAINT `fk_lw_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `lw_gallery_gallery`
--
ALTER TABLE `lw_gallery_gallery`
  ADD CONSTRAINT `fk_lw_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `lw_image_image` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `lw_gallery_image_to_gallery`
--
ALTER TABLE `lw_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_lw_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `lw_gallery_gallery` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `lw_image_image` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_image_image`
--
ALTER TABLE `lw_image_image`
  ADD CONSTRAINT `fk_lw_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `lw_image_image` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `lw_mail_mail_template`
--
ALTER TABLE `lw_mail_mail_template`
  ADD CONSTRAINT `fk_lw_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `lw_mail_mail_event` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_menu_menu_item`
--
ALTER TABLE `lw_menu_menu_item`
  ADD CONSTRAINT `fk_lw_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `lw_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_news_news`
--
ALTER TABLE `lw_news_news`
  ADD CONSTRAINT `fk_lw_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `lw_notify_settings`
--
ALTER TABLE `lw_notify_settings`
  ADD CONSTRAINT `fk_lw_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_page_page`
--
ALTER TABLE `lw_page_page`
  ADD CONSTRAINT `fk_lw_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `lw_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_lw_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `lw_store_attribute`
--
ALTER TABLE `lw_store_attribute`
  ADD CONSTRAINT `fk_lw_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `lw_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_attribute_option`
--
ALTER TABLE `lw_store_attribute_option`
  ADD CONSTRAINT `fk_lw_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `lw_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_category`
--
ALTER TABLE `lw_store_category`
  ADD CONSTRAINT `fk_lw_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `lw_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_delivery_payment`
--
ALTER TABLE `lw_store_delivery_payment`
  ADD CONSTRAINT `fk_lw_store_delivery_payment_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `lw_store_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_delivery_payment_payment` FOREIGN KEY (`payment_id`) REFERENCES `lw_store_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_order`
--
ALTER TABLE `lw_store_order`
  ADD CONSTRAINT `fk_lw_store_order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `lw_store_delivery` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_order_manager` FOREIGN KEY (`manager_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_order_payment` FOREIGN KEY (`payment_method_id`) REFERENCES `lw_store_payment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_order_status` FOREIGN KEY (`status_id`) REFERENCES `lw_store_order_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_order_user` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_order_coupon`
--
ALTER TABLE `lw_store_order_coupon`
  ADD CONSTRAINT `fk_lw_store_order_coupon_coupon` FOREIGN KEY (`coupon_id`) REFERENCES `lw_store_coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lw_store_order_coupon_order` FOREIGN KEY (`order_id`) REFERENCES `lw_store_order` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_order_product`
--
ALTER TABLE `lw_store_order_product`
  ADD CONSTRAINT `fk_lw_store_order_product_order` FOREIGN KEY (`order_id`) REFERENCES `lw_store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_order_product_product` FOREIGN KEY (`product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_product`
--
ALTER TABLE `lw_store_product`
  ADD CONSTRAINT `fk_lw_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `lw_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `lw_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `lw_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_product_attribute_value`
--
ALTER TABLE `lw_store_product_attribute_value`
  ADD CONSTRAINT `lw_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `lw_store_attribute` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lw_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `lw_store_attribute_option` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lw_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_product_category`
--
ALTER TABLE `lw_store_product_category`
  ADD CONSTRAINT `fk_lw_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `lw_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_product_image`
--
ALTER TABLE `lw_store_product_image`
  ADD CONSTRAINT `fk_lw_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `lw_store_product_image_group` (`id`) ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_lw_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_product_link`
--
ALTER TABLE `lw_store_product_link`
  ADD CONSTRAINT `fk_lw_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `lw_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_product_variant`
--
ALTER TABLE `lw_store_product_variant`
  ADD CONSTRAINT `fk_lw_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `lw_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_variant_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `lw_store_product_variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `lw_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_store_type_attribute`
--
ALTER TABLE `lw_store_type_attribute`
  ADD CONSTRAINT `fk_lw_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `lw_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `lw_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_user_tokens`
--
ALTER TABLE `lw_user_tokens`
  ADD CONSTRAINT `fk_lw_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_user_user_auth_assignment`
--
ALTER TABLE `lw_user_user_auth_assignment`
  ADD CONSTRAINT `fk_lw_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `lw_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `lw_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_user_user_auth_item_child`
--
ALTER TABLE `lw_user_user_auth_item_child`
  ADD CONSTRAINT `fk_lw_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `lw_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lw_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `lw_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `lw_yupe_settings`
--
ALTER TABLE `lw_yupe_settings`
  ADD CONSTRAINT `fk_lw_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `lw_user_user` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
