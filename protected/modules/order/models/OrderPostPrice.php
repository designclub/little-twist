<?php

/**
 * This is the model class for table "{{order_post_price}}".
 *
 * The followings are the available columns in table '{{order_post_price}}':
 * @property integer $id
 * @property string $price
 * @property integer $index
 * @property integer $order_id
 * @property integer $min_deys
 * @property integer $max_deys
 */
class OrderPostPrice extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order_post_price}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['index, order_id, min_deys, max_deys', 'numerical', 'integerOnly'=>true],
            ['price', 'length', 'max'=>6],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, price, index, order_id, min_deys, max_deys', 'safe', 'on'=>'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'index' => 'Index',
            'order_id' => 'Order',
            'min_deys' => 'Min Deys',
            'max_deys' => 'Max Deys',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('price', $this->price, true);
        $criteria->compare('index', $this->index);
        $criteria->compare('order_id', $this->order_id);
        $criteria->compare('min_deys', $this->min_deys);
        $criteria->compare('max_deys', $this->max_deys);

        return new CActiveDataProvider($this, [
            'criteria'=>$criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrderPostPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
