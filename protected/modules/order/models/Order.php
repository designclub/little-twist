<?php
/**
 * @property integer $id
 * @property integer $delivery_id
 * @property double $delivery_price
 * @property integer $payment_method_id
 * @property integer $paid
 * @property string $payment_time
 * @property string $payment_details
 * @property double $total_price
 * @property double $discount
 * @property double $coupon_discount
 * @property integer $separate_delivery
 * @property integer $status_id
 * @property string $date
 * @property integer $user_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $ip
 * @property string $url
 * @property string $note
 * @property string $modified
 * @property string $zipcode
 * @property string $country
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $apartment
 * @property integer $manager_id
 * @property integer $region_id
 * @property integer $family
 * @property integer $lastname
 * @property integer $dispatch_number
 *
 * @property OrderProduct[] $products
 * @property Delivery $delivery
 * @property Payment $payment
 * @property User $user
 * @property OrderStatus $status
 *
 */
use yupe\widgets\YPurifier;
use RussianPost\ApiClient;
use RussianPost\Http\Client;

Yii::import('application.modules.order.OrderModule');
Yii::import('application.modules.order.events.OrderEvents');
Yii::import('application.modules.order.events.PayOrderEvent');
Yii::import('application.modules.order.events.OrderEvent');
Yii::import('application.modules.order.events.OrderChangeStatusEvent');
Yii::import('application.modules.order.components.PostClient');

/**
 * Class Order
 */
class Order extends yupe\models\YModel
{
    /**
     *
     */
    const PAID_STATUS_NOT_PAID = 0;
    /**
     *
     */
    const PAID_STATUS_PAID = 1;

    /**
     *
     */
    const SCENARIO_USER = 'front';
    /**
     *
     */
    const SCENARIO_SDEK = 'sdek';    
    /**
     *
     */
    const SCENARIO_ADDRESS = 'address';    
    /**
     *
     */
    const SCENARIO_DELIVERY_POINT = 'delivery-point';
    /**
     *
     */
    const SCENARIO_ADMIN = 'admin';
    
    const SENDER_CITY_ID = 261; // Оренбург
    
    const Sberbank = 1; //оплата Сбербанк

    /**
     * @var null
     */
    public $couponId = null;

    /**
     * @var OrderProduct[]
     */
    private $_orderProducts = [];

    /**
     * @var bool
     */
    private $hasProducts = false; // ставим в true, когда в сценарии front добавляем хотя бы один продукт

    /**
     * @var
     */
    protected $oldAttributes;

    /**
     * @var bool
     */
    private $productsChanged = false; // менялся ли список продуктов в заказе

    /**
     * @var null
     */
    private $_validCoupons = null;

    public $family;

    public $post_address;
    
    public $post_id;
    
    public $sdek_id;
    
    public $longitude;
    
    public $latitude;
    
    public $geo;
    
    public $address_delivery;
    

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_order}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, email, phone, zipcode, country, city, street, house, apartment, region_id, regionName, cityName, family, lastname, address_delivery', 'filter', 'filter' => 'trim'],
            ['name, email, phone, zipcode, country, city, street, house, apartment, comment, regionName, cityName, family, lastname, address_delivery', 'filter', 'filter' => [$obj = new YPurifier(), 'purify']],
            ['status_id, delivery_id', 'required'],
            ['name, email, family, phone, city, delivery_id', 'required', 'on' => [ self::SCENARIO_USER, self::SCENARIO_ADDRESS, self::SCENARIO_DELIVERY_POINT ]],
            ['region_id, city, street, house', 'required', 'on' => self::SCENARIO_SDEK],
            ['zipcode, street, house', 'required', 'on' => self::SCENARIO_ADDRESS],
            ['zipcode, pvz_id', 'required', 'on' => self::SCENARIO_DELIVERY_POINT],
            ['email', 'email'],
            ['post_id, sdek_id', 'numerical'],
            [
                'delivery_id, separate_delivery, payment_method_id, paid, user_id, couponId, manager_id, cityCode, region_id, delivery_id, dispatch_number',
                'numerical',
                'integerOnly' => true,
            ],
            ['delivery_price, total_price, discount, coupon_discount', 'store\components\validators\NumberValidator'],
            ['name, phone, email, city, street, cityName, regionName, pvz_address', 'length', 'max' => 255],
            ['comment, note', 'length', 'max' => 1024],
            ['zipcode', 'length', 'max' => 30],
            ['house', 'length', 'max' => 50],
            ['country, regionCodeEx, regionCode', 'length', 'max' => 150],
            ['apartment', 'length', 'max' => 10],
            ['url', 'unique'],
            ['regionCode, regionCodeEx, regionName, cityCode, cityName, pvz_address', 'safe'],
            [
                'user_id, paid, payment_time, payment_details, total_price, discount, coupon_discount, separate_delivery, status_id, date, ip, url, modified',
                'unsafe',
                'on' => self::SCENARIO_USER,
            ],
            ['pvz_id, tariff_id, longitude, latitude, geo, zipcode, address_delivery, city, delivery_id, dispatch_number', 'safe'],
            [
                'id, delivery_id, delivery_price, payment_method_id, paid, payment_time, payment_details, total_price, discount, coupon_discount, separate_delivery, status_id, date, user_id, name, phone, email, comment, ip, url, note, modified, manager_id, family, lastname',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'products' => [self::HAS_MANY, 'OrderProduct', 'order_id', 'order' => 'products.id ASC'],
            'delivery' => [self::BELONGS_TO, 'Delivery', 'delivery_id'],
            'payment' => [self::BELONGS_TO, 'Payment', 'payment_method_id'],
            'status' => [self::BELONGS_TO, 'OrderStatus', 'status_id'],
            'client' => [self::BELONGS_TO, 'Client', 'user_id'],
            'manager' => [self::BELONGS_TO, 'User', 'manager_id'],
            'couponsIds' => [self::HAS_MANY, 'OrderCoupon', 'order_id'],
            'coupons' => [self::HAS_MANY, 'Coupon', 'coupon_id', 'through' => 'couponsIds'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'new' => [
                'condition' => 't.status_id = :status_id',
                'params' => [':status_id' => OrderStatus::STATUS_NEW],
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
                'createAttribute' => 'date',
                'updateAttribute' => 'modified',
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('OrderModule.order', '#'),
            'delivery_id' => Yii::t('OrderModule.order', 'Delivery method'),
            'delivery_price' => Yii::t('OrderModule.order', 'Delivery price'),
            'payment_method_id' => Yii::t('OrderModule.order', 'Payment method'),
            'paid' => Yii::t('OrderModule.order', 'Paid'),
            'payment_time' => Yii::t('OrderModule.order', 'Paid date'),
            'payment_details' => Yii::t('OrderModule.order', 'Payment details'),
            'total_price' => Yii::t('OrderModule.order', 'Total price'),
            'discount' => Yii::t('OrderModule.order', 'Discount (%)'),
            'coupon_discount' => Yii::t('OrderModule.order', 'Discount coupon'),
            'separate_delivery' => Yii::t('OrderModule.order', 'Separate delivery payment'),
            'status_id' => Yii::t('OrderModule.order', 'Status'),
            'date' => Yii::t('OrderModule.order', 'Created'),
            'user_id' => Yii::t('OrderModule.order', 'Client'),
            'name' => Yii::t('OrderModule.order', 'Имя'),
            'family' => Yii::t('OrderModule.order', 'Фамилия'),
            'lastname' => Yii::t('OrderModule.order', 'Ваше отчество'),
            'phone' => Yii::t('OrderModule.order', 'Phone'),
            'email' => Yii::t('OrderModule.order', 'Email'),
            'comment' => Yii::t('OrderModule.order', 'Comment'),
            'ip' => Yii::t('OrderModule.order', 'IP'),
            'url' => Yii::t('OrderModule.order', 'Url'),
            'note' => Yii::t('OrderModule.order', 'Note'),
            'modified' => Yii::t('OrderModule.order', 'Update date'),
            'zipcode' => Yii::t('OrderModule.order', 'Zipcode'),
            'region_id' => Yii::t('OrderModule.order', 'Регион'),
            'regionName' => Yii::t('OrderModule.order', 'Регион'),
            'country' => Yii::t('OrderModule.order', 'Country'),
            //'city' => Yii::t('OrderModule.order', 'City'),
            'city' => Yii::t('OrderModule.order', 'Settlement'),
            'street' => Yii::t('OrderModule.order', 'Street'),
            'house' => Yii::t('OrderModule.order', 'House'),
            'apartment' => Yii::t('OrderModule.order', 'Apartment'),
            'manager_id' => Yii::t('OrderModule.order', 'Manager'),
            'post_address' => 'Адрес доставки',
            'address_delivery' => 'Адрес доставки',
            'pvz_id' => 'Код пункта самовывоза СДЭК',
            'pvz_address' => 'Пункт самовывоза СДЭК',
            'tariff_id' => 'Тариф доставки СДЭК',
        ];
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->with = ['delivery', 'payment', 'client', 'status'];

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('delivery_id', $this->delivery_id);
        $criteria->compare('delivery_price', $this->delivery_price);
        $criteria->compare('payment_method_id', $this->payment_method_id);
        $criteria->compare('paid', $this->paid);
        $criteria->compare('payment_time', $this->payment_time);
        $criteria->compare('payment_details', $this->payment_details, true);
        $criteria->compare('total_price', $this->total_price);
        $criteria->compare('total_price', $this->total_price);
        $criteria->compare('discount', $this->discount);
        $criteria->compare('coupon_discount', $this->coupon_discount);
        $criteria->compare('separate_delivery', $this->separate_delivery);
        $criteria->compare('t.status_id', $this->status_id);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('modified', $this->modified, true);
        $criteria->compare('t.manager_id', $this->manager_id);

        if (null !== $this->couponId) {
            $criteria->with['couponsIds'] = ['together' => true];
            $criteria->addCondition('couponsIds.coupon_id = :id');
            $criteria->params = CMap::mergeArray($criteria->params, [':id' => $this->couponId]);
        }

        if (null !== $this->name) {
            $clientCriteria = new CDbCriteria();
            $clientCriteria->with['client'] = ['together' => true];
            $clientCriteria->addSearchCondition('client.last_name', $this->name, true, 'OR');
            $clientCriteria->addSearchCondition('client.first_name', $this->name, true, 'OR');
            $clientCriteria->addSearchCondition('client.middle_name', $this->name, true, 'OR');
            $clientCriteria->addSearchCondition('client.nick_name', $this->name, true, 'OR');
            $criteria->mergeWith($clientCriteria, 'OR');
        }

        return new CActiveDataProvider(
            __CLASS__,
            [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => $this->getTableAlias().'.id DESC'],
            ]
        );
    }

    /**
     * @return CActiveDataProvider
     */
    public function searchCoupons()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.order_id', $this->id);

        $criteria->with = ['coupon'];

        return new CActiveDataProvider(
            OrderCoupon::_CLASS_(),
            [
                'criteria' => $criteria,
            ]
        );
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->oldAttributes = $this->getAttributes();
        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->getScenario() === self::SCENARIO_USER || $this->getScenario() === self::SCENARIO_ADDRESS || $this->getScenario() === self::SCENARIO_DELIVERY_POINT){
            if (!$this->hasProducts) {
                $this->addError('products', Yii::t('OrderModule.order', 'There are no selected products'));
            }
        }

        return parent::beforeValidate();
    }

    /**
     * @return float|int
     */
    public function getProductsCost()
    {
        $cost = 0;
        $products = $this->productsChanged ? $this->_orderProducts : $this->products;

        foreach ($products as $op) {
            $cost += $op->price * $op->quantity;
        }

        return $cost;
    }

    /**
     * @return bool
     */
    public function isCouponsAvailable()
    {
        return Yii::app()->hasModule('coupon');
    }

    /**
     * @return bool
     */
    public function hasCoupons()
    {
        return !empty($this->couponsIds);
    }

    /**
     * @return array
     */
    public function getCouponsCodes()
    {
        $codes = [];

        foreach ($this->coupons as $coupon) {
            $codes[] = $coupon->code;
        }

        return $codes;
    }

    /**
     * @return mixed
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * @return int|mixed
     */
    public function getDeliveryCost()
    {
        $cost = $this->delivery_price;
        if ($this->isCouponsAvailable()) {
            $validCoupons = $this->getValidCoupons($this->getCouponsCodes());
            foreach ($validCoupons as $coupon) {
                if ($coupon->free_shipping) {
                    $cost = 0;
                }
            }
        }

        return $cost;
    }

    /**
     * Фильтрует переданные коды купонов и возвращает объекты купонов
     * @param $codes - массив кодов купонов
     * @return Coupon[] - массив объектов-купонов
     */
    public function getValidCoupons($codes)
    {
        if ($this->_validCoupons !== null) {
            return $this->_validCoupons;
        }

        $productsTotalPrice = $this->getProductsCost();

        $validCoupons = [];

        /* @var $coupon Coupon */
        /* проверим купоны на валидность */
        foreach ($codes as $code) {
            $coupon = Coupon::model()->getCouponByCode($code);

            if (null !== $coupon && $coupon->getIsAvailable($productsTotalPrice)) {
                $validCoupons[] = $coupon;
            }
        }

        return $validCoupons;
    }

    /**
     * Получает скидку для переданных купонов
     * @param $coupons Coupon[]
     * @return float - скидка
     */
    public function getCouponDiscount(array $coupons)
    {
        $productsTotalPrice = $this->getProductsCost();
        $delta = 0.00;
        if ($this->isCouponsAvailable()) {
            foreach ($coupons as $coupon) {
                switch ($coupon->type) {
                    case CouponType::TYPE_SUM:
                        $delta += $coupon->value;
                        break;
                    case CouponType::TYPE_PERCENT:
                        $delta += ($coupon->value / 100) * $productsTotalPrice;
                        break;
                }
            }
        }

        return $delta;
    }

    /**
     * @param array $attributes
     * @param array $products
     * @param int $status
     * @param int $client
     *
     * @return bool
     */
    public function store(array $attributes, array $products, $client = null, $status = OrderStatus::STATUS_NEW)
    {
        $isNew = $this->getIsNewRecord();
        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $this->status_id = (int)$status;
            $this->user_id = $client;
            $this->setAttributes($attributes);
            $this->setProducts($products);
            
            if (!$this->validate()) {
                return false;
            }
            
            $this->delivery_price = $this->getPriceTariff($attributes, $products);

            if (!$this->save()) {
                return false;
            }

            Yii::app()->eventManager->fire(
                $isNew ? OrderEvents::CREATED : OrderEvents::UPDATED,
                new OrderEvent($this)
            );

            $transaction->commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollback();

            return false;
        }
    }

    /**
     * @param array $coupons
     * @return bool
     */
    public function applyCoupons(array $coupons)
    {
        if (!$this->isCouponsAvailable()) {
            return true;
        }

        $coupons = $this->getValidCoupons($coupons);

        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            foreach ($coupons as $coupon) {
                $model = new OrderCoupon();

                $model->setAttributes(
                    [
                        'order_id' => $this->id,
                        'coupon_id' => $coupon->id,
                        'create_time' => new CDbExpression('NOW()'),
                    ]
                );

                $model->save();

                $coupon->decreaseQuantity();
            }

            $this->coupon_discount = $this->getCouponDiscount($coupons);

            $this->delivery_price = $this->getDeliveryCost();

            $this->update(['coupon_discount', 'delivery_price']);

            $transaction->commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollback();

            return false;
        }
    }

    /**
     * @return bool
     * @TODO вынести всю логику в saveData
     */
    public function beforeSave()
    {
        $this->total_price = $this->getProductsCost();

        if ($this->getIsNewRecord()) {
            $this->url = md5(uniqid(time(), true));
            $this->ip = Yii::app()->getRequest()->userHostAddress;
            
            if ($this->getScenario() === self::SCENARIO_USER || $this->getScenario() === self::SCENARIO_ADDRESS || $this->getScenario() === self::SCENARIO_DELIVERY_POINT){
                $this->delivery_id = (int)$this->delivery_id;

                // Если оплата через почту
               /* if ($this->delivery_id === Delivery::TYPE_POST_RUSSIAN  || $this->delivery_id === Delivery::TYPE_EMS) {
                    $post = OrderPostPrice::model()->findByPk($this->post_id);
                    if ($post) {
                        $this->delivery_price = $post->price;
                    }
                    $post->order_id = $this->id;
                    $post->save();
                }
                //если доставка СДЭК - ПВЗ или СДЭК - курьер
                elseif($this->delivery_id === Delivery::TYPE_SDEK_SELF || $this->delivery_id === Delivery::TYPE_SDEK_COURIER){

                    $sdek = OrderSdekData::model()->findByPk($this->sdek_id);
                    if ($sdek)
                        $this->delivery_price = $sdek->price;
                    $sdek->order_id = $this->id;
                    $sdek->save();
                }
                else {
                    $this->delivery_price = $this->delivery ? $this->delivery->getCost($this->total_price) : 0;
                }*/

                $this->separate_delivery = $this->delivery ? $this->delivery->separate_payment : null;
            }
        }

        $this->delivery_price = $this->getDeliveryCost();

        // Контроль остатков товара на складе
        // @TODO реализовать перерасчет остатков товаров при изменении списка заказанных товаров администратором
        if ($this->getIsNewRecord() && Yii::app()->getModule('store')->controlStockBalances) {
            foreach ($this->_orderProducts as $orderProduct) {
                $product = $orderProduct->product;
                if ($orderProduct->quantity > $product->getAvailableQuantity()) {
                    $this->addError(
                        'products',
                        Yii::t(
                            "OrderModule.order",
                            'Not enough product «{product_name}» in stock, maximum - {n} items',
                            [
                                '{product_name}' => $product->getName(),
                                '{n}' => $product->getAvailableQuantity(),
                            ]
                        )
                    );
                    return false;
                }

                // Изменение значения количества товара
                $newQuantity = $product->quantity - $orderProduct->quantity;
                $product->saveAttributes(['quantity' => $newQuantity]);
            }
        }

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        foreach ($this->products as $product) {
            $product->delete();
        }
        parent::afterDelete();
    }

    /**
     * @return array
     */
    public function getPaidStatusList()
    {
        return [
            self::PAID_STATUS_PAID => Yii::t("OrderModule.order", 'Paid'),
            self::PAID_STATUS_NOT_PAID => Yii::t("OrderModule.order", 'Not paid'),
        ];
    }

    /**
     * @return string
     */
    public function getPaidStatus()
    {
        $data = $this->getPaidStatusList();

        return isset($data[$this->paid]) ? $data[$this->paid] : Yii::t("OrderModule.order", '*unknown*');
    }

    /**
     *
     * Формат массива:
     * <pre>
     * array(
     *    '45' => array( //реальный id или сгенерированный новый, у новых внутри массива нет id
     *        'id' => '10', //если нет id, то новый
     *        'variant_ids' => array('10', '20, '30'), // массив с id вариантов
     *        'quantity' = > '5',
     *        'price' => '1000',
     *        'product_id' => '123',
     *    )
     * )
     * </pre>
     * @param $orderProducts Array
     */
    public function setProducts($orderProducts)
    {
        $this->productsChanged = true;
        $orderProductsObjectsArray = [];
        if (is_array($orderProducts)) {
            foreach ($orderProducts as $key => $op) {
                $product = null;
                if (isset($op['product_id'])) {
                    $product = Product::model()->findByPk($op['product_id']);
                }
                $variantIds = isset($op['variant_ids']) ? $op['variant_ids'] : [];
                if ($product) {
                    $this->hasProducts = true;
                }

                /* @var $orderProduct OrderProduct */
                $orderProduct = null;
                if (isset($op['id'])) {
                    $orderProduct = OrderProduct::model()->findByPk($op['id']);
                }

                if (!$orderProduct) {
                    $orderProduct = new OrderProduct();
                    $orderProduct->product_id = $product->id;
                    $orderProduct->product_name = $product->name;
                    $orderProduct->sku = $product->sku;
                }
 
                if ($this->getScenario() == self::SCENARIO_USER || $this->getScenario() == self::SCENARIO_ADDRESS || $this->getScenario() == self::SCENARIO_DELIVERY_POINT) {
                    $orderProduct->price = $product->getPrice($variantIds);
                } else {
                    $orderProduct->price = $op['price'];
                }

                $orderProduct->variantIds = $variantIds;
                $orderProduct->quantity = $op['quantity'];

                $orderProductsObjectsArray[] = $orderProduct;
            }
            $this->_orderProducts = $orderProductsObjectsArray;
        }
    }

    /**
     * Массив объектов OrderProduct
     * @param $products
     */
    private function updateOrderProducts($products)
    {
        if (!$this->productsChanged) {
            return;
        }

        $validOrderProductIds = [];

        foreach ($products as $var) {
            /* @var $var OrderProduct */
            if ($var->getIsNewRecord()) {
                $var->order_id = $this->id;
            }

            if ($var->save()) {
                $validOrderProductIds[] = $var->id;
            }
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('order_id = :order_id');
        $criteria->params = [':order_id' => $this->id];
        $criteria->addNotInCondition('id', $validOrderProductIds);
        OrderProduct::model()->deleteAll($criteria);
    }

    /**
     *
     */
    public function afterSave()
    {
        $this->updateOrderProducts($this->_orderProducts);
        parent::afterSave();
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return (float)$this->total_price - (float)$this->discount - (float)$this->coupon_discount;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice()
    {
        return (float)$this->delivery_price;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithDelivery()
    {
        $price = $this->getTotalPrice();

        if (!$this->separate_delivery) {
            $price += $this->getDeliveryPrice();
        }

        return $price;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return (int)$this->paid === static::PAID_STATUS_PAID;
    }

    /**
     * @return mixed
     */
    public function isPaymentMethodSelected()
    {
        return $this->payment_method_id;
    }


    /**
     * @param Payment $payment
     * @param int $paid
     * @return bool
     */
    public function pay(Payment $payment, $paid = self::PAID_STATUS_PAID)
    {
        if ($this->isPaid()) {
            return true;
        }

        $this->setAttributes([
            'paid' => (int)$paid,
            'payment_method_id' => $payment->id,
            'payment_time' => new CDbExpression('NOW()'),
        ]);

        $result = $this->save();

        if ($result) {
            Yii::app()->eventManager->fire(OrderEvents::SUCCESS_PAID, new PayOrderEvent($this, $payment));
        } else {
            Yii::app()->eventManager->fire(OrderEvents::FAILURE_PAID, new PayOrderEvent($this, $payment));
        }

        return $result;
    }

    /**
     * @param $url
     * @return static
     */
    public function findByUrl($url)
    {
        return $this->findByAttributes(['url' => $url]);
    }

    /**
     * @return bool
     * @TODO реализовать перерасчет остатков товаров при отмене заказа
     */
    public function isStatusChanged()
    {
        if ($this->oldAttributes['status_id'] != $this->status_id) {
            Yii::app()->eventManager->fire(OrderEvents::STATUS_CHANGED, new OrderChangeStatusEvent($this));

            return true;
        }

        return false;
    }

    /**
     * @param $number
     * @return static
     */
    public function findByNumber($number)
    {
        return $this->findByPk($number);
    }

    /**
     * @param string $separator
     * @return string
     */
    public function getAddress($separator = ', ')
    {
        
        if($this->delivery_id == Delivery::TYPE_SDEK_SELF){
            return implode($separator, [
                $this->zipcode,
                $this->city,
                $this->pvz_address,
            ]);
        }
        
        if($this->delivery_id == Delivery::TYPE_SELF){
            return 'Самовывоз';
        }
        
        if($this->delivery_id != Delivery::TYPE_SDEK_SELF || $this->delivery_id != Delivery::TYPE_SELF){
            return implode($separator, [
                $this->zipcode,
                //$this->country,
                $this->city,
                $this->street,
                $this->house,
                $this->apartment,
            ]);
        }
    }

    /**
     * @return CActiveDataProvider
     */
    public function getProducts()
    {
        return new CActiveDataProvider(
            'OrderProduct',
            [
                'criteria' => [
                    'condition' => 'order_id = :id',
                    'params' => [
                        ':id' => $this->id,
                    ],
                ],
            ]
        );
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        return isset($this->status) ? $this->status->name : Yii::t('OrderModule.order', '*unknown*');
    }

    /**
     * @param IWebUser $user
     * @return bool
     */
    public function checkManager(IWebUser $user)
    {
        if (!$this->manager_id) {
            return true;
        }

        if (((int)$this->manager_id === (int)$user->getId()) || $user->isSuperUser()) {
            return true;
        }

        return false;
    }
    
    public function checkIsDispatchNumber(){
        if (!empty($this->dispatch_number)) {
            return true;
        }
        return false;
    }

    public function getCDEKtariffList($tariffType){
        
        $listTariff =  [
            'pvz' => [
                5 => 'Экономичный экспресс склад-склад',
                10 => 'Экспресс лайт склад-склад',
                62 => 'Магистральный экспресс склад-склад',
                136 => 'Посылка склад-склад',
                234 => 'Экономичная посылка склад-склад',
                //12 => 'Экспресс лайт дверь-склад',
                //138 => 'Посылка дверь-склад',
            ],
            
            'courier' => [
                11 => 'Экспресс лайт склад-дверь',
                137 => 'Посылка склад-дверь',
                233 => 'Экономичная посылка склад-дверь',
                //1 => 'Экспресс лайт дверь-дверь',
                //139 => 'Посылка дверь-дверь',
            ],
            
            'all' => [
                5 => 'Экономичный экспресс склад-склад',
                10 => 'Экспресс лайт склад-склад',
                62 => 'Магистральный экспресс склад-склад',
                136 => 'Посылка склад-склад',
                234 => 'Экономичная посылка склад-склад',
                11 => 'Экспресс лайт склад-дверь',
                137 => 'Посылка склад-дверь',
                233 => 'Экономичная посылка склад-дверь',
            ],
            
            'groupTariff' => [
                'pvz' => [
                    5 => 'Экономичный экспресс склад-склад',
                    10 => 'Экспресс лайт склад-склад',
                    62 => 'Магистральный экспресс склад-склад',
                    136 => 'Посылка склад-склад',
                    234 => 'Экономичная посылка склад-склад',
                ],
                'courier' => [
                    11 => 'Экспресс лайт склад-дверь',
                    137 => 'Посылка склад-дверь',
                    233 => 'Экономичная посылка склад-дверь',
                ]
            ],
            
        ];
 
        return $listTariff[$tariffType];
    }

    public function getCDEKtariffName($tariffType, $id){
        $list = $this->getCDEKtariffList($tariffType);
        return $list[$id] ?? null;
    }

    public function getProductsInfo(){
        $positions = Yii::app()->cart->getPositions();

        $length = 0;
        $width = 0;
        $height = 0;
        $weight = 0;

        $i = 0;
        foreach ($positions as $key => $position) {
            $quantity = $position->getQuantity();
            if ($i===0) {
                $length += $position->length > 0 ? ($position->length * 100) * $quantity : 20 * $quantity;
                $width += $position->width > 0 ? ($position->width * 100) * $quantity : 20 * $quantity;
            }
            $height += $position->height > 0 ? ($position->height * 100) * $quantity : 20 * $quantity;
            $weight += $position->weight > 0 ? ($position->weight * 1000) * $quantity : 1000 * $quantity;

            $i++;
        }

        return [
            'length' => $length,
            'width' => $width,
            'height' => $height,
            'weight' => $weight,
        ];
    }
    
    public function dimensionsParcel(){
        $positions = Yii::app()->cart->getPositions();
        $dimensions = [];
        $dimensions['height'] = 0;
        $dimensions['weight'] = 0;
        
        $i = 0;
        foreach ($positions as $key => $position) {
            $quantity = $position->getQuantity();
            $dimensions['width'][$i] = !empty($position->width) ? $position->width * 100 : 0;                
            $dimensions['length'][$i] = !empty($position->length) ? $position->length * 100 : 0;
            $dimensions['height'] += !empty($position->height) ? $position->height * 100 * $quantity : 0;
            $dimensions['weight'] += !empty($position->weight) ? $position->weight * 1000 * $quantity : 1;
            $i++;
        }
        
        return $dimensions;
    }
    
    /**
    * new block calc tariffs
    *
    * 
    */
    public function goodsPacking(){
        
        $goods = []; $weightmax = 0; $i = 0;
        
        $positions = Yii::app()->cart->getPositions();

        if(!empty($positions)){
            foreach ($positions as $key => $position){
                $quantity = $position->getQuantity();
                
                for ($j = 1; $j <= $quantity; $j++){
                    $goods[$i]['width'] = !empty($position->width) ? $position->width * 100 : 0;
                    $goods[$i]['height'] = !empty($position->height) ? $position->height * 100 : 0;
                    $goods[$i]['length'] = !empty($position->length) ? $position->length * 100 : 0;
                    $goods[$i]['weight'] = !empty($position->weight) ? $position->weight : 0;
                    $weightmax += $position->weight;
                    ++$i;
                }
            }
        }

        return $goods;
    }
    
    /**
    * calc min tariff for SDEK
    *
    *
    */
    public function getTariffSdekMin($typeSdek, $order, $products){
        
        $listTariffs = [];
        
        switch($typeSdek){
            case Delivery::TYPE_SDEK_SELF:
                $tariffType = 'pvz';
            break;
            case Delivery::TYPE_SDEK_COURIER:
                $tariffType = 'courier';
            break;
        }

        $tariffList = Yii::app()->getComponent('cdekRepository')->getListTariffs($tariffType);
        
        $goods = $this->goodsPacking();
 
        $listTariffsSdek = Yii::app()->cdek->geCalcTariffs(Order::SENDER_CITY_ID, $order['zipcode'], $tariffList, $goods);
   
        if (isset($listTariffsSdek['result'])) {
            foreach ($listTariffsSdek['result'] as $key => $item) {
                if (!isset($item['result']['errors'])) {
                    $listTariffs[$key] =  $item['result']['priceByCurrency'];
                }
            }
        }
 
        if(!empty($listTariffs))
            return min($listTariffs);
        else
            return 0;
    }
    
    public function getTariffPostMin($typePost, $order){
                
        $listTariffs = [];
        
        $goods = $this->dimensionsParcel();
       
        $client = new ApiClient(
            Yii::app()->params['postRF']['token'],
            Yii::app()->params['postRF']['login'],
            Yii::app()->params['postRF']['password']
        );
 
        $query = [
            "dimension" => [
                "height" => $goods['height'],
                "length" => max($goods['length']),
                "width" => max($goods['width']),
            ],
            "index-from" => '460000',
            "index-to" => (string)$order['zipcode'],
            "delivery-point-index" => (string)$order['zipcode'],
            "mass" => $goods['weight'],
            "mail-category" => "ORDINARY",
            "mail-type" => $typePost == Delivery::TYPE_POST_RUSSIAN ? "POSTAL_PARCEL" : "EMS",
            "completenessChecking" => 'false',
            "vsd" => 'false',
            "courier" => 'false',
            "dimension-type" => 'L', 
            "entries-type" => 'SALE_OF_GOODS', 
            "fragile" => "false",
        ];

        $responce = $client->getTariff($query);
 
        if(!$responce)
            return 0;
        
        if ($responce->isSuccessful()) {
            $listTariffs = $responce->result;
            $listTariffs['index'] = (string)$order['zipcode'];
        }

        if (count($listTariffs) > 0) {
            $index   = $listTariffs['index'] ?? '';
            $minDays   = $listTariffs['delivery-time']['min-days'] ?? '';
            $maxDays   = $listTariffs['delivery-time']['max-days'] ?? '';
            $totalRateRub = $listTariffs['total-rate'] > 0 ? $listTariffs['total-rate'] / 100 : 0; // в рублях
            $totalVatRub  = $listTariffs['total-vat'] > 0 ? $listTariffs['total-vat'] / 100 : 0; // в рублях НДС
            $cost = $totalRateRub + $totalVatRub;
            return $cost;
        }
        else{
            return 0;
        }
    }
    
    public function getTariffMin($order){
        
        $listTariffsResult = [];

        $listTariffs = [];

        $tariffList = Yii::app()->getComponent('cdekRepository')->getListTariffs('all');

        $listTarriffsSdek =  $this->getCDEKtariffList('groupTariff');

        $goods = $this->goodsPacking();
        
        $listTariffsSdek = Yii::app()->cdek->geCalcTariffs(Order::SENDER_CITY_ID, $order['zipcode'], $tariffList, $goods);
                
        if (isset($listTariffsSdek['result'])) {
            foreach ($listTariffsSdek['result'] as $key => $item) {
                if (!isset($item['result']['errors'])) {
                    $listTariffs[$item['tariffId']] =  $item['result']['priceByCurrency'];
                }
            }
        }
        
        $listSdekTariffs = [];
        
        foreach($listTarriffsSdek as $key => $group){
            foreach($group as $id => $tariff){
                if(!empty($listTariffs[$id])){
                    $listSdekTariffs[$key][$id] = $listTariffs[$id];
                }
            }
        }
                
        $listTariffsResult[Delivery::TYPE_SDEK_SELF] = [
            'min' => !empty($listSdekTariffs['pvz']) ? min($listSdekTariffs['pvz']) : 0,
            'id' => !empty($listSdekTariffs['pvz']) ? array_search(min($listSdekTariffs['pvz']), $listSdekTariffs['pvz']) : 0
        ];
        
        $listTariffsResult[Delivery::TYPE_SDEK_COURIER] = [
            'min' => !empty($listSdekTariffs['courier']) ? min($listSdekTariffs['courier']) : 0,
            'id' => !empty($listSdekTariffs['courier']) ? array_search(min($listSdekTariffs['courier']), $listSdekTariffs['courier']) : 0
        ];
        
        $listTariffsResult[Delivery::TYPE_POST_RUSSIAN] = [
            'min' => $this->getTariffPostMin(Delivery::TYPE_POST_RUSSIAN, $order),
        ];
        
        $listTariffsResult[Delivery::TYPE_EMS] = [
            'min' => $this->getTariffPostMin(Delivery::TYPE_EMS, $order),
        ];
 
        return $listTariffsResult;
        
    }
    
    public function getPriceTariff($order){
        $tariff =  Order::model()->getTariffMin($order);
        $price = $order['delivery_id'] == Delivery::TYPE_SELF ? 0 : $tariff[$order['delivery_id']];
        return $price['min'];
    }
    
    public function saveCookieOrder($order){
        Yii::app()->request->cookies['order'] = new CHttpCookie('order', $order);
    }
    
    public function saveCookieOrderProduct($orderProduct){
        Yii::app()->request->cookies['orderProduct'] = new CHttpCookie('orderProduct', $orderProduct);
    }
    
    public function getProductInfoOrder(){
        $totalWeight = 0; $weightProductOrder = 0;
        
        foreach($this->products as $productOrder){
            $totalWeight += $productOrder->product->weight;
        }
        
        return [
            'totalWeight' => $totalWeight
        ];
    }
}