<?php

/**
 * This is the model class for table "{{store_order_sdek_data}}".
 *
 * The followings are the available columns in table '{{store_order_sdek_data}}':
 * @property integer $id
 * @property string $date
 * @property string $price
 * @property integer $region_id
 * @property integer $city_id
 * @property string $pvz_id
 * @property string $pvz_address
 * @property integer $tariff_id
 * @property integer $min_days
 * @property integer $max_days
 */
class OrderSdekData extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_order_sdek_data}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price', 'required'),
			array('region_id, city_id, tariff_id, min_days, max_days', 'numerical', 'integerOnly'=>true),
			array('price', 'length', 'max'=>6),
			array('pvz_id', 'length', 'max'=>100),
			array('pvz_address', 'length', 'max'=>1000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, price, region_id, city_id, pvz_id, pvz_address, tariff_id, min_days, max_days', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function beforeSave() {
        
        $this->date = new CDbExpression('NOW()');

        return parent::beforeSave();
    }
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'price' => 'Price',
			'region_id' => 'Region',
			'city_id' => 'City',
			'pvz_id' => 'Pvz',
			'pvz_address' => 'Pvz Address',
			'tariff_id' => 'Tariff',
			'min_days' => 'Min Days',
			'max_days' => 'Max Days',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('pvz_id',$this->pvz_id,true);
		$criteria->compare('pvz_address',$this->pvz_address,true);
		$criteria->compare('tariff_id',$this->tariff_id);
		$criteria->compare('min_days',$this->min_days);
		$criteria->compare('max_days',$this->max_days);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderSdekData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
