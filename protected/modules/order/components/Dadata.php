<?php

/**
 * Контроллер для доступа к сервису Dadata
 *
 */

class Dadata extends CApplicationComponent{
	
    public $url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/';
		
	/**
	* @param string
	* используется для запросов данных
	*
	*/
    protected $_api_key;
	
	/**
	* @param string
	* используется для стандартизации данных
	* для запросов данных не используется
	*/
    protected $_secret_key;
	
	
	protected $_params = [];

    /**
     * @param string $api_key
     * @param string $secret_key
     */
	public function init(){
        parent::init();
    }

	public function setApiKey($api_key){
        $this->_api_key = $api_key;
    }
	
	public function getApiKey($api_key){
        $this->_api_key = $api_key;
    }
 
    public function setSecretKey($secret_key){
		$this->_secret_key = $secret_key;
    }
	
	protected function getParams($query){
		return http_build_query($this->_params);
	}
	
	protected function paramsToJson() {
		return json_encode( $this->_params );
	}
		
	protected function resultToArray($result){
		return json_encode($result, true);
	}
	
    /**
     * Возвращает результат запроса к сервису
     *
     */
    public function connect($url){
		
        if ( $ch = curl_init() ) {
			
			curl_setopt( $ch, CURLOPT_URL, $url);
			curl_setopt( $ch, CURLOPT_POST, true);
			curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json',
				'Accept: application/json',
				'Authorization: Token ' . $this->_api_key,
				//'X-Secret: ' . $this->_secret_key,
			]);
			
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $this->paramsToJson());

			$result = curl_exec($ch);
			curl_close($ch);
			return $result;
			
		} else {
			throw new HttpException( 'Can not create connection to ' . $url . ' with args ' . $this->_params, 404 );
		}
		
    }
	
	/**
	*
	* поис по ИНН
	*
	*/
	public function findByInn($inn){
		$this->_params['query'] = $inn;
		$result = $this->connect( $this->url . 'findById/party');
		return json_decode( $result );
	}
	
}
