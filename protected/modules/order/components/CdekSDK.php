<?php

use CdekSDK\Common;
use CdekSDK\Requests;

/**
 * Class CdekSDK
 *
 */
class CdekSDK extends CApplicationComponent{
    
    public $login;

    public $password;
    
    public $senderCityId;

    public function addOrderCdek($orderStore, $productInfo){

        $client = new \CdekSDK\CdekClient($this->login, $this->password);
        
        $order = new Common\Order([
            'Number'   => $orderStore->id,
            'SendCityCode'    => $this->senderCityId, // Оренбург
            'RecCityPostCode' => $orderStore->zipcode,
            'RecipientName'  => $orderStore->family . ' ' . $orderStore->lastname,
            'RecipientEmail' => $orderStore->email,
            'Phone' => '+'. preg_replace('/[^0-9]/', '', $orderStore->phone),
            'TariffTypeCode' => $orderStore->tariff_id,
            'PvzCode' => ($orderStore->delivery_id == Delivery::TYPE_SDEK_SELF) ? $orderStore->pvz_id : '',
        ]);
        
        if($orderStore->delivery_id == Delivery::TYPE_SDEK_COURIER){
            $order->setAddress(Common\Address::create([
                'Street' => $orderStore->street,
                'House'  => $orderStore->house,
                'Flat'   => $orderStore->apartment,
            ]));
        }
 
        $package = Common\Package::create([
            'Number'  => $orderStore->id,
            'BarCode' => $orderStore->id,
            'Weight'  => $productInfo['totalWeight'] * 1000, // Общий вес (в граммах)
        ]);

        foreach($orderStore->products as $productOrder){
            $package->addItem(new Common\Item([
                'Amount'  => $productOrder->quantity, // Количество единиц одноименного товара (в штуках)
                'WareKey' => $productOrder->sku, // Идентификатор/артикул товара/вложения
                'Cost'    => 1, // Объявленная стоимость товара (за единицу товара)
                'Payment' => 0, // Оплата за товар при получении (за единицу товара)
                'Weight'  => $productOrder->product->weight * 1000, // Вес (за единицу товара, в граммах)
                'Comment' => $productOrder->product_name
            ]));
        }
        
        $order->addPackage($package);

        $request = new Requests\DeliveryRequest([
            'Number' => $orderStore->id,
        ]);
        
        $request->addOrder($order);

        $response = $client->sendDeliveryRequest($request);

        if ($response->hasErrors()) {
            // обработка ошибок            
            foreach ($response->getErrors() as $order) {
                // заказы с ошибками
                return [
                    'result' => [
                        'isEmptyError' => false,
                        'message' => $order->getMessage(),
                        'ErrorCode' => $order->getErrorCode(),
                        'Number' => $order->getNumber(),
                    ]
                ];
            }
        }

        foreach ($response->getOrders() as $order) {
            // сверяем данные заказа, записываем номер 
            $order->getNumber();
            $dispatchNumber = $order->getDispatchNumber();
            break;
        }
        
        return [
            'result' => [
                'isEmptyError' => true,
                'dispatchNumber' => $dispatchNumber
            ]
        ];
    }
    
    public function printReceipt($dispatch_number){
        
        $client = new \CdekSDK\CdekClient($this->login, $this->password);
        
        $request = new Requests\PrintReceiptsRequest([
            'CopyCount' => 4,
        ]);
        
        $request->addOrder(Common\Order::withDispatchNumber($dispatch_number));

        $response = $client->sendPrintReceiptsRequest($request);
 
        if ($response->hasErrors()) {
            return [
                'result' => false,
                'message' => 'Заказ не найден в базе СДЭК'
            ];
        }

        // Или возвращаем содержимое PDF файла...
        header("Content-type:application/pdf");
        // It will be called downloaded.pdf
        header("Content-Disposition:attachment;filename=consignment.pdf");
        
        // The PDF source is in original.pdf
        echo (string) $response->getBody();     
    }
}
