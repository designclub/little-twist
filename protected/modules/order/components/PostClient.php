<?php

use RussianPost\Http\Client;

/**
 * PostClient
 */
class PostClient extends Client
{
    protected $url = 'https://otpravka-api.pochta.ru/postoffice/1.0/';
}
