<?php if(!empty($deliveryTypes)): ?>
    <h3>Выберите вариант доставки:</h3>
    <fieldset class="row block-delivery">
        <?php foreach ($deliveryTypes as $key => $delivery): ?>
            <?php if(($delivery->id == Delivery::TYPE_SELF) || (!empty($deliveryTariffs[$delivery->id]))): ?>
                <div class="item-delivery <?= $delivery->class_delivery; ?> col-sm-4 col-xs-12">
                    <input type="radio" name="Order[delivery_id]" id="delivery-<?= $delivery->id; ?>"
                           value="<?= $delivery->id; ?>"
                           
                           <?php if(!empty($order->delivery_id)):?>
                               <?php if($delivery->id == $order->delivery_id):?>
                                   checked="checked"
                                   checked
                               <?php endif;?>
                           <?php endif;?>
                           
                           data-price="<?= $delivery->id == Delivery::TYPE_SELF ? $delivery->price : $deliveryTariffs[$delivery->id]['min']; ?>"
                           data-tariff="<?= ( $delivery->id == Delivery::TYPE_SDEK_SELF || $delivery->id == Delivery::TYPE_SDEK_COURIER )   ? $deliveryTariffs[$delivery->id]['id'] : 0 ; ?>"
                           data-url="<?= ($delivery->id != Delivery::TYPE_SELF) ? Yii::app()->createUrl('/order/order/getDataDeliveries') : ""; ?>"
                           data-free-from="<?= $delivery->free_from; ?>"
                           data-available-from="<?= $delivery->available_from; ?>"
                           data-separate-payment="<?= $delivery->separate_payment; ?>">
                    <label class="radio" for="delivery-<?= $delivery->id; ?>">
                        <div class="delivery-name"><?= $delivery->name; ?></div>
                        <?php if($delivery->id != Delivery::TYPE_SELF): ?>
                            <div class="delivery-price">
                                <p><i class="fa fa-rub" aria-hidden="true"></i></p>
                                <p><?= $deliveryTariffs[$delivery->id]['min']; ?> руб.</p>
                            </div>
                        <?php endif;?>
                        <div class="delivery-description"><?= $delivery->description; ?></div>
                    </label>
                </div>
            <?php endif;?>
        <?php endforeach; ?>
   </fieldset>
<?php else: ?>
    <?= Yii::t("CartModule.cart", "Delivery method aren't selected! The ordering is impossible!") ?>
<?php endif;?>