<?php

/**
 * Class OrderController
 */
class OrderController extends \yupe\components\controllers\FrontController
{
    /**
     * @param null $url
     * @throws CHttpException
     */
    public function actionView($url = null){
        if (!Yii::app()->getModule('order')->showOrder && !Yii::app()->getUser()->isAuthenticated()) {
            throw new CHttpException(404, Yii::t('OrderModule.order', 'Page not found!'));
        }

        $model = Order::model()->findByUrl($url);

        if ($model === null) {
            throw new CHttpException(404, Yii::t('OrderModule.order', 'Page not found!'));
        }

        $this->render('view', ['model' => $model]);
    }

    /**
     *
     */
    public function actionCreate(){
        
        $model = new Order(Order::SCENARIO_USER);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Order') && Yii::app()->getRequest()->getIsAjaxRequest()){
            Order::model()->saveCookieOrder(Yii::app()->getRequest()->getPost('Order'));
            Order::model()->saveCookieOrderProduct(Yii::app()->getRequest()->getPost('OrderProduct'));
        }
        
        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Order') && !Yii::app()->getRequest()->getIsAjaxRequest()) {
            
            $order = Yii::app()->getRequest()->getPost('Order');

            if(!empty($order['delivery_id'])){
                if($order['delivery_id'] == Delivery::TYPE_SDEK_SELF)
                    $model->scenario = Order::SCENARIO_DELIVERY_POINT;
                else if( $order['delivery_id'] != Delivery::TYPE_SDEK_SELF && $order['delivery_id'] != Delivery::TYPE_SELF)
                    $model->scenario = Order::SCENARIO_ADDRESS;
            }

            $products = Yii::app()->getRequest()->getPost('OrderProduct');

            $coupons = isset($order['couponCodes']) ? $order['couponCodes'] : [];

            if ($model->store($order, $products, Yii::app()->getUser()->getId(), (int)Yii::app()->getModule('order')->defaultStatus)) {

/*$response = Yii::app()->cdekSDK->addOrderCdek($model, $model->getProductInfoOrder());

if($response['result']['isEmptyError']){
    $model->dispatch_number = $response['result']['dispatchNumber'];
    $model->save();
}
                
$this->render('success', ['order' => $model]);*/
                
                Yii::app()->cart->clear();

                if (!empty($coupons)) {
                    $model->applyCoupons($coupons);
                }
                

                
                Yii::app()->getUser()->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OrderModule.order', 'The order created')
                );

                Yii::app()->eventManager->fire(OrderEvents::CREATED_HTTP, new OrderEvent($model));

                if (Yii::app()->getModule('order')->showOrder) {
                    $this->redirect(['/order/order/view', 'url' => $model->url]);
                }

                $this->redirect(['/store/product/index']);
            } else {
                $error = CHtml::errorSummary($model);
                Yii::app()->getUser()->setFlash(
                    yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                    $error ?: Yii::t('OrderModule.order', 'Order error')
                );

                $this->redirect(['/cart/cart/index']);
            }
        }

        $this->redirect(Yii::app()->getUser()->getReturnUrl());
    }

    /**
     * @throws CHttpException
     */
    public function actionCheck()
    {
        if (!Yii::app()->getModule('order')->enableCheck) {
            throw new CHttpException(404);
        }

        $form = new CheckOrderForm();

        $order = null;

        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $form->setAttributes(
                Yii::app()->getRequest()->getPost('CheckOrderForm')
            );

            if ($form->validate()) {
                $order = Order::model()->findByNumber($form->number);
            }
        }

        $this->render('check', ['model' => $form, 'order' => $order]);
    }
    
    public function actionDeliveryTypes(){
        
        $deliveryTypes = Delivery::model()->published()->findAll();
        
        $deliveryTariffs = Order::model()->getTariffMin(Yii::app()->getRequest()->getPost('Order'));
 
        $order = Order::model()->saveCookieOrder(Yii::app()->getRequest()->getPost('Order'));
        
        if(Yii::app()->getRequest()->getIsAjaxRequest()){
            $this->renderPartial('application.modules.order.views.order._delivery-types', ['deliveryTypes' => $deliveryTypes, 'deliveryTariffs' => $deliveryTariffs, 'order' => $order], false, true);
            Yii::app()->end();
        }
    }
    
    public function actionGetDataDeliveries(){
        
        $listPVZ = []; $weightmin = 0;
        
        $order = Yii::app()->getRequest()->getPost('Order');
        $products = Yii::app()->getRequest()->getPost('OrderProduct');
                
        Order::model()->saveCookieOrder($order);
        Order::model()->saveCookieOrderProduct($products);
        
        if($order['delivery_id'] == Delivery::TYPE_SDEK_SELF){
            $listPVZ = Yii::app()->cdek->getListPVZ(null, $order['zipcode'], $weightmin);
             $this->renderPartial('_list-pvz', ['pvz' => $listPVZ], false, true);
        }
        Yii::app()->end();
    }
    
    public function actionUpdateDeliveryData(){

        $orderData = Yii::app()->getRequest()->getPost('Order');
        $orderProduct = Yii::app()->getRequest()->getPost('OrderProduct');
        
        Order::model()->saveCookieOrder($orderData);
        Order::model()->saveCookieOrderProduct($orderProduct);
        
        $order = new Order;
        $order->setAttributes($orderData);
        
        $deliveryTypes = Delivery::model()->published()->findAll();
        $deliveryTariffs = Order::model()->getTariffMin($orderData);
 
        if(Yii::app()->getRequest()->getIsAjaxRequest()){
            $this->renderPartial('application.modules.order.views.order._delivery-types', ['deliveryTypes' => $deliveryTypes, 'deliveryTariffs' => $deliveryTariffs, 'order' => $order ], false, true);
            Yii::app()->end();
            
        }
    }
    
    public function actionGetPrices(){
        
        $weightmax = 0;
        
        $goods = [];
        $listPVZ = [];
        $tariffs = [];
 
        $products = Yii::app()->getRequest()->getPost('OrderProduct');
        $order = Yii::app()->getRequest()->getPost('Order');
        $delivery_id = $order['delivery_id'];

        switch($delivery_id){
            case 2:
                $tariffType = 'pvz';
            break;
            case 4:
                $tariffType = 'courier';
            break;
            default:
                $tariffType = 'all';
            break;
        }
  
        $tariffList = Yii::app()->getComponent('cdekRepository')->getListTariffs($tariffType);

        if (is_array($products)) {
            $i = 0;
            foreach ($products as $key => $op) {
                $product = null;
                if (isset($op['product_id'])) {
                    for ($j = 1; $j <= $op['quantity']; $j++){
                        $product = Product::model()->findByPk($op['product_id']);
                        if(isset($product->weight) && $product->length && $product->width && $product->height){
                            $goods[$i]['width'] = $product->width * 100;
                            $goods[$i]['height'] = $product->height * 100;
                            $goods[$i]['length'] = $product->length * 100;
                            $goods[$i]['weight'] = $product->weight;
                            //$goods[$i]['volume'] = $product->width * $product->height * $product->length;
                            $weightmax += $product->weight;
                            ++$i;
                        }
                    }
                }
            }
        }
 
        //если выбран самовывоз из пункта доставки
        if($delivery_id == 2){
            $listPVZ = Yii::app()->cdek->getListPVZ($order['city'], null, $weightmax);
        }
 
        $prices = Yii::app()->cdek->geCalcOrder(Order::SENDER_CITY_ID, $order['city'], $tariffList, $goods);
        //$prices = Yii::app()->cdek->geCalcWithPriorityOrder(Order::SENDER_CITY_ID, $order['city'], $tariffList, $goods);
 
        if (isset($prices['result'])) {
            foreach ($prices['result'] as $key => $item) {
                if (!isset($item['result']['errors'])) {
                    $tariffs[$key]['id'] = $item['tariffId'];
                    $tariffs[$key]['name'] = Order::model()->getCDEKtariffName($tariffType, $item['tariffId']);
                    $tariffs[$key] += $item['result'];
                }
            }
        }
 
        $this->renderPartial('_cdek-tariff', ['tariffs' => $tariffs, 'pvz' => $listPVZ, 'delivery' => $delivery_id], false, true);
    }
    
    public function actionSaveSDEK(){
        $goods = []; $weightmax = 0;
        $order = Yii::app()->getRequest()->getPost('Order');
        $products = Yii::app()->getRequest()->getPost('OrderProduct');
        
        if (is_array($products)) {
            $i = 0;
            foreach ($products as $key => $op) {
                $product = null;
                if (isset($op['product_id'])) {
                    for ($j = 1; $j <= $op['quantity']; $j++){
                        $product = Product::model()->findByPk($op['product_id']);
                        if(isset($product->weight) && $product->length && $product->width && $product->height){
                            $goods[$i]['weight'] = $product->weight;
                            $goods[$i]['length'] = $product->length * 100;
                            $goods[$i]['width'] = $product->width * 100;
                            $goods[$i]['height'] = $product->height * 100;
                            $weightmax += $product->weight;
                            ++$i;
                        }
                    }
                }
            }
        }

        if(isset($order['tariff_id'])){
            
            $tariffList = [
                0 => [
                    'id' => $order['tariff_id']
                ]
            ];

            $price = Yii::app()->cdek->geCalcOrder(Order::SENDER_CITY_ID, $order['city'], $tariffList, $goods);
 
           
            
            $criteria = new CDbCriteria;
            $criteria->condition = 'price = :price AND region_id = :region AND city_id = :city';
            $criteria->params = [':price' => $price['result'][0]['result']['price'], ':region' => $order['region_id'], ':city' => $order['city']];
            
            if(isset($order['tariff_id']))
                $criteria->compare('tariff_id', $order['tariff_id']);
            
            if(isset($order['pvz_id']))
                $criteria->compare('pvz_id', $order['pvz_id']);
            
            $sdekData = OrderSdekData::model()->find($criteria);
            
            if(count($sdekData) == 0){
                $sdekData = new OrderSdekData();
                $sdekData->setAttributes([
                    'price'    => $price['result'][0]['result']['price'],
                    'region_id'    => $order['region_id'],
                    'city_id' => $order['city'],
                    'tariff_id' => $order['tariff_id'] ?? '',
                    'pvz_id' => $order['pvz_id'] ?? '',
                ]);
                $sdekData->save();
            }
            
            echo $sdekData->id;
            
            Yii::app()->end();
        }
    }
    
}
