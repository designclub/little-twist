<?php

class m191018_090038_create_order_post_price_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{order_post_price}}', [
            'id' => 'pk',
            'price' => 'decimal(6,2)',
            'index' => 'integer',
            'order_id' => 'integer',
            'min_deys' => 'integer',
            'max_deys' => 'integer',
        ], $this->getOptions());
    }

    public function safeDown()
    {
        $this->dropTable('{{order_post_price}}');
    }
}
