<?php

class m191020_183335_add_column_tariff_pvz_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_order}}', 'pvz_id', 'varchar(100)');
        $this->addColumn('{{store_order}}', 'pvz_address', 'varchar(1000)');
        $this->addColumn('{{store_order}}', 'tariff_id', 'integer(4)');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_order}}', 'pvz_id');
        $this->dropColumn('{{store_order}}', 'pvz_address');
        $this->dropColumn('{{store_order}}', 'tariff_id');
	}
}