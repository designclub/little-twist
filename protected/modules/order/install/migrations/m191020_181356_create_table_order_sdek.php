<?php

class m191020_181356_create_table_order_sdek extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{store_order_sdek_data}}', [
            'id' => 'pk',
            'date' => 'datetime NOT NULL',
            'price' => 'decimal(6,2) NOT NULL',
            'region_id' => 'integer',
            'city_id' => 'integer',
            'pvz_id' => 'varchar(100) DEFAULT NULL',
            'pvz_address' => 'varchar(1000) DEFAULT NULL',
            'tariff_id' => 'integer',
            'min_days' => 'integer',
            'max_days' => 'integer',
        ], $this->getOptions());
    }

    public function safeDown()
    {
        $this->dropTable('{{store_order_sdek_data}}');
    }
}