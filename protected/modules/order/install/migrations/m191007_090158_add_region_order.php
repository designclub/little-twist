<?php

class m191007_090158_add_region_order extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_order}}', 'region_id', 'integer');
        $this->addColumn('{{store_order}}', 'regionName', 'varchar(255)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_order}}', 'region_id');
        $this->dropColumn('{{store_order}}', 'regionName');
    }
}
