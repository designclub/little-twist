<?php

class m200212_172620_add_dispatch_number extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_order}}', 'dispatch_number', 'int(12)');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_order}}', 'dispatch_number');
	}
}