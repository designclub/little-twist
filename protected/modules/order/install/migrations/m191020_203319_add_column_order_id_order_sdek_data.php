<?php

class m191020_203319_add_column_order_id_order_sdek_data extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_order_sdek_data}}', 'order_id', 'integer(11)');
	}

	public function safeDown()
	{
         $this->dropColumn('{{store_order_sdek_data}}', 'order_id');
	}
}