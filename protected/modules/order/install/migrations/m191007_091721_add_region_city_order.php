<?php

class m191007_091721_add_region_city_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_order}}', 'regionCode', 'varchar(255)');
        $this->addColumn('{{store_order}}', 'regionCodeEx', 'varchar(255)');
        $this->addColumn('{{store_order}}', 'cityName', 'varchar(255)');
        $this->addColumn('{{store_order}}', 'cityCode', 'integer(11)');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_order}}', 'regionCode');
        $this->dropColumn('{{store_order}}', 'regionCodeEx');
        $this->dropColumn('{{store_order}}', 'cityName');
        $this->dropColumn('{{store_order}}', 'cityCode');
	}
}