<?php

class m191017_190431_add_fields_for_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_order}}', 'fname', 'varchar(255)');
        $this->addColumn('{{store_order}}', 'lastname', 'varchar(255)');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_order}}', 'fname');
        $this->dropColumn('{{store_order}}', 'lastname');
	}
}