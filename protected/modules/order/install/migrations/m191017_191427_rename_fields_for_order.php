<?php

class m191017_191427_rename_fields_for_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->renameColumn('{{store_order}}', 'fname', 'family');

	}

	public function safeDown()
	{
        $this->dropColumn('{{store_order}}', 'family');
	}
}