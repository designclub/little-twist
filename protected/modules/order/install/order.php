<?php

return [
    'module' => [
        'class' => 'application.modules.order.OrderModule',
        'panelWidgets' => [
            'application.modules.order.widgets.PanelOrderStatWidget' => [
                'limit' => 5
            ]
        ],
    ],
    'import' => [
        'application.modules.order.models.*',
        'application.modules.order.helpers.*',
        'application.modules.order.components.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'order.pay.success' => [
                    ['PayOrderListener', 'onSuccessPay']
                ],
                'http.order.created' => [
                    ['OrderListener', 'onCreate']
                ],
                'http.order.updated' => [
                    ['OrderListener', 'onUpdate']
                ]
            ]
        ],
        'orderNotifyService' => [
            'class' => 'application.modules.order.components.OrderNotifyService',
            'mail'  => 'mail'
        ],
        'dadata' => [
			'class' => 'application.modules.order.components.Dadata',
			'url' => 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/',
			'apiKey' => '85986aed4115e317a88a5288e91de4a3c3b5f0bb',
			//'secretKey' => '198644172667c5b77d8212a6eeb061635dcdf54f',
		],
        'cdekSDK' => [
            'class' => 'application.modules.order.components.CdekSDK',
            //'login' => '7JM7K5twfzEV1ssCRklthcIPbbVZrZrZ', //test
            //'password' => 't8XBoL1rUofIK9dKoXVB3Tji2F2hPHSk' //test
            'login' => 'DZtMApBb58R3N9vNc8O8UeFmYXWF0m34', //work
            'password' => 'devEhPwEX6kAiy08i7xvoY7WYz0SGjcV', //secure password
            'senderCityId' => 261 //zipcode for city sending
        ],
    ],
    'rules' => [
        '/order/check'    => '/order/order/check',
        '/order/getSettlement' => '/order/order/getSettlement',
        '/order/getCities' => '/order/order/getCities',
        '/order/deliveryTypes' => '/order/order/deliveryTypes',
        '/order/updateDeliveryData' => '/order/order/updateDeliveryData',
        '/order/getPrices' => '/order/order/getPrices',
        '/order/getListPVZ' => '/order/order/getListPVZ',
        '/order/saveSDEK' => '/order/order/saveSDEK',
        '/order/<url:\w+>' => 'order/order/view',
        '/store/order/<action:\w+>' => 'order/order/<action>',
        '/store/account' => 'order/user/index',
        '/store/account/<action:\w+>' => 'order/user/<action>',
    ],
];
