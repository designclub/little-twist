<?php
/**
 * SlickWidget виджет
 *
 *
 **/
Yii::import('application.modules.gallery.models.*');

class SlickWidget extends yupe\widgets\YWidget
{
    //сколько изображений выводить на одной странице
    public $limit = 30;

    //ID-галереи
    public $galleryId;

    //Галерея
    public $gallery;

    public $slickClass = 'slick-carousel';
	
    public $view = 'slick';

    public $options = [
		'autoplay' => true,
        'arrows' => false,
        'dots' => false,
    ];
    
    public $clietOptions = [];
	
    /**
     * Запускаем отрисовку виджета
     *
     * @return void
     */
    public function run(){
		
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(
                Yii::getPathOfAlias('application.modules.gallery.views.assets.css') . '/slick.min.css'
            )
        );
		
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
            Yii::getPathOfAlias('application.modules.gallery.views.assets.js') . '/slick.min.js'), CClientScript::POS_BEGIN);
		
        if(is_array($this->clietOptions) && count($this->clietOptions))
            $options = CJavaScript::encode(array_merge($this->options, $this->clietOptions));
        else
		  $options = CJavaScript::encode($this->options);
		
		Yii::app()->getClientScript()->registerScript($this->galleryId, "$('.{$this->slickClass}').slick({$options})");
		
		if(!empty($this->galleryId)){
			$dataProvider = new CActiveDataProvider(
				'ImageToGallery', [
					'criteria'   => [
						'condition' => 't.gallery_id = :gallery_id',
						'params'    => [':gallery_id' => $this->galleryId],
						'limit'     => $this->limit,
						'order'     => 't.create_time DESC',
						'with'      => 'image',
					],
					'pagination' => ['pageSize' => $this->limit],
				]
			);
		}
		
        $this->render($this->view,
            [	
				'id'			=> $this->galleryId,
                'dataProvider' 	=> !empty($dataProvider) ? $dataProvider : null,
                'gallery'      	=> $this->gallery,
                'slickClass'      	=> $this->slickClass,
            ]);
    }
}
