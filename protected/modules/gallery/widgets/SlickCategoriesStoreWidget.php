<?php
/**
 * SlickCategoriesStoreWidget виджет
 *
 *
 **/
Yii::import('application.modules.gallery.models.*');

class SlickCategoriesStoreWidget extends yupe\widgets\YWidget
{

    public $id = 'carousel-categories-store';
    
    public $slickClass = 'slick-carousel-categories';
	
    public $view = 'slick-carousel-categories';

    public $options = [
		'autoplay' => true,
        'arrows' => false,
        'dots' => false,
        'responsive' => [
                [
                    'breakpoint' => 767,
                    'settings'=> [
                        'slidesToShow' => 1,
                        'arrows' => false
                    ]
                ],                
                [
                    'breakpoint' => 1024,
                    'settings'=> [
                        'slidesToShow' => 2,
                    ]
                ],
            ]
        ];
    
    public $clietOptions = [];
	
    /**
     * @var ProductRepository
     */
    protected $categoriesRepository;
    
    public function init()
    {
        $this->categoriesRepository = Yii::app()->getComponent('categoryRepository');
    }
    
    /**
     * Запускаем отрисовку виджета
     *
     * @return void
     */
    public function run(){
		
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(
                Yii::getPathOfAlias('application.modules.gallery.views.assets.css') . '/slick.min.css'
            )
        );
		
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
            Yii::getPathOfAlias('application.modules.gallery.views.assets.js') . '/slick.min.js'), CClientScript::POS_BEGIN);
		
        if(is_array($this->clietOptions) && count($this->clietOptions))
            $options = CJavaScript::encode(array_merge($this->options, $this->clietOptions));
        else
		  $options = CJavaScript::encode($this->options);
		
		Yii::app()->getClientScript()->registerScript($this->id, "$('.{$this->slickClass}').slick({$options})");
		
        $dataProvider = $this->categoriesRepository->getAllCategoriesRootsDataProvider();
 
        $this->render($this->view,
            [	
                'dataProvider' 	=> !empty($dataProvider) ? $dataProvider : null,
                'slickClass'      	=> $this->slickClass,
            ]);
    }
}
