<?php
/**
 * OrderStatusWidget виджет формы "Статус заказа"
 */
Yii::import('application.modules.mail.models.form.OrderStatusFormModal');

class OrderStatusWidget extends yupe\widgets\YWidget
{
    public $view = 'order-status-widget';

    public function run()
    {
        $model = new OrderStatusFormModal;
        if (isset($_POST['OrderStatusFormModal'])) {
            $model->attributes = $_POST['OrderStatusFormModal'];
            if($model->verify == ''){
                if ($model->validate()) {
					$status = Yii::app()->remonline->getOrder($model->code);

                    if(!isset($status)){
                        $model->addError('code', Yii::t('MailModule.mail', 'Заказ не найден. Проверьте номер заказа!'));
                    } else{
                        $message = $status['status']['name'];
                        Yii::app()->user->setFlash('order-status-success', $message);
                        Yii::app()->controller->refresh();
                    }
                }
            }
        }      

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

}
