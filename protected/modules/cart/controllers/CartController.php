<?php

use yupe\components\controllers\FrontController;
use RussianPost\ApiClient;
use RussianPost\Http\Client;

Yii::import('application.modules.order.components.PostClient');

/**
 * Class CartController
 */
class CartController extends FrontController
{
    /**
    *
    */
    public function actionIndex(){

        $positions = Yii::app()->cart->getPositions();
        
        $order = new Order(Order::SCENARIO_USER);
        
        if(!empty(Yii::app()->request->cookies['order']->value))
            $order->setAttributes(Yii::app()->request->cookies['order']->value);
 
        if(!empty($order->delivery_id)){
            if($order->delivery_id == Delivery::TYPE_SDEK_SELF)
                $order->scenario = Order::SCENARIO_DELIVERY_POINT;
            else if( $order->delivery_id != Delivery::TYPE_SDEK_SELF && $order->delivery_id != Delivery::TYPE_SELF)
                $order->scenario = Order::SCENARIO_ADDRESS;
        }
        
        if (Yii::app()->getUser()->isAuthenticated()) {
            $user = Yii::app()->getUser()->getProfile();
            $order->setAttributes([
                'name' => $user->getFullName(),
                'email' => $user->email,
            ]);
        }

        $coupons = [];

        if (Yii::app()->hasModule('coupon')) {
            $couponCodes = Yii::app()->cart->couponManager->coupons;

            foreach ($couponCodes as $code) {
                $coupons[] = Coupon::model()->getCouponByCode($code);
            }
        }

        $deliveryTypes = Delivery::model()->published()->findAll();

        $this->render(
            'index',
            ['positions' => $positions, 'order' => $order, 'coupons' => $coupons, 'deliveryTypes' => $deliveryTypes]
        );
    }

    public function actionPostAddress()
    {
        $address = isset($_GET['address']) ? $_GET['address'] : null;

        $data = [];

        if ($address) {
            $client = new ApiClient(
                Yii::app()->params['postRF']['token'],
                Yii::app()->params['postRF']['login'],
                Yii::app()->params['postRF']['password']
            );

            $responce = $client
                ->getCleanAddress([
                    ['id'=> 'adr1', 'original-address' => $address]
                ]);

            if ($responce->isSuccessful()) {
                // Нормализованый адрес
                $na = current($responce->result);
                $string = '';

                $string .= isset($na['index']) ? $na['index'] : '';
                $string .= isset($na['region']) ? ', '.$na['region'] : '';
                $string .= isset($na['place']) ? ', '.$na['place'] : '';
                $string .= isset($na['street']) ? ', '.$na['street'] : '';
                $string .= isset($na['house']) ? ', дом '.$na['house'] : '';
                $string .= isset($na['slash']) ? '/'.$na['slash'] : '';

                $data['address'] = $string;
                $data['data'] = $na;
            }
        }

        echo CJSON::encode($data);
    }

    /**
     * Поиск почтовых отделений по адресу и расчет стоимости доставки
     * @return [type] [description]
     */
    public function actionSearchPostoffice()
    {
        $address = Yii::app()->request->getParam('address');
        $info = Yii::app()->request->getParam('info');
        $deliveryType = Yii::app()->request->getParam('deliveryType');

        $postOffices = [];

        if ($address) {
            $client = new PostClient(
                Yii::app()->params['postRF']['token'],
                Yii::app()->params['postRF']['login'],
                Yii::app()->params['postRF']['password']
            );

            $responce = $client
                ->makeRequest('by-address', Client::METHOD_GET, [
                    'address' => $address,
                ]);

            if ($responce->isSuccessful()) {
                // Нормализованый адрес
                $postOffices = $responce->result['postoffices'] ?? [];
            }
        }

        $client = new ApiClient(
            Yii::app()->params['postRF']['token'],
            Yii::app()->params['postRF']['login'],
            Yii::app()->params['postRF']['password']
        );

        $query = [
            "dimension" => [
                "height" => $info['height'],
                "length" => $info['length'],
                "width" => $info['width'],
            ],
            "index-from" => '460000',
            "delivery-point-index" => (string)current($postOffices),
            "index-to" => (string)current($postOffices),
            "mass" => $info['weight'],
            "mail-category" => "ORDINARY",
            "mail-type" => $deliveryType == 5 ? "EMS_TENDER" : "EMS_OPTIMAL",
            //"mail-type" => $deliveryType == 5 ? "EMS_OPTIMAL" : "POSTAL_PARCEL",
            "fragile" => "false",
        ];

        $tarif = [];
        $responce = $client->getTariff($query);
        if ($responce->isSuccessful()) {
            // Нормализованый адрес
            $tarif = $responce->result;
            $tarif['index'] = (string)current($postOffices);
        }

        if (count($tarif) > 0) {
            $index   = $tarif['index'] ?? '';
            $minDays   = $tarif['delivery-time']['min-days'] ?? '';
            $maxDays   = $tarif['delivery-time']['max-days'] ?? '';
            $totalRateRub = $tarif['total-rate'] > 0 ? $tarif['total-rate'] / 100 : 0; // в рублях
            $totalVatRub  = $tarif['total-vat'] > 0 ? $tarif['total-vat'] / 100 : 0; // в рублях НДС
            $cost = $totalRateRub + $totalVatRub;

            $model = new OrderPostPrice;
            $model->setAttributes([
                'price'    => $cost,
                'index'    => $index,
                'min_deys' => $minDays,
                'max_deys' => $maxDays,
            ]);

            $model->save();

            $this->renderPartial('_post-tariff', [
                'index' => $index,
                'minDays' => $minDays,
                'maxDays' => $maxDays,
                'totalRateRub' => $totalRateRub,
                'totalVatRub' => $totalVatRub,
                'cost' => $cost,
                'model' => $model,
            ]);

            Yii::app()->end();
        }

        echo '';
    }

    /**
     * @throws CHttpException
     */
    public function actionAdd()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $product = Yii::app()->getRequest()->getPost('Product');

        if (empty($product['id'])) {
            throw new CHttpException(404);
        }

        /* @var IECartPosition $model */
        $model = CartProduct::model()->published()->findByPk((int)$product['id']);

        if (null === $model) {
            throw new CHttpException(404);
        }

        $variantsId = Yii::app()->getRequest()->getPost('ProductVariant', []);
        $variants = [];
        foreach ((array)$variantsId as $var) {
            if (!$var) {
                continue;
            }
            $variant = ProductVariant::model()->findByPk($var);
            if ($variant && $variant->product_id == $model->id) {
                $variants[] = $variant;
            }
        }
        $model->selectedVariants = $variants;
        $quantity = empty($product['quantity']) ? 1 : (int)$product['quantity'];

        try {
            Yii::app()->cart->put($model, $quantity);
            Yii::app()->ajax->success(['message' => Yii::t("CartModule.cart", 'Product successfully added to your basket'), 'count'=>Yii::app()->cart->getCount(), 'sum'=>Yii::app()->cart->getCost(), 'product'=>$product]);
        } catch (Exception $exception) {
            Yii::app()->ajax->failure($exception->getMessage());
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionUpdate()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('id')) {
            throw new CHttpException(404);
        }

        /* @var IECartPosition $position */
        $position = Yii::app()->cart->itemAt(Yii::app()->getRequest()->getPost('id'));
        $quantity = (int)Yii::app()->getRequest()->getPost('quantity');

        try {
            Yii::app()->cart->update($position, $quantity);
            Yii::app()->ajax->success(Yii::t("CartModule.cart", 'Quantity changed'));
        } catch (Exception $exception) {
            Yii::app()->ajax->failure($exception->getMessage());
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionDelete()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('id')) {
            throw new CHttpException(404);
        }
        
        Yii::app()->cart->remove(Yii::app()->getRequest()->getPost('id'));
        Yii::app()->ajax->success(Yii::t("CartModule.cart", 'Product removed from cart'));
    }

    /**
     * @throws CHttpException
     */
    public function actionClear()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        Yii::app()->cart->clear();
        Yii::app()->ajax->success(Yii::t("CartModule.cart", 'Cart is cleared'));
    }

    /**
     *
     */
    public function actionWidget()
    {
        $this->widget('cart.widgets.ShoppingCartWidget', ['id' => 'shopping-cart-widget']);
    }
}
