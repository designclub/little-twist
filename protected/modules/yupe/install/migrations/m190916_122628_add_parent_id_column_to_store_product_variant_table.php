<?php

class m190916_122628_add_parent_id_column_to_store_product_variant_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_product_variant}}', 'parent_id', 'integer');
        $this->addForeignKey(
            'fk_{{store_product_variant}}_parent_id',
            '{{store_product_variant}}',
            'parent_id',
            '{{store_product_variant}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_{{store_product_variant}}_parent_id',
            '{{store_product_variant}}'
        );
        $this->dropColumn('{{store_product_variant}}', 'parent_id');
    }
}
