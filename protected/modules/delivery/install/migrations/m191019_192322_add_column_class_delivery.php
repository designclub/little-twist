<?php

class m191019_192322_add_column_class_delivery extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_delivery}}', 'class_delivery', 'varchar(255)');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_delivery}}', 'class_delivery');
	}
}