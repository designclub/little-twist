<?php
use yupe\components\controllers\FrontController;
use yupe\widgets\YFlashMessages;

/**
 * Class PaymentController
 */
class PaymentController extends FrontController
{
    /**
     * @param null $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionProcess($id = null)
    {
        /* @var $payment Payment */
        $payment = Payment::model()->findByPk($id);

        if (null === $payment && !$payment->module) {
            throw new CHttpException(404);
        }

        /** @var PaymentSystem $paymentSystem */
        if ($paymentSystem = Yii::app()->paymentManager->getPaymentSystemObject($payment->module)) {
            $result = $paymentSystem->processCheckout($payment, Yii::app()->getRequest());
            
            if ($result instanceof Order) {
                Yii::app()->getUser()->setFlash(YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PaymentModule.payment', 'Success get pay info!'));
                $this->redirect(['/order/order/view', 'url' => $result->url]);
            }
        }
    }
    	   
	public function actionSuccess(){
 
		if (!empty(Yii::app()->getRequest()->getParam('orderId'))){
			$order = Order::model()->findByAttributes(['orderId' => Yii::app()->getRequest()->getParam('orderId')]);
			$order->payment_method_id = Order::Sberbank;
            
            if($order->save()){
                
                $payment = Payment::model()->findByPk($order->payment_method_id);
                
                if ($paymentSystem = Yii::app()->paymentManager->getPaymentSystemObject($payment->module)){
                    $result = $paymentSystem->processCheckout($payment, Yii::app()->getRequest());

                    if($result->isPaid()){
                        $order->paid = Order::PAID_STATUS_PAID;
                        
                        if($order->delivery_id == TYPE_SDEK_SELF || $order->delivery_id == TYPE_SDEK_COURIER){
                            
                            $response = Yii::app()->cdekSDK->addOrderCdek($order, $order->getProductInfoOrder());
                            
                            if($response['result']['isEmptyError']){
                                $order->dispatch_number = $response['result']['dispatchNumber'];
                                $order->save();
                            }
                        }
                        $this->render('success', ['order' => $order]);
                    }
                    else
                        $this->redirect('fail');
                }
			}
		}
    }
 
                
/*    public function actionTestOrder(){
        $id = 118;
        $order = Order::model()->findByPk($id);
        $dispatchNumber = Yii::app()->cdekSDK->addOrderCdek($order);
        echo '<pre>';
        print_r($dispatchNumber);
        Yii::app()->end();
    }*/

    public function actionFail(){
        $this->render('fail');
    }
}
