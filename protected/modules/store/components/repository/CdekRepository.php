<?php

/**
 * Class CdekRepository
 */
class CdekRepository extends CApplicationComponent
{

    public function getListRegions()
    {
        $regions = Yii::app()->cdek->getListRegions();
        return $regions;
    }

    public function getListRegionsForSelect()
    {
        $tags = [];
        $regions = $this->getListRegions();
        if (count($regions) > 0) {
            foreach ($regions as $key => $region) {
                if (isset($region['regionCodeExt'], $region['regionName'])) {
                    $tags[$region['regionCodeExt']] = $region['regionName'];
                }
            }
        }
        return $tags;
    }

    public function getListCitiesForSelect()
    {
        $tags = [];
        $cities = $this->getListCities($region = 56);
        if (count($cities) > 0) {
            foreach ($cities as $key => $city) {
                $tags[$city['cityCode']] = $city['cityName'];
            }
        }
        return $tags;
    }

    public function getListCitiesForRegion($region)
    {
        $tags = [];
        $cities = $this->getListCities($region);
        if (count($cities) > 0) {
            foreach ($cities as $key => $city) {
                $tags[$city['cityCode']] = $city['cityName'];
            }
        }
        return $tags;
    }

    public function getListCities($region = 56)
    {
        $cities = Yii::app()->cdek->getListCities($region);
        return $cities;
    }
    
    public function getListTariffs($tariffType){

        $list = Order::model()->getCDEKtariffList($tariffType);
        $tariffs = [];
 
        if(count($list) > 0){
            foreach($list as $key => $tariff){     
                $tariffs[] = ['id' => $key];
            }
        }

        return $tariffs;
    }
}
