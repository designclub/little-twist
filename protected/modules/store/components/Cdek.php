<?php

/**
 * Class Cdek
 *
 */
class Cdek extends CApplicationComponent
{

    public $url = 'https://integration.edu.cdek.ru'; //test Url
    //public $url = 'https://integration.cdek.ru'; //work Url
    
    //public $priceurl = 'http://api.edu.cdek.ru'; //test Url
    public $priceurl = 'http://api.cdek.ru'; //work Url
    
    public $login;

    public $password;

    private $_params = [];

    public function init()
    {
        parent::init();
    }

    public function setLogin($login)
    {
        //$this->_params['login'] = $this->login;
        $this->login = $this->login;
    }

    public function getLogin($login)
    {
        $this->login = $login;
    }

    public function setPassword($password)
    {
        //$this->_params['password'] = $this->password;
        $this->password = $this->password;
    }

    public function getPassword($password)
    {
        $this->password = $password;
    }

    protected function getParams()
    {
        return http_build_query($this->_params);
    }

    protected function paramsToJson()
    {
        return json_encode($this->_params);
    }

    protected function resultToArray($result)
    {
        return json_decode($result, true);
    }
    
    protected function getAuthToken(){
        return md5(date('Y-m-d').'&'. $this->password);
    }

    public function connect($url)
    {

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . $this->getParams());
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        /*  curl_setopt( $ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept: application/json',
            ]);*/
/*echo '<pre>';
print_r( $url . '?' . $this->getParams());
Yii::app()->end();*/
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        } else {
            throw new HttpException('Can not create connection to ' . $url . ' with args ' . $this->_params, 404);
        }
    }


    protected function connectPost($url)
    {

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->paramsToJson());
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
            ]);
            
            $result = curl_exec($ch);
            curl_close($ch);
/*echo '<pre>';
print_r($this->_params);
print_r($this->resultToArray($result));
Yii::app()->end();*/
            return $result;
        } else {
            throw new HttpException('Can not create connection to ' . $url . ' with args ' . $this->_params, 404);
        }
    }    
    
    protected function connectPostParams($params, $url)
    {

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
            ]);
            
            $result = curl_exec($ch);
            curl_close($ch);
            
echo '<pre>';
echo 'params = ';
print_r($params);echo '<br>';
print_r(json_encode($params));
echo '<br>';
echo 'request = ';
print_r($this->resultToArray($result));
Yii::app()->end();
            
            return $result;
        } else {
            throw new HttpException('Can not create connection to ' . $url . ' with args ' . $this->_params, 404);
        }
    }

    /**
    *   Список регионов
    *
    *
    */
    public function getListRegions($countryCode = 'RU', $countryName = null)
    {

        if ($countryCode) {
            $this->_params['countryCode'] = $countryCode;
        }

        if ($countryName) {
            $this->_params['countryName'] = $countryName;
        }

        $result = $this->connect($this->url . '/v1/location/regions/json');
        return $this->resultToArray($result);
    }

    /**
    * Список городов
    * var countryCode Код страны в формате ISO 3166-1 alpha-2 по-умолчанию = RU
    * var regionCodeExt Код региона по-умолчанию = 56 //Оренбургская область
    *
    */
    public function getListCities($regionCodeExt = null, $countryCode = 'RU')
    {
        if ($countryCode) {
            $this->_params['countryCode'] = $countryCode;
        }

        if (!empty($regionCodeExt)) {
            $this->_params['regionCodeExt'] = $regionCodeExt;
        } else {
            $this->_params['regionCodeExt'] = 56;
        }

        $result = $this->connect($this->url . '/v1/location/cities/json');
        return $this->resultToArray($result);
    }

    /**
    * Получение списка ПВЗ
    * cityid Код города по базе СДЭК 
    *  type = all 
    * «PVZ» - для отображения только складов СДЭК;
    * «POSTOMAT» - для отображения постоматов партнёра;
    * «ALL» - для отображения всех ПВЗ не зависимо от их типа.
    *
    */
    public function getListPVZ($cityid = null, $citypostcode = null, $weightmin, $type = "ALL"){

        !is_null($citypostcode) ? $this->_params['citypostcode'] =  $citypostcode : $this->_params['cityid'] = $cityid;
        $this->_params['weightmin'] = $weightmin;
        $this->_params['type'] = $type;
        $result = $this->connect($this->url . '/pvzlist/v1/json');
 
        return $this->resultToArray($result);
    }
    
    /**
    * Получение стоимости доставки по тарифам без приоритета товаров по заданным тарифам
    * senderCityId Код города отправителя из базы СДЭК
    * receiverCityId Код города получателя из базы СДЭК
    * tariffList Список тарифов
    * goods Габаритные характеристики места (товаров)
    *
    */
    public function geCalcOrder($senderCityId = 261, $receiverCityId, $tariffList, $goods){

        $this->_params['version'] = "1.0";
        $this->_params['currency'] = "RUB";
        $this->_params['senderCityId'] = $senderCityId;
        $this->_params['receiverCityId'] = $receiverCityId;
        $this->_params['tariffList'] = $tariffList;
        $this->_params['goods'] = $goods;
        //$this->_params['authLogin'] = $this->getAuthToken();
        $this->_params['authLogin'] = $this->login;
        $this->_params['secure'] = $this->password;
        $this->_params['dateExecute'] = date('Y-m-d');


        $result = $this->connectPost($this->priceurl . '/calculator/calculate_tarifflist.php');
/*echo '<pre>';
print_r($this->_params);
print_r($this->resultToArray($result));
Yii::app()->end();*/
        return $this->resultToArray($result);
    } 
    
    /**
    * Получение стоимости доставки по тарифам без приоритета товаров по заданным тарифам
    * senderCityId Код города отправителя из базы СДЭК
    * receiverCityPostCode Код города получателя из базы СДЭК
    * tariffList Список тарифов
    * goods Габаритные характеристики места (товаров)
    *
    */
    public function geCalcTariffs($senderCityId = 261, $receiverCityPostCode, $tariffList, $goods){

        $this->_params['version'] = "1.0";
        $this->_params['currency'] = "RUB";
        $this->_params['senderCityId'] = $senderCityId;
        $this->_params['receiverCityPostCode'] = $receiverCityPostCode;
        $this->_params['tariffList'] = $tariffList;
        $this->_params['goods'] = $goods;
        //$this->_params['authLogin'] = $this->getAuthToken();
        $this->_params['authLogin'] = $this->login;
        $this->_params['secure'] = $this->password;
        $this->_params['dateExecute'] = date('Y-m-d');

        $result = $this->connectPost($this->priceurl . '/calculator/calculate_tarifflist.php');
        
        return $this->resultToArray($result);
    }
    
    /**
    *
    * Расчет стоимости доставки товаров по заданным тарифам с приоритетом
    * senderCityId Код города отправителя из базы СДЭК
    * receiverCityId Код города получателя из базы СДЭК
    * tariffList Список тарифов
    * goods Габаритные характеристики места (товаров)
    *
    */
    public function geCalcWithPriorityOrder($senderCityId = 261, $receiverCityId, $tariffList, $goods){

        $this->_params['version'] = "1.0";
        $this->_params['currency'] = "RUB";
        $this->_params['senderCityId'] = $senderCityId;
        $this->_params['receiverCityId'] = $receiverCityId;
        $this->_params['tariffList'] = $tariffList;
        $this->_params['goods'] = $goods;
        //$this->_params['authLogin'] = $this->getAuthToken();
        $this->_params['authLogin'] = $this->login;
        $this->_params['secure'] = $this->password;
        $this->_params['dateExecute'] = date('Y-m-d');
        
        $result = $this->connectPost($this->priceurl . '/calculator/calculate_price_by_json.php');
        return $this->resultToArray($result);
    }

    /**
    * Заказ от ИМ
    *
    * 
    *
    */
    public function createOrder($order, $SendCityCode = 261){

        $this->_params = [];
        $deliveryRequest = [
            'Number' => $order->id,
            'Date' => $order->date,
            'Account' => 'z9GRRu7FxmO53CQ9cFfI6qiy32wpfTkd',
            'Secure' => 'w24JTCv4MnAcuRTx0oHjHLDtyt3I6IBq',
            'OrderCount' => count($order->products)
        ];
        
        $orderCdek = [
            'Number' => $order->id,
            'SendCityCode' => $SendCityCode,
            'RecCityPostCode' => $order->zipcode,
            'RecipientName' => $order->family,
            'RecipientEmail' => $order->email,
            'Phone' => $order->phone,
            //'TariffTypeCode' => $order->tariff_id,
            'TariffTypeCode' => 137,
        ];
        
        $address = [
            'Street' => $order->street,
            'House' => $order->house,
            'Flat' => $order->apartment,
        ];
        
        $package = [
            'Number' => $order->id,
            'BarCode' => $order->id,
            'Weight' => 1,
        ];
        
        $item = [
            'WareKey' => 'NN0001', // Идентификатор/артикул товара/вложения
            'Cost'    => 500, // Объявленная стоимость товара (за единицу товара)
            'Payment' => 0, // Оплата за товар при получении (за единицу товара)
            'Weight'  => 120, // Вес (за единицу товара, в граммах)
            'Amount'  => 2, // Количество единиц одноименного товара (в штуках)
            'Comment' => 'Test item',
        ];
        
        $this->_params['DeliveryRequest'] = $deliveryRequest; 
        $this->_params['Order'] = $orderCdek;  
        $this->_params['Package'] = $package;  
        $this->_params['Address'] = $address;
        $this->_params['Package'] = $package;
        $this->_params['Item'] = $item;
 
        $result = $this->connectPost($this->url . '/new_orders.php');

    }
    
    public function createTestOrder(){
        
        $deliveryRequest = [
            'Number' => '213123',
            'Date' => '2020-02-11',
            'Account' => 'z9GRRu7FxmO53CQ9cFfI6qiy32wpfTkd',
            'Secure' => 'w24JTCv4MnAcuRTx0oHjHLDtyt3I6IBq',
            'OrderCount' => 3
        ];
        
        $order = [
            'Number'   => 'TEST-123456',
            'SendCityCode'    => 44, // Москва
            'RecCityPostCode' => '630001', // Новосибирск
            'RecipientName'  => 'Иван Петров',
            'RecipientEmail' => 'petrov@test.ru',
            'Phone'          => '+7 (383) 202-22-50',
            'TariffTypeCode' => 139, // Посылка дверь-дверь от ИМ
        ];
        
        $address = [
            'Street' => 'Холодильная улица',
            'House'  => '16',
            'Flat'   => '22',
        ];
        
        $package = [
            'Number'  => 'TEST-123456',
            'BarCode' => 'TEST-123456',
            'Weight'  => 500, // Общий вес (в граммах)
            'SizeA'   => 10, // Длина (в сантиметрах), в пределах от 1 до 1500
            'SizeB'   => 10,
            'SizeC'   => 10,
        ];
        
        $item = [
            'WareKey' => 'NN0001', // Идентификатор/артикул товара/вложения
            'Cost'    => 500, // Объявленная стоимость товара (за единицу товара)
            'Payment' => 0, // Оплата за товар при получении (за единицу товара)
            'Weight'  => 120, // Вес (за единицу товара, в граммах)
            'Amount'  => 2, // Количество единиц одноименного товара (в штуках)
            'Comment' => 'Test item', 
        ];
        
        $params = [
            $deliveryRequest,
            'Order' => $order,
            'Address' => $address,
            'Package' => $package,
            'Item' => $item,
        ];

        $result = $this->connectPostParams($params, $this->url . '/new_orders.php');
    }
}
