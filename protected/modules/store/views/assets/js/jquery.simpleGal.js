(function($){

  $.fn.extend({

    simpleGal: function (options) {

      var defaults = {
        mainImage: ".placeholder"
      };

      options = $.extend(defaults, options);

      return this.each(function () {

        var thumbnail = $(this).find("a"), mainImage = $(this).parent().find(options.mainImage);
 
        thumbnail.on("click", function (e) {
          e.preventDefault();
          var galleryImage = $(this).attr("href");
            $('#main-image').attr("src", galleryImage);
            $('.img-lightbox').attr("href", galleryImage);
            $('.zoomImg').attr("src", galleryImage);
            //mainImage.attr("src", galleryImage);
        });

      });

    }

  });

})(jQuery);

$(document).ready(function(){
    $('a.zoom').zoom();
});
